from app.modules.bot.models import BotModel, WitApp
from app.modules.user.models import UserModel
from app.modules.account.models import AccountModel
from .utils import post_json, json_response, put_json, get_request, delete_json
from app.modules.saved_reply.models import SavedReplyModel
import uuid, time

ROOT_URL = '/v2/bots'

def test_create_bot(api, login):
    """Create new bot."""
    account_id = login["account_id"]
    api_key = login["api_key"]

    # TEST CASE: there's space in the bot name
    url = '{0}/{1}'.format(ROOT_URL, account_id)
    data = {
        "name": "testing bot",
        "channel": "messenger",
        "channel_id": "jEuL2jjRRx2gYAe8VGHSbw2",
        "lang": "id"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 400

    # BEST CASE: user submit all data correctly based on type
    url = '{0}/{1}'.format(ROOT_URL, account_id)
    data = {
        "name": "testing-bot-{}".format(uuid.uuid4()),
        "channel": "messenger",
        "channel_id": "jEuL2jjRRx2gYAe8VGHSbw2",
        "lang": "id"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    # print(json_response(rv))
    assert rv.status_code == 201

    # TEST CASE: create new bot in the same channel
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 400

    data = {
        "name": "testing-bot-{}".format(uuid.uuid4()),
        "channel": "messenger",
        "channel_id": "jEuL2jjRRx2gYAe8VGHSbw3",
        "lang": "id"
    }

    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 201

    # TEST CASE: exceeding bot limit
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 403

    # TEST CASE: account_id is provided but incorrect
    url = '{0}/{1}'.format(ROOT_URL, 'wrongwrong')
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 404

    # TEST CASE: wrong format
    url = '{0}/{1}'.format(ROOT_URL, account_id)
    data = {
        "name": "my bot",
        "platform": "messenger"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 400

    # TEST CASE: duplicate name
    url = '{0}/{1}'.format(ROOT_URL, account_id)
    data = {
        "name": "testing-bot",
        "channel": "messenger",
        "channel_id": "jEuL2jjRRx2gYAe8VGHSbw2",
        "lang": "id"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 403


def test_create_bot_2(api, login):
    account_id = login["account_id"]
    api_key = login["api_key"]

    # TEST CASE: not pay :(
    url = '{0}/{1}'.format(ROOT_URL, account_id)
    data = {
        "name": "testing-bot",
        "channel": "messenger",
        "channel_id": "jEuL2jjRRx2gYAe8VGHSbw2",
        "lang": "id"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 403

def test_entity_bot(api, login):
    account_id = login["account_id"]
    api_key = login["api_key"]

    url = '{0}/{1}/entity'.format(ROOT_URL, account_id)

    # TESTCASE : get empty entity
    rv = get_request(api, url)
    assert rv.status_code == 200
    assert len(json_response(rv)) == 0

    # TESTCASE : post entity with right format
    data = {
        "doc": "This entity for howto purposes",
        "entity_name": "how_to / bagaimana"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 200

    # TESTCASE : post entity twice
    data = {
        "doc": "This entity for howto purposes",
        "entity_name": "how_to / bagaimana"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 400

    # TESTCASE : post entity exceeding limit
    data = {
        "doc": "This entity for howto purposes",
        "entity_name": "how_to 2"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 403

    # TESTCASE: post entity with wrong format
    data = {
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 400

    # TESTCASE : delete entity, and add it again
    dl_url = '{0}/{1}/entity?entity_name={2}'.format(ROOT_URL, account_id, "how_to / bagaimana")
    dl = api.delete(dl_url)
    assert dl.status_code == 204

    data = {
        "doc": "This entity for howto purposes",
        "entity_name": "how_to"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 200

    # TESTCASE : delete entity with wrong entity_name
    dl_url = '{0}/{1}/entity?entity_name={2}'.format(ROOT_URL, account_id, "how_tos")
    dl = api.delete(dl_url)
    assert dl.status_code == 404

def test_validation_bot(api, login):
    account_id = login["account_id"]
    api_key = login["api_key"]

    url = '{0}/{1}/validate'.format(ROOT_URL, account_id)

    # TESTCASE : validate with right format
    data = {
      "_text": "how to order",
      "entity_name": "how_to",
      "value": "order"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    # print(json_response(rv))
    assert rv.status_code == 200

    # TESTCASE : validate with empty entity name
    data = {
      "_text": "how to order",
      "entity_name": "",
      "value": "order"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 404

    # TESTCASE : validate with wrong entity name
    data = {
      "_text": "how to order",
      "entity_name": "wrong",
      "value": "order"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 404

    # TESTCASE : validate with wrong format
    data = {
      "_text": "how to order",
      "value": "order"
    }
    rv = post_json(api=api, url=url, json_dict=data)
    assert rv.status_code == 400

    # TESTCASE : check if entity have expression
    rv = get_request(api, '{0}/{1}/entity'.format(ROOT_URL, account_id))
    resp = json_response(rv)[0]['values']
    assert len(resp) == 1

def test_get_meaning_bot(api, login):
    account_id = login["account_id"]
    api_key = login["api_key"]

    # rv = get_request(api, '{0}/{1}/entity'.format(ROOT_URL, account_id))
    time.sleep(10)
    # TESTCASE: Get meaning with right format
    url = '{0}/{1}/meaning?q=how to order?'.format(ROOT_URL, account_id)
    rv = get_request(api, url)
    assert rv.status_code == 200
    # print(json_response(rv))
    # assert len(json_response(rv)['entities']) > 0

    # TESTCASE: Get meaning without entity
    url = '{0}/{1}/meaning?q=can you make it to alaska?'.format(ROOT_URL, account_id)
    rv = get_request(api, url)
    assert rv.status_code == 200

def test_value_bot(api, login):
    account_id = login["account_id"]
    api_key = login["api_key"]

    url = '{0}/{1}/value?entity_name={2}'.format(ROOT_URL, account_id, 'how_to')
    rv = get_request(api, url)
    assert rv.status_code == 200
    assert len(json_response(rv)) > 0

    url = '{0}/{1}/value?entity_name={2}'.format(ROOT_URL, account_id, 'howtos')
    rv = get_request(api, url)
    assert rv.status_code == 200
    assert len(json_response(rv)) == 0

def test_bot_rule(api, login, session):
    account_id = login["account_id"]
    api_key = login["api_key"]

    # TESTCASE : make saved reply
    saved_reply = SavedReplyModel(
        account_id=account_id,
        label='test_saved_reply',
        channel='line',
        id=1
        )
    session.add(saved_reply)
    saved_reply = SavedReplyModel(
        account_id=account_id,
        label='test_saved_reply_2',
        channel='messenger',
        id=2
        )
    session.add(saved_reply)
    session.commit()

    # TESTCASE : create bot rule with right format
    url = '/v2/bot_rules/{}/1'.format(account_id)
    data = {
        "entity_name": "how_to",
        "value": "order",
        "rules": [
            {
                "rule_type": "SEND_EMAIL",
                "emails": ["dikaaadit@gmail.com"]
            }
        ]
    }
    rv = post_json(api, url, json_dict=data)
    print(json_response(rv))
    assert rv.status_code == 200

    data = {
        "entity_name": "how_to",
        "value": "order",
        "rules": [
            {
                "rule_type": "SEND_WEBHOOK",
                "webhook_url": "https://www.google.com"            
            }
        ]
    }
    rv = post_json(api, url, json_dict=data)
    assert rv.status_code == 200

    data = {
        "entity_name": "how_to",
        "value": "order",
        "rules": [
            {
                "rule_type": "ASSIGN_HUMAN",
                "assign_username": "mradityanod@gmail.com"            
            },
            {
                "rule_type": "SEND_MESSAGE",
                "replies": [2]
            }
        ]
    }
    # q = UserModel.query.filter_by(username="rizki@chatkoo.com").all()
    rv = post_json(api, url, json_dict=data)
    # print(json_response(rv))
    assert rv.status_code == 200

    # # TESTCASE : create bot rule with wrong format
    # data = {
    #     "rule_type": "SEND_EMAIL",
    #     "entity_name": "how_to",
    #     "value": "order",
    #     "reply_id": [1]
    # }
    # rv = post_json(api, url, json_dict=data)
    # assert rv.status_code == 400

    # # TESTCASE : create bot rule with wrong webhook format
    # data = {
    #     "rule_type": "SEND_WEBHOOK",
    #     "entity_name": "how_to",
    #     "value": "order",
    #     "webhook_url": "http://google.com",
    #     "reply_id": [1]
    # }
    # rv = post_json(api, url, json_dict=data)
    # assert rv.status_code == 400

    # # TESTCASE : create bot rule with wrong assignee format
    # data = {
    #     "rule_type": "SEND_WEBHOOK",
    #     "entity_name": "how_to",
    #     "value": "order",
    #     "reply_id": [1]
    # }
    # rv = post_json(api, url, json_dict=data)
    # assert rv.status_code == 400

    # # TESTCASE : create bot rule with wrong entity
    # data = {
    #     "rule_type": "SEND_EMAIL",
    #     "entity_name": "how_tos",
    #     "emails": ["mradityanod@gmail.com"],
    #     "value": "order",
    #     "reply_id": [1]
    # }
    # rv = post_json(api, url, json_dict=data)
    # print(json_response(rv))
    # assert rv.status_code == 404

    # # TESTCASE : create bot rule with wrong reply type
    # data = {
    #     "rule_type": "SEND_EMAIL",
    #     "entity_name": "how_to",
    #     "emails": ["mradityanod@gmail.com"],
    #     "value": "order",
    #     "reply_id": [2]
    # }
    # rv = post_json(api, url, json_dict=data)
    # assert rv.status_code == 400

    # # TESTCASE : multiple rule in one bot
    # data = {
    #     "rule_type": "SEND_EMAIL",
    #     "entity_name": "how_to",
    #     "emails": ["mradityanod@gmail.com"],
    #     "value": "order",
    #     "reply_id": [2]
    # }
    # rv = post_json(api, url, json_dict=data)
    # assert rv.status_code == 400

    # # TESTCASE : modify bot rule
    # url = '/v2/bot_rules/{}/1/1'.format(account_id)
    # data = {
    #     "rule_type": "SEND_EMAIL",
    #     "entity_name": "how_to",
    #     "value": "order",
    #     "emails": ["famasya@icloud.com"],
    #     "reply_id": [1]
    # }
    # rv = put_json(api, url, json_dict=data)
    # # print(json_response(rv))
    # assert rv.status_code == 200

    # # TESTCASE : delete bot rule
    # url = '/v2/bot_rules/{}/1/1'.format(account_id)
    # rv = delete_json(api, url)
    # assert rv.status_code == 204

def test_delete_expression(api, login):
    account_id = login["account_id"]
    api_key = login["api_key"]

    # TEST CASE : delete expression with right format
    url = '{0}/{1}/expression?entity_name=how_to&_text=how to order&value=order'.format(ROOT_URL, account_id)
    rv = delete_json(api, url)
    assert rv.status_code == 204

    # TEST CASE : delete expression with wrong data
    url = '{0}/{1}/expression?entity_name=howto&_text=how to order&value=order'.format(ROOT_URL, account_id)
    rv = delete_json(api, url)
    assert rv.status_code == 404

def test_get_bot(api, login):
    """Test get a bot"""
    account_id = login["account_id"]
    api_key = login["api_key"]

    url = '{0}/{1}'.format(ROOT_URL, account_id)
    rv = get_request(api, url)
    assert rv.status_code == 200

    url = '{0}/{1}'.format(ROOT_URL, "account_id")
    rv = get_request(api, url)
    assert rv.status_code == 200
    assert len(json_response(rv)) == 0

def test_get_bot_detail(api, login):
    """Test get a bot"""
    account_id = login["account_id"]
    api_key = login["api_key"]

    # BEST CASE: account id is provided
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, 1)
    rv = api.get(url)
    assert rv.status_code == 200

    # TEST CASE: account id is provided but incorrect
    url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongtoken', 1)
    rv = api.get(url)
    assert rv.status_code == 404

    # TEST CASE: wrong bot id
    url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongtoken', 100)
    rv = api.get(url)
    assert rv.status_code == 404


def test_update_bot(api, login):
    """Update test."""
    account_id = login["account_id"]
    api_key = login["api_key"]

    data = {
        "name": "testing-bot-{}".format(uuid.uuid4()),
        "channel": "messenger",
        "channel_id": "jEuL2jjRRx2gYAe8VGHSbw2",
        "is_active": True
    }
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, 1)
    rv = put_json(api, url, json_dict=data)
    assert rv.status_code == 200

    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, 100)
    rv = put_json(api, url, json_dict=data)
    assert rv.status_code == 404

    url = '{0}/{1}/{2}'.format(ROOT_URL, "account_id", 1)
    rv = put_json(api, url, json_dict=data)
    assert rv.status_code == 404

def test_delete_bot(api, login):
    account_id = login["account_id"]
    api_key = login["api_key"]

    # BEST CASE: all data provided correctly
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, 1)
    s = api.delete(url)
    assert s.status_code == 204

    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, 2)
    s = api.delete(url)
    assert s.status_code == 204

    # App should deleted when all bot has been removed
    wit = WitApp.query.filter_by(account_id=account_id).filter_by(deleted=False).first()
    assert wit == None

    account = AccountModel.query.filter_by(id=account_id).first()
    assert account.current_bot_usage == 0

    # TEST CASE: account id is provided but incorrect
    url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongwrong', 1)
    rv = api.delete(url)
    assert rv.status_code == 404

def test_channel_bot(api, login):
    account_id = login["account_id"]
    api_key = login["api_key"]

    url = '{0}/{1}/channel_available'.format(ROOT_URL, account_id)
    rv = api.get(url)
    assert rv.status_code == 200
