# pylint: disable=C0103

"""Test file for Inbox API endpoint."""
from app.modules.customer.models import CustomerModel
from app.modules.user.models import UserModel
from .utils import put_json, post_json, json_response

ROOT_URL = '/v2/customers'
args = 'start=1&limit=10'


def test_get_customer_list(api, login):
    """Test get list of customers."""
    account_id = login['account_id']
    api_key = login["api_key"]

    # BEST CASE: user submit the data correctly
    url = '{0}/{1}?{2}'.format(ROOT_URL, account_id, args)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 200
    assert len(rv.data) > 0

    # TEST CASE: user submit with incorrect account_id
    url = '{0}/{1}?{2}'.format(ROOT_URL, 'wrongwrong', args)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 404

    # TEST CASE: user submit with incorrect api_key
    url = '{0}/{1}?{2}'.format(ROOT_URL, account_id, args)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    s = api.get(url, headers=headers)
    assert s.status_code == 400

    # TEST CASE: user submit without api_key
    url = '{0}/{1}?{2}'.format(ROOT_URL, account_id, args)
    s = api.get(url, headers=None)
    assert s.status_code == 401


def test_update_customer(api, login, session):
    """Test update a customer detail."""
    account_id = login['account_id']
    api_key = login["api_key"]

    # CREATE NEW CUSTOMER
    customer = CustomerModel(
        account_id=account_id,
        id='aasdf',
        name='Rizki Aditya',
        avatar_url='https://scontent.xx.fbcdn.net/v/t1.0-1/15590513_10207534149903200_1299323674069209622_n.jpg?oh=c4cd1bdd5952720a605267fdbd95c99e&oe=59C3E263',
        email='rizki@chatkoo.com',
        platform='messenger',
        platform_id='10206045681532421'
    )
    session.add(customer)
    session.commit()

    update_customer = {
        "name": "falaqie nila",
        "avatar_url": "example.com/falaqie.png",
        "email": "bla@bla.com"
    }

    # BEST CASE: user submit the data correctly
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, customer.id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = put_json(api, url=url, json_dict=update_customer, headers=headers)

    assert rv.status_code == 200

    # TEST CASE: user submit with incorrect account_id
    url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongwrong', customer.id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = put_json(api, url=url, json_dict=update_customer, headers=headers)
    assert rv.status_code == 404

    # TEST CASE: user submit with incorrect api_key
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, customer.id)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    rv = put_json(api, url=url, json_dict=update_customer, headers=headers)
    assert rv.status_code == 400

    # TEST CASE: user submit without api_key
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, customer.id)
    rv = put_json(api, url=url, json_dict=update_customer, headers=None)
    assert rv.status_code == 401

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    another_user = UserModel.query.filter_by(username='mradityanod@gmail.com').first()

    # TEST CASE: user submit with another account_id
    url = '{0}/{1}/{2}'.format(ROOT_URL, another_user.account_id, customer.id)
    headers = {"Authorization": another_user.api_key}
    rv = put_json(api, url=url, json_dict=update_customer, headers=headers)
    assert rv.status_code == 404


def test_get_customer_detail(api, login, session):
    """Test get a customer detail."""
    account_id = login['account_id']
    api_key = login["api_key"]

    # CREATE NEW CUSTOMER
    customer = CustomerModel(
        account_id=account_id,
        id='abcdef',
        name='Rizki Aditya',
        avatar_url='https://scontent.xx.fbcdn.net/v/t1.0-1/15590513_10207534149903200_1299323674069209622_n.jpg?oh=c4cd1bdd5952720a605267fdbd95c99e&oe=59C3E263',
        email='rizki@chatkoo.com',
        platform='messenger',
        platform_id='10206045681532421'
    )
    session.add(customer)
    session.commit()

    # BEST CASE: user submit the data correctly
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, customer.id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 200

    # TEST CASE: user submit with incorrect account_id
    url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongwrong', customer.id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 404

    # TEST CASE: user submit with incorrect api_key
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, customer.id)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 400

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    another_user = UserModel.query.filter_by(username='mradityanod@gmail.com').first()

    # TEST CASE: user submit with another account_id
    url = '{0}/{1}/{2}'.format(ROOT_URL, another_user.account_id, customer.id)
    headers = {"Authorization": another_user.api_key}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 404


def test_delete_customer(api, login, session):
    """Test get a customer detail."""
    account_id = login['account_id']
    api_key = login["api_key"]

    # CREATE NEW CUSTOMER
    customer = CustomerModel(
        account_id=account_id,
        id='dsfsdytdy',
        name='Rizki Aditya',
        avatar_url='https://scontent.xx.fbcdn.net/v/t1.0-1/15590513_10207534149903200_1299323674069209622_n.jpg?oh=c4cd1bdd5952720a605267fdbd95c99e&oe=59C3E263',
        email='rizki@chatkoo.com',
        platform='messenger',
        platform_id='10206045681532421'
    )
    session.add(customer)
    session.commit()

    # BEST CASE: user submit the data correctly
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, customer.id)
    headers = {'Authorization': '{0}'.format(api_key)}
    s = api.delete(url, headers=headers)
    assert s.status_code == 204

    # TEST CASE: user submit with incorrect account_id
    url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongwrong', customer.id)
    headers = {'Authorization': '{0}'.format(api_key)}
    s = api.delete(url, headers=headers)
    assert s.status_code == 404

    # TEST CASE: user submit with incorrect api_key
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, customer.id)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    s = api.delete(url, headers=headers)
    assert s.status_code == 400

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    another_user = UserModel.query.filter_by(username='mradityanod@gmail.com').first()

    # TEST CASE: user submit with another account_id
    url = '{0}/{1}/{2}'.format(ROOT_URL, another_user.account_id, customer.id)
    headers = {"Authorization": another_user.api_key}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 404
