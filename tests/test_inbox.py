# pylint: disable=C0103

"""Test file for Inbox API endpoint."""
import os
import requests
import json
import pytest


ROOT_URL = 'http://localhost:8000/v2/inboxes'
user = {
    'username': 'mradityanod@gmail.com',
    'password': 'kasurinirusak',
    'remember': False
}


login_url = 'http://localhost:8000/v2/users/login'
new_user = {
    'username': 'ditz24159292@gmail.com',
    'password': 'kasurinirusak',
    'remember': False
}


@pytest.fixture(scope="module")
def login():
    """Login fixture, so that it's no need login for each test."""
    login_url = 'http://localhost:8000/v2/users/login'

    r = requests.post(login_url, json=user)

    resp = {
        'account_id': r.json()['account_id'],
        'api_key': r.json()['api_key']
    }

    return resp


# GET AN INBOX
@pytest.mark.get_inbox_detail
def test_get_inbox_detail(login):
    """Test get an inbox."""
    account_id = login['account_id']
    api_key = login['api_key']

    # Get an inbox_id
    args = 'start=1&limit=10&archived=false'
    inbox_url = '{}/{}?{}'.format(ROOT_URL, account_id, args)
    headers = {'Authorization': '{0}'.format(api_key)}
    r = requests.get(inbox_url, headers=headers)

    inbox_id = r.json()['results'][0]['id']

    # TEST CASE : repeat to get inboxes one by one
    for i in range(len(r.json()['results'])):
        inbox_id = r.json()['results'][i-1]['id']

        # test get status
        get_url = '{}/{}/{}'.format(ROOT_URL, account_id, inbox_id)
        headers = {'Authorization': '{0}'.format(api_key)}
        s = requests.get(get_url, headers=headers)

        assert s.status_code == 200

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}/{}'.format(ROOT_URL, 'wrongwrong', inbox_id)
    s = requests.get(url, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect api_key
    url = '{}/{}/{}'.format(ROOT_URL, account_id, inbox_id)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    s = requests.get(url, headers=headers)

    assert s.status_code == 401

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    r = requests.post(login_url, json=new_user)
    new_account_id = r.json()['account_id']
    new_api_key = r.json()['api_key']

    # TEST CASE: user submit inbox_id with wrong account_id
    url = '{}/{}/{}'.format(ROOT_URL, new_account_id, inbox_id)
    headers = {'Authorization': '{0}'.format(new_api_key)}
    s = requests.get(url, headers=headers)

    assert s.status_code == 404


# GET INBOX LIST
@pytest.mark.get_inbox_list
def test_get_inbox_list(login):
    """Test get list of inboxes."""
    account_id = login['account_id']
    api_key = login['api_key']

    # test get inbox
    args = 'start=1&limit=10&archived=false'
    inbox_url = '{}/{}?{}'.format(ROOT_URL, account_id, args)
    headers = {'Authorization': '{0}'.format(api_key)}
    r = requests.get(inbox_url, headers=headers)

    assert r.status_code == 200


# GET INBOX STATUS
@pytest.mark.get_total_status
def test_get_total_status(login):
    """Test get total status."""
    account_id = login['account_id']
    api_key = login['api_key']

    # test get status
    inbox_url = '{}/{}/status'.format(ROOT_URL, account_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    r = requests.get(inbox_url, headers=headers)

    assert r.status_code == 200


# GET INBOX LIST BY STATUS
@pytest.mark.inbox_bystatus
def test_get_inbox_bystatus(login):
    """Test get list of inboxes by status."""
    account_id = login['account_id']
    api_key = login['api_key']

    args = 'start=1&limit=10&archived=false'

    list_status = ['open', 'closed', 'inprogress', 'mine']

    for status in list_status:
        inbox_url = '{}/{}/status/{}?{}'.format(ROOT_URL, account_id, status, args)
        headers = {'Authorization': '{0}'.format(api_key)}
        r = requests.get(inbox_url, headers=headers)

        assert r.status_code == 200


# UPDATE INBOX
@pytest.mark.update_inbox
def test_update_inbox(login):
    """Test update an inbox."""
    account_id = login['account_id']
    api_key = login['api_key']

    # get inbox id
    args = 'start=1&limit=10&archived=false'
    inbox_url = '{}/{}?{}'.format(ROOT_URL, account_id, args)
    headers = {'Authorization': '{0}'.format(api_key)}
    r = requests.get(inbox_url, headers=headers)

    inbox_id = r.json()['results'][0]['id']

    # TEST CASE : edit status
    update_inbox = {
        "status": "pending",
        "archived": False,
        "unread_count": 2
    }
    update_url = '{}/{}/{}'.format(ROOT_URL, account_id, inbox_id)
    s = requests.put(update_url, json=update_inbox, headers=headers)

    assert s.status_code == 200

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}/{}'.format(ROOT_URL, 'wrongwrong', inbox_id)
    s = requests.put(url, json=update_inbox, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect api_key
    url = '{}/{}/{}'.format(ROOT_URL, account_id, inbox_id)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    s = requests.put(url, json=update_inbox, headers=headers)

    assert s.status_code == 401

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    r = requests.post(login_url, json=new_user)
    new_account_id = r.json()['account_id']
    new_api_key = r.json()['api_key']

    # TEST CASE: user submit inbox_id with wrong account_id
    url = '{}/{}/{}'.format(ROOT_URL, new_account_id, inbox_id)
    headers = {'Authorization': '{0}'.format(new_api_key)}
    s = requests.delete(url, headers=headers)

    assert s.status_code == 404


# DELETE INBOX
@pytest.mark.delete_inbox
def test_delete_inbox(login):
    account_id = login['account_id']
    api_key = login['api_key']

    # TEST CASE : delete inbox with archived false
    # get inbox id
    args = 'start=1&limit=10&archived=false'
    inbox_url = '{}/{}?{}'.format(ROOT_URL, account_id, args)
    headers = {'Authorization': '{0}'.format(api_key)}
    r = requests.get(inbox_url, headers=headers)

    for i in range(len(r.json()['results'])):
        inbox_id = r.json()['results'][i-1]['id']

        delete_url = '{}/{}/{}'.format(ROOT_URL, account_id, inbox_id)

        s = requests.delete(delete_url, headers=headers)

        assert s.status_code == 204

    # TEST CASE : delete inbox with archived true
    # get inbox id
    args = 'start=1&limit=10&archived=true'
    inbox_url = '{}/{}?{}'.format(ROOT_URL, account_id, args)
    headers = {'Authorization': '{0}'.format(api_key)}
    r = requests.get(inbox_url, headers=headers)

    for i in range(len(r.json()['results'])):
        inbox_id = r.json()['results'][i-1]['id']

        delete_url = '{}/{}/{}'.format(ROOT_URL, account_id, inbox_id)

        s = requests.delete(delete_url, headers=headers)

        assert s.status_code == 204

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}/{}'.format(ROOT_URL, 'wrongwrong', inbox_id)
    s = requests.delete(url, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect api_key
    url = '{}/{}/{}'.format(ROOT_URL, account_id, inbox_id)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    s = requests.delete(url, headers=headers)

    assert s.status_code == 401

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    r = requests.post(login_url, json=new_user)
    new_account_id = r.json()['account_id']
    new_api_key = r.json()['api_key']

    # TEST CASE: user submit inbox_id with wrong account_id
    url = '{}/{}/{}'.format(ROOT_URL, new_account_id, inbox_id)
    headers = {'Authorization': '{0}'.format(new_api_key)}
    s = requests.delete(url, headers=headers)

    assert s.status_code == 404
