# pylint: disable=C0103

"""Test file for Channel API endpoint."""
from app.modules.channel.models import ChannelModel
from app.modules.user.models import UserModel
from .utils import post_json, put_json, json_response

ROOT_URL = '/v2/channels'

#  CREATE NEW LINE INTEGRATION
def test_create_channel(api, login):
    """Test create new line."""
    account_id = login["account_id"]
    api_key = login["api_key"]

    # BEST CASE: user submit all data correctly based on type
    # LINE
    url = '{0}/{1}'.format(ROOT_URL, account_id)
    line = {
        "type": "line",
        "channel_access_token": "Hadsfdsf44DFdsf",
        "channel_secret": "dsfsgdf34552dfsf",
        "channel_name": "Chatkoo's Bot"
    }
    headers = {"Authorization": api_key}
    rv = post_json(api=api, url=url, json_dict=line, headers=headers)
    assert rv.status_code == 201

    # Messenger
    # Cannot be automatically tested due to facebook permission policy
    # messenger = {
    #     "type": "messenger",
    #     "page_name": "Chatkoo's Bot",
    #     "page_id": "1788136031513500",
    #     "page_access_token": "EAAZAPZCyzt5ZCIBAIoKU3UXZCS72TlApZBgmqe5qZCDFTWic4gaJ77OpkGr2BRfwkrwJVOfkceZCM8LTPFec8sn8KnM82QxzvIprIyzejrjdhRvRVg6X9Xci4C2xwAIVN0BkNC3MST7jM16DhGCRJoqmb7DNCiWRxdhTZAVgdg2xBgZDZD"
    # }
    # headers = {"Authorization": api_key}
    # rv = post_json(api=api, url=url, json_dict=messenger, headers=headers)
    # print(json_response(rv))
    # assert rv.status_code == 201

    # Telegram
    telegram = {
        "type": "telegram",
        "bot_name": "Bot name",
        "username": "@botname",
        "token": "sdft4w5esfdfgdgfg"
    }
    headers = {"Authorization": api_key}
    rv = post_json(api=api, url=url, json_dict=telegram, headers=headers)
    assert rv.status_code == 201

    # TEST CASE: wrong channel type
    data = {
        "type": "messenger",
        "channel_access_token": "Hadsfdsf44DFdsf",
        "channel_secret": "dsfsgdf34552dfsf",
        "channel_name": "Chatkoo's Bot"
    }
    headers = {"Authorization": api_key}
    rv = post_json(api=api, url=url, json_dict=data, headers=headers)
    assert rv.status_code == 400  # Invalid request

    # TEST CASE: type is not provided
    data = {
        "channel_access_token": "Hadsfdsf44DFdsf",
        "channel_secret": "dsfsgdf34552dfsf",
        "channel_name": "Chatkoo's Bot"
    }
    headers = {"Authorization": api_key}
    rv = post_json(api=api, url=url, json_dict=data, headers=headers)
    assert rv.status_code == 400  # Invalid request

    # TEST CASE: account_id is not provided
    #########

    # TEST CASE: account_id is provided but incorrect
    line_url = '{0}/{1}'.format(ROOT_URL, 'wrongwrong')
    line = {
        "type": "line",
        "channel_access_token": "harpa45dfd",
        "channel_secret": "beatboxdasf",
        "channel_name": "Macintosh43sdf"
    }
    headers = {"Authorization": api_key}
    rv = post_json(api, line_url, line, headers)
    assert rv.status_code == 404

    # # TEST CASE: access token is not provided
    line_url = '{0}/{1}'.format(ROOT_URL, account_id)
    line = {
        "type": "line",
        # "channel_access_token": "harpa45dfd",
        "channel_secret": "beatboxdasf",
        "channel_name": "Macintosh43sdf"
    }
    rv = post_json(api, line_url, line, headers=None)
    assert rv.status_code == 401

    # TEST CASE: user submit the same data
    #########


# GET ALL LIST CHANNELS
def test_get_all_channel(api, login):
    """Get all channels."""
    account_id = login['account_id']
    api_key = login["api_key"]

    # BEST CASE
    url = '{0}/{1}'.format(ROOT_URL, account_id)
    headers = {"Authorization": api_key}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 200
    assert len(rv.data) > 0

    # TEST CASE: wrong account_id
    url = '{0}/{1}'.format(ROOT_URL, 'wrongwrong')
    headers = {"Authorization": api_key}
    rv = api.get(url, headers=headers)
    res = json_response(rv)
    assert rv.status_code == 200
    assert len(res) == 0


# GET A CHANNEL
def test_get_channel(api, login):
    """Get a channel."""
    account_id = login['account_id']
    api_key = login["api_key"]

    line = ChannelModel.query.filter_by(channel_access_token="Hadsfdsf44DFdsf").first()

    # BEST CASE: account id is provided
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    headers = {"Authorization": api_key}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 200

    # TEST CASE: account id is not provided
    ########

    # TEST CASE: account id is provided but incorrect
    url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongtoken', line.id)
    headers = {"Authorization": api_key}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 404

    # TEST CASE: access token is incorrect
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    headers = {"Authorization": 'wrongwrong'}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 400  # Invalid api_key

    # TEST CASE: access token is not provided
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    rv = api.get(url, headers=None)
    assert rv.status_code == 401  # Unauthorized

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    another_user = UserModel.query.filter_by(username='mradityanod@gmail.com').first()

    # TEST CASE: user submit with another account_id
    url = '{0}/{1}/{2}'.format(ROOT_URL, another_user.account_id, line.id)
    headers = {"Authorization": another_user.api_key}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 404


# UPDATE A LINE INTEGRATION
def test_update_channel(api, login):
    """Update line."""
    account_id = login['account_id']
    api_key = login["api_key"]

    # CREATE NEW CHANNEL
    line_url = '{0}/{1}'.format(ROOT_URL, account_id)
    line = {
        "type": "line",
        "channel_access_token": "abcdefg",
        "channel_secret": "fgbvb",
        "channel_name": "Chatkoo's Bot"
    }
    headers = {"Authorization": api_key}
    rv = post_json(api=api, url=line_url, json_dict=line, headers=headers)
    assert rv.status_code == 201

    line = ChannelModel.query.filter_by(channel_access_token="abcdefg").first()

    # BEST CASE: account id is provided
    new_line = {
        "type": "line",
        "channel_access_token": "ABCD-LINE-HIJ1",
        "channel_secret": "ABCD-LINE-HIJ2",
        "channel_name": "ABCD-LINE-HIJ3"
    }
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    headers = {"Authorization": "{0}".format(api_key)}
    rv = put_json(api, line_url, json_dict=new_line, headers=headers)
    assert rv.status_code == 200

    # TEST CASE: wrong type
    new_line = {
        "type": "messenger",
        "channel_access_token": "ABCD-LINE-HIJ1",
        "channel_secret": "ABCD-LINE-HIJ2",
        "channel_name": "ABCD-LINE-HIJ3"
    }
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    headers = {"Authorization": "{0}".format(api_key)}
    rv = put_json(api, line_url, json_dict=new_line, headers=headers)
    assert rv.status_code == 400

    # TEST CASE: account id is not provided
    ########

    # TEST CASE: account id is provided but incorrect
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongwrong', line.id)
    headers = {"Authorization": "{0}".format(api_key)}
    rv = put_json(api, line_url, json_dict=new_line, headers=headers)
    assert rv.status_code == 404  # not found

    # TEST CASE: api_key is not provided
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    rv = put_json(api, line_url, json_dict=new_line, headers=None)
    assert rv.status_code == 401  # Unauthorized

    # TEST CASE: api_key is provided but incorrect
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    headers = {"Authorization": "{0}".format('wrongtoken')}
    rv = put_json(api, line_url, json_dict=new_line, headers=headers)
    assert rv.status_code == 400  # Invalid api_key

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    another_user = UserModel.query.filter_by(username='mradityanod@gmail.com').first()

    # TEST CASE: Use another account_id
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, another_user.account_id, line.id)
    headers = {"Authorization": "{0}".format(api_key)}
    rv = put_json(api, line_url, json_dict=new_line, headers=headers)
    assert rv.status_code == 404


# DELETE LINE INTEGRATION
def test_delete_channel(api, login):
    """Test delete line integration."""
    account_id = login['account_id']
    api_key = login['api_key']

    # CREATE NEW CHANNEL
    line_url = '{0}/{1}'.format(ROOT_URL, account_id)
    line = {
        "type": "line",
        "channel_access_token": "channel2",
        "channel_secret": "channel2",
        "channel_name": "channel2"
    }
    headers = {"Authorization": api_key}
    rv = post_json(api=api, url=line_url, json_dict=line, headers=headers)
    assert rv.status_code == 201

    line = ChannelModel.query.filter_by(channel_access_token="channel2").first()

    # BEST CASE: all data provided correctly
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    headers = {"Authorization": api_key}
    s = api.delete(line_url, headers=headers)
    assert s.status_code == 204

    # TEST CASE: account id is not provided
    #####

    # TEST CASE: account id is provided but incorrect
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongwrong', line.id)
    headers = {"Authorization": api_key}
    rv = api.delete(line_url, headers=headers)
    assert rv.status_code == 404

    # TEST CASE: api_key is not provided
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    rv = api.delete(line_url, headers=None)
    assert rv.status_code == 401  # Unauthorized

    # TEST CASE: api_key is provided but incorrect
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, line.id)
    headers = {"Authorization": "wrongwrong"}
    rv = api.delete(line_url, headers=headers)
    assert rv.status_code == 400  # Bad request

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========

    another_user = UserModel.query.filter_by(username="mradityanod@gmail.com").first()

    # TEST CASE: user submit inbox_id with wrong account_id
    line_url = '{0}/{1}/{2}'.format(ROOT_URL, another_user.account_id, line.id)
    headers = {'Authorization': api_key}
    rv = api.delete(line_url, headers=headers)
    assert rv.status_code == 404
