# pylint: disable=C0103

"""Testing file for Integration API endpoint."""
import requests
import json
import pytest


ROOT_URL = 'http://localhost:8000/v2/integrations'
user = {
    'username': 'mradityanod@gmail.com',
    'password': 'kasurinirusak'
}

@pytest.fixture(scope="module")
def login():
    """Login fixture, so that it's no need login for each test."""
    login_url = 'http://localhost:8000/v2/users/login'

    r = requests.post(login_url, json=user)

    resp = {
        'account_id': r.json()['account_id'],
        'access_token': r.json()['access_token']
    }

    return resp

# CREATE ZENDESK INTEGRATION
@pytest.mark.create_zendesk
def test_create_zendesk(login):
    """Test create new zendesk."""
    account_id = login['account_id']
    access_token = login['access_token']

    zendesk = {
        "url": "chatkoo.zendesk.com",
        "email": "rizki@chatkoo.com",
        "password": "kasurinirusak"
    }

    # BEST CASE: user submit all data correctly
    url = '{}/{}/zendesk'.format(ROOT_URL, account_id)
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    s = requests.post(url, json=zendesk, headers=headers)

    assert s.status_code == 201

    # TEST CASE: user submit with incorrect account_id
    url = '{}/{}/zendesk'.format(ROOT_URL, 'wrongwrong')
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    s = requests.post(url, json=zendesk, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit with incorrect access_token
    url = '{}/{}/zendesk'.format(ROOT_URL, account_id)
    headers = {'Authorization': 'Bearer {}'.format('wrongtoken')}
    s = requests.post(url, json=zendesk, headers=headers)

    assert s.status_code == 422


# UPDATE ZENDESK INTEGRATIONS
@pytest.mark.update_zendesk
def test_update_zendesk(login):
    """Test update zendesk integration."""
    account_id = login['account_id']
    access_token = login['access_token']

    zendesk = {
        "url": "chatkoo.zendesk.com",
        "email": "mradityanod@gmail.com",
        "password": "kasurinirusak"
    }

    # Get integration id
    url = '{}/{}'.format(ROOT_URL, account_id)
    headers = {"Authorization": "Bearer {}".format(access_token)}
    s = requests.get(url, headers=headers)
    data = s.json()
    for i in data:
        if i['type'] == 'zendesk':
            zendesk_id = i['id']
            break

    # BEST CASE: user submit all data correctly
    url = '{}/{}/zendesk/{}'.format(ROOT_URL, account_id, zendesk_id)
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    s = requests.put(url, json=zendesk, headers=headers)

    assert s.status_code == 200

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}/zendesk/{}'.format(ROOT_URL, 'wrongwrong', zendesk_id)
    s = requests.put(url, json=zendesk, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect access_token
    url = '{}/{}/zendesk/{}'.format(ROOT_URL, account_id, zendesk_id)
    headers = {'Authorization': 'Bearer {}'.format('wrongtoken')}
    s = requests.put(url, json=zendesk, headers=headers)

    assert s.status_code == 422

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========

    login_url = 'http://localhost:8000/v2/users/login'
    user = {
        'username': 'ditz24159292@gmail.com',
        'password': 'kasurinirusak'
    }
    r = requests.post(login_url, json=user)
    new_account_id = r.json()['account_id']
    new_access_token = r.json()['access_token']

    # TEST CASE: user submit inbox_id with wrong account_id
    url = '{}/{}/zendesk/{}'.format(ROOT_URL, new_account_id, zendesk_id)
    headers = {'Authorization': 'Bearer {}'.format(new_access_token)}
    s = requests.put(url, json=zendesk, headers=headers)

    assert s.status_code == 404


# UPDATE ZENDESK INTEGRATIONS
@pytest.mark.delete_zendesk
def test_delete_zendesk(login):
    """Test update zendesk integration."""
    account_id = login['account_id']
    access_token = login['access_token']

    # Get integration id
    url = '{}/{}'.format(ROOT_URL, account_id)
    headers = {"Authorization": "Bearer {}".format(access_token)}
    s = requests.get(url, headers=headers)
    data = s.json()
    for i in data:
        if i['type'] == 'zendesk':
            zendesk_id = i['id']
            break

    # BEST CASE: user submit all data correctly
    url = '{}/{}/zendesk/{}'.format(ROOT_URL, account_id, zendesk_id)
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    s = requests.delete(url, headers=headers)

    assert s.status_code == 204

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}/zendesk/{}'.format(ROOT_URL, 'wrongwrong', zendesk_id)
    s = requests.delete(url, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect access_token
    url = '{}/{}/zendesk/{}'.format(ROOT_URL, account_id, zendesk_id)
    headers = {'Authorization': 'Bearer {}'.format('wrongtoken')}
    s = requests.delete(url, headers=headers)

    assert s.status_code == 422

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========

    login_url = 'http://localhost:8000/v2/users/login'
    new_user = {
        'username': 'ditz24159292@gmail.com',
        'password': 'kasurinirusak'
    }
    r = requests.post(login_url, json=new_user)
    new_account_id = r.json()['account_id']
    new_access_token = r.json()['access_token']

    # TEST CASE: user submit inbox_id with wrong account_id
    url = '{}/{}/zendesk/{}'.format(ROOT_URL, new_account_id, zendesk_id)
    headers = {'Authorization': 'Bearer {}'.format(new_access_token)}
    s = requests.delete(url, headers=headers)

    assert s.status_code == 404
