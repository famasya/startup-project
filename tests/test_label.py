# pylint: disable=C0103

"""Test file for Label API endpoint."""
import requests
import pytest
import json

ROOT_URL = 'http://localhost:8000/v2/labels'

# LOGIN
@pytest.fixture(scope="module")
def login():
    """Login fixture, so that it's no need login for each test."""
    # test login
    login_url = 'http://localhost:8000/v2/users/login'
    user = {
        'username': 'mradityanod@gmail.com',
        'password': 'kasurinirusak'
    }
    r = requests.post(login_url, json=user)
    resp = {
        'account_id': r.json()['account_id'],
        'access_token': r.json()['access_token']
    }
    return resp


# CREATE INBOX
@pytest.mark.create_inbox
def test_post_inbox(login):
    """Test create inbox."""
    account_id = login['account_id']
    access_token = login['access_token']

    inbox = {
        "short_text": "string",
        "status": "string",
        "archived": False,
        "unread_count": 0,
        "customer": {
            "id": "104-34324-110101",
            "name": "string",
            "avatar_url": "string",
            "active": True,
            "platform": "string"
        },
        "messages": {
            "id": "234-3452-4353463",
            "type": "string",
            "text": "string",
            "avatar_url": "string",
            "name": "string",
            "author_id": "string",
            "source": "string"
        }
    }
    inbox_url = 'http://localhost:8000/v2/inboxes/{}'.format(account_id)
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    r = requests.post(inbox_url, json=inbox, headers=headers)

    assert r.status_code == 201

# CREATE ISSUE
@pytest.mark.create_label
def test_create_label(login):
    """Test create a label."""
    account_id = login['account_id']
    access_token = login['access_token']

    # BEST CASE: user submit all data correctly
    label = {
        "label": "return"
    }
    url = '{}/{}'.format(ROOT_URL, account_id)
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    s = requests.post(url, json=label, headers=headers)

    assert s.status_code == 201

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}'.format(ROOT_URL, 'wrongwrong')
    s = requests.post(url, json=label, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect access_token
    url = '{}/{}'.format(ROOT_URL, account_id)
    headers = {'Authorization': 'Bearer {}'.format('wrongtoken')}
    s = requests.post(url, json=label, headers=headers)

    assert s.status_code == 422


# GET LIST ALL ISSUES
@pytest.mark.get_label_list
def test_get_label_list(login):
    """Test get list of label."""
    account_id = login['account_id']
    access_token = login['access_token']

    # BEST CASE: user submit all data correctly
    url = '{}/{}'.format(ROOT_URL, account_id)
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    s = requests.get(url, headers=headers)

    assert s.status_code == 200

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}'.format(ROOT_URL, 'wrongwrong')
    s = requests.get(url, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect access_token
    url = '{}/{}'.format(ROOT_URL, account_id)
    headers = {'Authorization': 'Bearer {}'.format('wrongtoken')}
    s = requests.get(url, headers=headers)

    assert s.status_code == 422


# GET A ISSUE
@pytest.mark.get_label_detail
def test_get_label_detail(login):
    """Get an label."""
    account_id = login['account_id']
    access_token = login['access_token']

    # Get label id
    url = '{}/{}'.format(ROOT_URL, account_id)
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    s = requests.get(url, headers=headers)
    label_id = s.json()[0]['id']

    # BEST CASE: user submit all data correctly
    url = '{}/{}/{}'.format(ROOT_URL, account_id, label_id)
    s = requests.get(url, headers=headers)

    assert s.status_code == 200

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}/{}'.format(ROOT_URL, 'wrongwrong', label_id)
    s = requests.get(url, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect access_token
    url = '{}/{}/{}'.format(ROOT_URL, account_id, label_id)
    headers = {'Authorization': 'Bearer {}'.format('wrongtoken')}
    s = requests.get(url, headers=headers)

    assert s.status_code == 422

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========

    login_url = 'http://localhost:8000/v2/users/login'
    user = {
        'username': 'ditz24159292@gmail.com',
        'password': 'kasurinirusak'
    }
    r = requests.post(login_url, json=user)
    new_account_id = r.json()['account_id']
    new_access_token = r.json()['access_token']

    # TEST CASE: user submit label_id with wrong account_id
    url = '{}/{}/{}'.format(ROOT_URL, new_account_id, label_id)
    headers = {'Authorization': 'Bearer {}'.format(new_access_token)}
    s = requests.get(url, headers=headers)

    assert s.status_code == 404


@pytest.mark.update_label
def test_update_label(login):
    """Update a label."""
    account_id = login['account_id']
    access_token = login['access_token']

    # Get label id
    url = '{}/{}'.format(ROOT_URL, account_id)
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    s = requests.get(url, headers=headers)
    label_id = s.json()[0]['id']

    # BEST CASE: user submit all data correctly
    put_label = {
        "label": "Error Payment"
    }
    url = '{}/{}/{}'.format(ROOT_URL, account_id, label_id)
    s = requests.put(url, json=put_label, headers=headers)

    assert s.status_code == 200

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}/{}'.format(ROOT_URL, 'wrongwrong', label_id)
    s = requests.put(url, json=put_label, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect access_token
    url = '{}/{}/{}'.format(ROOT_URL, account_id, label_id)
    headers = {'Authorization': 'Bearer {}'.format('wrongtoken')}
    s = requests.put(url, json=put_label, headers=headers)

    assert s.status_code == 422

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========

    login_url = 'http://localhost:8000/v2/users/login'
    user = {
        'username': 'ditz24159292@gmail.com',
        'password': 'kasurinirusak'
    }
    r = requests.post(login_url, json=user)
    new_account_id = r.json()['account_id']
    new_access_token = r.json()['access_token']

    # TEST CASE: user submit label_id with wrong account_id
    url = '{}/{}/{}'.format(ROOT_URL, new_account_id, label_id)
    headers = {'Authorization': 'Bearer {}'.format(new_access_token)}
    s = requests.put(url, json=put_label, headers=headers)

    assert s.status_code == 404


@pytest.mark.delete_label
def test_delete_label(login):
    """Delete a label."""
    account_id = login['account_id']
    access_token = login['access_token']

    # Get label id
    url = '{}/{}'.format(ROOT_URL, account_id)
    headers = {'Authorization': 'Bearer {}'.format(access_token)}
    s = requests.get(url, headers=headers)
    label_id = s.json()[0]['id']

    # BEST CASE: user submit all data correctly
    url = '{}/{}/{}'.format(ROOT_URL, account_id, label_id)
    s = requests.delete(url, headers=headers)

    assert s.status_code == 204

    # TEST CASE: user submit incorrect account_id
    url = '{}/{}/{}'.format(ROOT_URL, 'wrongwrong', label_id)
    s = requests.delete(url, headers=headers)

    assert s.status_code == 404

    # TEST CASE: user submit incorrect access_token
    url = '{}/{}/{}'.format(ROOT_URL, account_id, label_id)
    headers = {'Authorization': 'Bearer {}'.format('wrongtoken')}
    s = requests.delete(url, headers=headers)

    assert s.status_code == 422

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========

    login_url = 'http://localhost:8000/v2/users/login'
    user = {
        'username': 'ditz24159292@gmail.com',
        'password': 'kasurinirusak'
    }
    r = requests.post(login_url, json=user)
    new_account_id = r.json()['account_id']
    new_access_token = r.json()['access_token']

    # TEST CASE: user submit label_id with wrong account_id
    url = '{}/{}/{}'.format(ROOT_URL, new_account_id, label_id)
    headers = {'Authorization': 'Bearer {}'.format(new_access_token)}
    s = requests.delete(url, headers=headers)

    assert s.status_code == 404
