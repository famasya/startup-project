from flask import json

# HELPER FUNCTIONS
def post_json(api, url, json_dict, headers=None):
    """Send dictionary json_dict as a json to the specified url """
    return api.post(url, data=json.dumps(json_dict), content_type='application/json', headers=headers)

def put_json(api, url, json_dict, headers=None):
    """Send PUT request dictionary json_dict as a json to the specified url """
    return api.put(url, data=json.dumps(json_dict), content_type='application/json', headers=headers)

def delete_json(api, url, json_dict=None, headers=None):
    """Send PUT request dictionary json_dict as a json to the specified url """
    return api.delete(url, data=json.dumps(json_dict), content_type='application/json', headers=headers)

def json_response(response):
    """Decode json from response"""
    return json.loads(response.data.decode('utf8'))

def get_request(api, url, headers=None):
	"""Send get request to the URL"""
	return api.get(url, headers=headers)
