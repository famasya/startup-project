# pylint: disable=C0103

"""Test file for Users API endpoint."""
from app.modules.user.models import UserModel
from .utils import post_json, put_json, json_response

ROOT_URL = '/v2/users'

# REGISTER
def test_register(api):
    """Test create a user."""
    # BEST CASE: everything is ok
    user = {
        "username": "famasya.21@gmail.com",
        "password": "justpassword",
        "company_name": "chatkoo",
        "first_name": "rizki",
        "last_name": "aditya"
    }
    rv = post_json(api=api, url=ROOT_URL, json_dict=user, headers=None)
    assert rv.status_code == 201

    # TEST CASE: email already exist
    user = {
        "username": "famasya.21@gmail.com",
        "password": "justpassword",
        "company_name": "chatkoo",
        "first_name": "rizki",
        "last_name": "aditya"
    }
    rv = post_json(api=api, url=ROOT_URL, json_dict=user, headers=None)
    assert rv.status_code == 400

    # TEST CASE: the data is not provided
    user = {
        'username': '',
        'password': 'kasurinirusak',
        'company_name': ''
    }
    rv = post_json(api=api, url=ROOT_URL, json_dict=user, headers=None)
    assert rv.status_code == 400

    # TEST CASE: email send
    ####


# GET A USER
def test_get_user(api):
    """Test get a user."""
    # CREATE NEW USERS
    user = {
        "username": "support@chatkoo.com",
        "password": "justpassword",
        "company_name": "chatkoo",
        "first_name": "rizki",
        "last_name": "aditya"
    }
    rv = post_json(api=api, url=ROOT_URL, json_dict=user, headers=None)
    assert rv.status_code == 201

    # GET RESPONSE
    resp = json_response(rv)
    account_id = resp["account"]["id"]
    api_key = resp["api_key"]
    user_id = resp["id"]

    # BEST CASE: user submit all data correctly
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 200

    # TEST CASE: user submit with incorrect account_id
    url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongwrong', user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 404

    # TEST CASE: user submit with incorrect api_key
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, user_id)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 400

    # TEST CASE: user submit with no api_key
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, user_id)
    rv = api.get(url, headers=None)
    assert rv.status_code == 401

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    another_user = UserModel.query.filter_by(username='mradityanod@gmail.com').first()

    url = '{0}/{1}/{2}'.format(ROOT_URL, another_user.account_id, user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 404


# UPDATE USER
def test_update_user(api):
    """Test update user data."""
    # CREATE NEW USERS
    user = {
        "username": "admin@chatkoo.com",
        "password": "justpassword",
        "company_name": "chatkoo",
        "first_name": "rizki",
        "last_name": "aditya"
    }
    rv = post_json(api=api, url=ROOT_URL, json_dict=user, headers=None)
    assert rv.status_code == 201

    # GET RESPONSE
    resp = json_response(rv)
    account_id = resp["account"]["id"]
    api_key = resp["api_key"]
    user_id = resp["id"]

    # BEST CASE: everything is ok
    data = {
        "first_name": "rizki aja",
        "last_name": "aditya aja",
        "avatar_url": "example.com/avatar.png",
        "company_name": "zendesk"
    }
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = put_json(api=api, url=url, json_dict=data, headers=headers)
    assert rv.status_code == 200

    # TEST CASE: user update with username or password blank
    data = {
        "username": "",
        "password": ""
    }
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = put_json(api=api, url=url, json_dict=data, headers=headers)
    assert rv.status_code == 400

    # TEST CASE: user submit with incorrect account_id
    data = {
        "first_name": "rizki aja",
        "last_name": "aditya aja",
        "avatar_url": "example.com/avatar.png",
        "company_name": "zendesk"
    }
    url = '{0}/{1}/{2}'.format(ROOT_URL, "wrongwrong", user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = put_json(api=api, url=url, json_dict=data, headers=headers)
    assert rv.status_code == 404

    # TEST CASE: user submit with incorrect api_key
    url = '{}/{}/{}'.format(ROOT_URL, account_id, user_id)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    rv = put_json(api=api, url=url, json_dict=data, headers=headers)
    assert rv.status_code == 400

    # TEST CASE: user submit without api_key
    url = '{}/{}/{}'.format(ROOT_URL, account_id, user_id)
    rv = put_json(api=api, url=url, json_dict=data, headers=None)
    assert rv.status_code == 401

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    another_user = UserModel.query.filter_by(username='mradityanod@gmail.com').first()

    url = '{0}/{1}/{2}'.format(ROOT_URL, another_user.account_id, user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.get(url, headers=headers)
    assert rv.status_code == 404


def test_user_delete(api):
    """Test delete user."""
    # CREATE NEW USERS
    user = {
        "username": "noreply@chatkoo.com",
        "password": "justpassword",
        "company_name": "chatkoo",
        "first_name": "rizki",
        "last_name": "aditya"
    }
    rv = post_json(api=api, url=ROOT_URL, json_dict=user, headers=None)
    assert rv.status_code == 201

    # GET RESPONSE
    resp = json_response(rv)
    account_id = resp["account"]["id"]
    api_key = resp["api_key"]
    user_id = resp["id"]

    # BEST CASE: user submit all data correctly
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.delete(url, headers=headers)
    assert rv.status_code == 204

    # TEST CASE: user submit with incorrect account_id
    url = '{0}/{1}/{2}'.format(ROOT_URL, 'wrongwrong', user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.delete(url, headers=headers)
    assert rv.status_code == 404

    # TEST CASE: user submit with incorrect api_key
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, user_id)
    headers = {'Authorization': '{0}'.format('wrongtoken')}
    rv = api.delete(url, headers=headers)
    assert rv.status_code == 400

    # TEST CASE: user submit without api_key
    url = '{0}/{1}/{2}'.format(ROOT_URL, account_id, user_id)
    rv = api.delete(url, headers=None)
    assert rv.status_code == 401

    # ========== LOGIN WITH ANOTHER ACCOUNT ID ==========
    another_user = UserModel.query.filter_by(username='mradityanod@gmail.com').first()

    url = '{0}/{1}/{2}'.format(ROOT_URL, another_user.account_id, user_id)
    headers = {'Authorization': '{0}'.format(api_key)}
    rv = api.delete(url, headers=headers)
    assert rv.status_code == 404
