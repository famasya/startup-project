import requests
import subprocess
import signal
import websocket
import json
import time

URL = 'http://localhost:8000/socket/login'

def test_websocket_login(api):
	r = requests.get('http://localhost:9222/json')

	# GET websocketDebuggerUrl to do remote call
	ws = websocket.create_connection(r.json()[0]['webSocketDebuggerUrl'])
	# Send javascript evaluation through websocket
	ws.send(json.dumps({"id": 0, "method": "Runtime.evaluate", "params": {"expression": "$('#login').text()+$('#join').text()+$('#leave').text()+$('#logout').text()+$('#notauthorized').text()"}}))
	time.sleep(7)
	# Get the result
	result = json.loads(ws.recv())['result']['result']['value']
	assert result == '11111'
