import datetime, time
import jwt
import pytest
import requests, pprint
from .utils import json_response, post_json

PAYLOAD = {
    "exp": datetime.datetime.utcnow() + datetime.timedelta(seconds=5),
    "recipient_id": 3454363234324
}
LINE_PAYLOAD = {
	'to' : 12345
}

def test_jwt_token(api, key):
	# Test for messenger JWT token. Should returning status_code 200 and saved page_acess_token
    jwt_token = jwt.encode(PAYLOAD, key, algorithm="HS256").decode("utf-8")
    req = api.get('/messenger/{}'.format(jwt_token))

    PAYLOAD['data'] = LINE_PAYLOAD
    jwt_token_line = jwt.encode(PAYLOAD, key, algorithm="HS256").decode("utf-8")
    req_line = post_json(api, '/line/connector/{}'.format(jwt_token), PAYLOAD)

    response = json_response(req)

    # Should returning status_code 400 since data is incomplete. But passing JWT validation
    assert req_line.status_code == 400
    assert req.status_code == 200
    assert response['page_access_token'] == 'dsfsdfsdydrt'

def test_jwt_token_with_expiration(api, key):
	# Test for messenger expiration token. Should returning 401
	jwt_token = jwt.encode(PAYLOAD, key, algorithm="HS256").decode("utf-8")

	PAYLOAD['data'] = LINE_PAYLOAD
	jwt_token_line = jwt.encode(PAYLOAD, key, algorithm="HS256").decode("utf-8")

	time.sleep(6)
	req = api.get('/messenger/{}'.format(jwt_token))
	req_line = post_json(api, '/line/connector/{}'.format(jwt_token), PAYLOAD)

	assert req_line.status_code == 401
	assert req.status_code == 401

def test_jwt_token_with_wrong_key(api):
	# Test for messenger with wrong key. Should returning 401
	jwt_token = jwt.encode(PAYLOAD, 'syalalalla', algorithm="HS256").decode("utf-8")
	req = api.get('/messenger/{}'.format(jwt_token))

	PAYLOAD['data'] = LINE_PAYLOAD
	jwt_token_line = jwt.encode(PAYLOAD, 'key', algorithm="HS256").decode("utf-8")
	req_line = api.post('/line/connector/{}'.format(jwt_token))

	assert req_line.status_code == 401
	assert req.status_code == 401
