# rest-api-v2
---
Chatkoo is the simple way to reach customers on Messengers for customer service and sales team. It integrates multiple messaging platforms, such as: Messenger, LINE, Telegram, and more, as long as it's realtime messaging apps, into a single platform.

We define people who use Chatkoo directly or indirectly into 3 main roles:

1. **User** -> People who use Chatkoo platform. It can be customer service dept team or sales dept team in a company.
2. **Account** -> It represents single or a group of people using Chatkoo platform. For each users registered to Chatkoo for the first time, Account will be automatically created. It has 'One to many' relations to Users and Customers.
3. **Customer** -> people who contact the Users using one of the prefered messaging channel.
