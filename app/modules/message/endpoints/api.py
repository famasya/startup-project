# pylint: disable=R0201, C0103, W0611, C0111, C0301

"""List of API endpoint for Message module."""
from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ....utilities.api_limiter import api_limiter

from ..ops import (
    create_message, get_message_list, get_message_detail
)

api = Namespace('messages', description='Message related operations')

# SEND TEXT MESSAGE ON MESSENGER

post_message = api.model('Message', {
    "sender_id": fields.String,
    "recipient_id": fields.String,
    "message_type": fields.String,
    "text": fields.String
})

put_message = api.model('Update Message', {
    'source': fields.String,
    'message_type': fields.String,
    'text': fields.String,
    'attachment_url': fields.String,
    'avatar_url': fields.String,
    'sender_id': fields.String,
    'recipient_id': fields.String,
    'reply_token': fields.String,
    'archived': fields.Boolean(default=False)
})


# API METHODS
@api.doc(responses={
    200: 'OK - Everything went as planned.',
    201: 'Created',
    400: 'Bad Request - Something in your header or request body was malformed.',
    401: 'Unauthorized - Necessary credentials were either missing or invalid.',
    402: 'Payment Required - The action is not available on your payment plan, or you have exceeded usage limits for your current plan.',
    404: 'Not Found - The object you’re requesting doesn’t exist.',
    429: 'Too Many Requests - You are calling our APIs more frequently than we allow.',
    500: 'Internal Server Error'
})
@api.route('/<account_id>/<inbox_id>')
class NewMessage(Resource):
    @api.doc('list_message')
    @login_required
    def get(self, account_id, inbox_id):
        api_limiter(account_id, request.referrer)
        """List all messages per account_id."""
        start = request.args.get('start', 1)
        limit = request.args.get('limit', 10)

        resp = get_message_list(account_id, inbox_id, start, limit)

        return resp


    @api.doc('create_message')
    @api.expect(post_message)
    @login_required
    def post(self, account_id, inbox_id):
        api_limiter(account_id, request.referrer)
        """
        Send a message.
        The payload on this sample is for all type of messages and channels.
        Make sure when we post the json data, the format is adapted to:
        - Type of messages: text, attachment, quick_reply, and template
        - Source of message: messenger, line, sms, telegram

        **Messenger text format:**
        ```json
        {
          message_type: "FB_TEXT",
          text: "satu"
        },
        ```

        **Messenger image format:**

        Make sure in the end of the attachment_url should have extension file
        For image, facebook supports .png, .jpeg, and .gif

        ```json
        {
          message_type: "FB_ATTACHMENT",
          attachment_url: "http://res.cloudinary.com/dgxxta0ke/image/upload/v1497167642/ovk9iujknfaigmka8c1b.jpg"
        }
        ```

        **Messenger Quick Reply format:**

        ```json
        {
          message_type: 'FB_QUICK_REPLY',
          text: 'Hello, do you want to talk with...',
          quick_replies: [
            {
              title: 'Shaman',
              content_type: 'text',
              payload: 'Shaman'
            },
            {
              title: 'King',
              content_type: 'text',
              payload: 'King'
            }
          ]
        }
        ```

        **Messenger Generic Template format:**

        ```json
        {
          message_type: "FB_GENERIC_TEMPLATE",
          elements: [{
            image_url: "https://res.cloudinary.com/dgxxta0ke/image/upload/v1506598882/q3iqzzs1dbspls0efgfd.jpg",
            subtitle: "Hello",
            template_title: "Okeeeee",
            template_url: "https://google.com.vn",
            buttons: [{
              button_title: "Hi you",
              button_type: "web_url",
              button_url: "https://www.messenger.com"
            }]
          }]
        }
        ```

        **Messenger Button Template format:**

        ```json
        {
          message_type: 'FB_BUTTON_TEMPLATE',
          text:'what do you want to do',
          buttons: [{
            "button_type":"web_url",
            "button_url":"https://www.messenger.com",
            "button_title":"Visit Messenger"
          }]
        }        
        ```        

        **LINE text format:**
        ```json
        {
            "sender_id": "sdff45tert",
            "recipient_id": "dfsf34wressf",
            "text": "hello world",
            "message_type": "line_text"
        }
        ```

        **LINE image format:**

        The attachment_url must be:
        - Https
        - Max 1024x1024
        - Max 1 MB
        - JPEG
        unless it will be not sent properly

        ```json
        {
            "sender_id": "sdff45tert",
            "recipient_id": "dfsf34wressf",
            "attachment_url": "example.com/avatar.jpeg",
            "message_type": "line_attachment"
        }
        ```
        """
        # fetch data from json post
        data = request.json
        resp = create_message(data, account_id, inbox_id)
        return resp


# GET A MESSAGE
@api.doc(responses={
    200: 'OK - Everything went as planned.',
    201: 'Created',
    400: 'Bad Request - Something in your header or request body was malformed.',
    401: 'Unauthorized - Necessary credentials were either missing or invalid.',
    402: 'Payment Required - The action is not available on your payment plan, or you have exceeded usage limits for your current plan.',
    404: 'Not Found - The object you’re requesting doesn’t exist.',
    429: 'Too Many Requests - You are calling our APIs more frequently than we allow.',
    500: 'Internal Server Error'
})
@api.route('/<account_id>/<inbox_id>/<message_id>')
class MessageItem(Resource):
    @login_required
    def get(self, account_id, inbox_id, message_id):
        """Get a message."""
        resp = get_message_detail(account_id, inbox_id, message_id)
        return resp
