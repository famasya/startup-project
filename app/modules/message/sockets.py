# pylint: disable=C0326, C0326, C0121, E1101
"""
Each time user access to message api, then they will join to the room
of the message.
"""
import time
import uuid
import requests
from urllib.parse import urlparse
from flask import session, current_app, jsonify
from flask_socketio import emit, join_room, leave_room
from ...utilities.abort import abort
from flask_login import current_user
from .models import MessageModel, TemplateButton, MessageTemplate
from .tasks import send_message, emit_websocket_message
from .schemas import message_schema
from ..user.models import UserModel
from ..inbox.models import InboxModel, InboxSession
from ..inbox.schemas import inbox_schema
from ..inbox.sockets import (
    emit_counter_inbox, emit_websocket_inbox
)
from ..integration.models import IntegrationModel
from ..customer.models import CustomerModel
from ..channel.models import ChannelModel
from ...extensions.socketio import socketio
from ...extensions.db import db
from ...utilities.time import timestamp

WEBHOOK_URL = current_app.config['WEBHOOK_URL']


@socketio.on("connect", namespace="/messages")
def connect_handler():
    if current_user.is_authenticated:
        # Mark the user as still online
        current_user.ping()
        resp = {
            "message": "{0} is authenticated.".format(current_user.username),
            "status_code": 200
        }
        emit("authenticated", resp)
    else:
        return False


@socketio.on("join", namespace="/messages")
def on_join(data):
    """After authenticated, user join the room based on account_id."""
    room = data["inbox_id"]
    if current_user.is_authenticated and room:
        join_room(room)
        emit('status', {'message': current_user.username + ' is viewing..', 'status': 200}, room=room)


@socketio.on("leave", namespace="/messages")
def on_leave(data):
    """Leave account room each time user logout."""
    room = data["inbox_id"]
    if current_user.is_authenticated:
        leave_room(room)
        emit('status', {'message': current_user.username + ' has left the inbox.', 'status': 200}, room=room)


@socketio.on("disconnect", namespace="/messages")
def client_disconnected():
    """Update user's online when disconnected."""
    if current_user.is_authenticated:
        current_user.online = False
        current_user.save_to_db()

        # Give response
        resp = {
            "message": "{0} offline.".format(current_user.username),
            "status_code": 200
        }
        emit("disconnect", resp)


def check_format(data):
    try:
        inbox_id = data["inbox_id"]
        sender_id = data['sender_id']  # Identifier for channel
        recipient_id = data['recipient_id']  # Identifier for customer's channel
        messages = data['messages']  # list
    except (KeyError, TypeError):
        return emit("message", {"message": "Invalid request.", "status_code": 400})

    for item in messages:
        try:
            message_type = item["message_type"].upper()
        except (KeyError, TypeError):
            return emit("message", {"message": "Invalid message type.", "status_code": 400})

        client_message_id = item.get("client_message_id")  # for frontend to create temporary state

        if message_type == "FB_TEXT" or message_type == "LINE_TEXT" or message_type == "WEB_TEXT":
            try:
                text = item["text"]
            except (KeyError, TypeError):
                return emit("message", {"message": "Text is required.", "status_code": 400})

        elif message_type == "FB_ATTACHMENT" or message_type == "LINE_ATTACHMENT" or message_type == "TELEGRAM_ATTACHMENT":
            try:
                attachment_url = item["attachment_url"]
            except (KeyError, TypeError):
                return emit("message", {"message": "Attachment is required.", "status_code": 400})

        elif message_type == "FB_QUICK_REPLY":
            try:
                text = item['text']
                quick_replies = item['quick_replies']  # list
            except (KeyError, TypeError):
                return emit("message", {"message": "text and quick_replies are required.", "status_code": 400})

            for quick_reply in quick_replies:
                try:
                    title = quick_reply["title"]
                    payload = quick_reply["payload"]
                except (KeyError, TypeError):
                    return emit("message", {"message": "Invalid quick replies request.", "status_code": 400})

        elif message_type == "FB_BUTTON_TEMPLATE":
            try:
                text = item["text"]
                buttons = item["buttons"]
            except (TypeError, KeyError):
                return emit("message", {"message": "text and buttons are required.", "status_code": 400})

            for button in buttons:
                try:
                    button_type = button["type"]
                except (KeyError, TypeError):
                    return emit("message", {"message": "button_type is required.", "status_code": 400})

                if button_type == "web_url":
                    try:
                        button_url = button["url"]
                        button_title = button["title"]
                    except (KeyError, TypeError):
                        return emit("message", {"message": "Invalid buttons request.", "status_code": 400})

                    # Validate webhook url, it must be https
                    url = urlparse(button_url)
                    if url.scheme != 'https':
                        return emit("message", {"message": "button's url must be https.", "status_code": 400})

                elif button_type == "postback" or button_type == "phone_number":
                    try:
                        button_title = button["title"]
                        payload = button["payload"]
                    except (KeyError, TypeError):
                        return emit("message", {"message": "Invalid buttons requests", "status_code": 400})

                elif button_type == "element_share":
                    pass

                else:
                    return abort(400, "Invalid button type.")

        elif message_type == "FB_GENERIC_TEMPLATE":
            try:
                templates = item["message_templates"]  # list
            except (KeyError, TypeError):
                return emit("message", {"message": "Invalid templates request.", "status_code": 400})

            for template in templates:
                try:
                    template_title = template["template_title"]
                    image_url = template["image_url"]
                    subtitle = template["subtitle"]
                    template_url = template["template_url"]
                    buttons = template["buttons"]
                except (KeyError, TypeError):
                    return emit("message", {"message": "Invalid templates request.", "status_code": 400})

                for button in buttons:
                    try:
                        button_type = button["type"]
                    except (KeyError, TypeError):
                        return emit("message", {"message": "button_type is required.", "status_code": 400})
                    if button_type == "web_url":
                        try:
                            button_url = button["url"]
                            button_title = button["title"]
                        except (KeyError, TypeError):
                            return emit("message", {"message": "Invalid buttons requests", "status_code": 400})

                    elif button_type == "postback" or button_type == "phone_number":
                        try:
                            button_title = button["title"]
                            payload = button["payload"]
                        except (KeyError, TypeError):
                            return emit("message", {"message": "Invalid buttons requests", "status_code": 400})

                    elif button_type == "element_share":
                        pass
                    else:
                        return emit("message", {"message": "Invalid button_type", "status_code": 400})

        elif message_type == "LINE_TEMPLATE":
            try:
                templates = item["message_templates"]  # list
            except (KeyError, TypeError):
                return emit("message", {"message": "Invalid templates request.", "status_code": 400})

            for template in templates:
                try:
                    template_title = template["template_title"]
                    image_url = template["image_url"]
                    subtitle = template["subtitle"]
                    buttons = template["buttons"]
                except (KeyError, TypeError):
                    return emit("message", {"message": "Invalid templates request.", "status_code": 400})

                for button in buttons:
                    try:
                        button_type = button["type"]
                    except (KeyError, TypeError):
                        return emit("message", {"message": "button_type is required.", "status_code": 400})
                    if button_type == "web_url":
                        try:
                            button_url = button["url"]
                            button_title = button["title"]
                        except (KeyError, TypeError):
                            return emit("message", {"message": "Invalid buttons requests", "status_code": 400})

                    elif button_type == "postback" or button_type == "phone_number":
                        try:
                            button_title = button["title"]
                            payload = button["payload"]
                        except (KeyError, TypeError):
                            return emit("message", {"message": "Invalid buttons requests", "status_code": 400})
                    else:
                        return emit("message", {"message": "Invalid button_type", "status_code": 400})

        else:
            return emit("message", {"message": "Invalid buttons request", "status_code": 400})

@socketio.on("send_message", namespace="/messages")
def handle_message(json):
    """Receive message from client."""
    inbox_id = json["inbox_id"]
    recipient_id = json["recipient_id"]
    sender_id = json["sender_id"]
    messages = json["messages"]  # list

    # Check emit format
    check_format(json)

    # Verify api_key
    api_key = current_user.api_key
    user = UserModel.query.filter_by(api_key=api_key).first()
    account_id = user.account_id
    if user is None:
        return emit("message", {"message": "Wrong api_key", "status_code": 400})
    obj = db.session.query(InboxModel, CustomerModel).\
          filter(InboxModel.id==inbox_id).\
          filter(InboxModel.deleted==False).\
          filter(CustomerModel.id==InboxModel.customer_id).\
          filter(CustomerModel.deleted==False).first()
    try:
        inbox = getattr(obj, "InboxModel")
    except AttributeError:
        return emit("message", {"message": "Inbox not found.", "status_code": 404})

    # Get current_user to supply user's avatar_url
    avatar_url = current_user.avatar_url

    # Update inbox
    # When inbox status is closed, but user send message, it won't
    # create new inbox session, only when customer initiate new
    # conversation while the inbox is closed, will create new session.
    if inbox.status == 'open' or inbox.status == 'closed':
        inbox.status = 'inprogress'
        inbox.bot_status = False
        inbox.user_assigned = current_user.username
        inbox.save_to_db()

        # Update inbox_session first_reply and first_handling
        inbox_sessions = InboxSession.query.filter_by(inbox_id=inbox.id).all()
        for item in inbox_sessions:
            if item.open_date and item.closed_date is None:
                item.first_reply = timestamp()
                item.first_handling = current_user.username
                item.save_to_db()

        # ===========================================
        # Emit counter when status changes
        # ===========================================
        emit_counter_inbox.delay(inbox.account_id)

    if inbox.status == 'inprogress':
        inbox.user_assigned = current_user.username
        inbox.bot_status = False
        inbox.save_to_db()

        # ===========================================
        # Emit counter when status changes
        # ===========================================
        emit_counter_inbox.delay(inbox.account_id)

    message_list = []
    for message in messages:
        try:
            message_type = message["message_type"].upper()
        except (KeyError, TypeError):
            return emit("message", {"message": "Invalid request.", "status_code": 400})

        client_message_id = message.get("client_message_id")  # for frontend to create temporary state
        # Obj will be filtered by following message_type:
        # =====================
        # fb_text : messenger text message
        # fb_attachment : messenger attachment message
        # fb_quick_reply : messenger quick_reply
        # fb_generic_template : messenger generic template
        # line_text : line text message
        # line_image : line image message
        if message_type == "FB_TEXT":
            text = message["text"]

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found", "status_code": 404})
            page_access_token = channel.page_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "user_id": recipient_id,
                "text": text,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/text'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # A successful send api will return a JSON
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=user.account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.text = text
            message.save_to_db()

            # Save update inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = text
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)


        if message_type == "FB_ATTACHMENT":
            attachment_url = message["attachment_url"]

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found", "status_code": 404})
            page_access_token = channel.page_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "user_id": recipient_id,
                "attachment_url": attachment_url,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/attachment'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # A successful send api will return a JSON
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=user.account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.attachment_url = attachment_url
            message.save_to_db()

            # Update inbox short text
            inbox.last_updated = message.timestamp
            inbox.short_text = "You sent an attachment.."
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        if message_type == "FB_QUICK_REPLY":
            text = message["text"]
            quick_replies = message["quick_replies"]

            # A successful send api will return a JSON
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=user.account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.text = text
            message.save_to_db()

            # Save template message
            template = MessageTemplate(
                message_seq=message.seq
            )
            template.save_to_db()

            # Check quick reply request structure
            for item in quick_replies:
                try:
                    title = item["title"]
                    payload = item["payload"]
                except (KeyError, TypeError):
                    return emit("message", {"message": "Invalid quick replies format", "status_code": 400})

                item["content_type"] = "text"

                button = TemplateButton(
                    template_id=template.id,
                    button_title=title,
                    payload=payload
                )
                button.save_to_db()

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found", "status_code": 404})
            page_access_token = channel.page_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "quick_replies": quick_replies,
                "text": text,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/quick_reply'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # Update inbox short text
            inbox.last_updated = message.timestamp
            inbox.short_text = "[quick reply] {0}".format(text)
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        if message_type == "FB_BUTTON_TEMPLATE":
            text = message["text"]
            buttons = message["buttons"]

            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=user.account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.text = text
            message.save_to_db()


            # Save template message
            template = MessageTemplate(
                message_seq=message.seq,
                template_title=text,
            )
            template.save_to_db()

            # check the structure
            for item in buttons:
                button_type = item["type"]

                # Use this to link the button to saved replies
                # When customer click this button, then bot will
                # trigger the actions
                if button_type == "postback" or button_type == "phone_number":
                    title = item['title']
                    payload = item['payload']

                    button = TemplateButton(
                        template_id=template.id,
                        button_title=title,
                        button_type=button_type,
                        payload=payload
                    )
                    button.save_to_db()

                # Use this to redirect customer when they click it
                elif button_type == "web_url":
                    title = item['title']
                    url = item['url']

                    button = TemplateButton(
                        template_id=template.id,
                        button_url=url,
                        button_title=title,
                        button_type=button_type
                    )
                    button.save_to_db()

                # Use this to let customer share the button to their friends
                elif button_type == "element_share":
                    button = TemplateButton(
                        template_id=template.id,
                        button_type=button_type
                    )
                    button.save_to_db()

                else:
                    return emit("message", {"message": "Invalid button type.", "status_code": 400})

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found", "status_code": 404})
            page_access_token = channel.page_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================

            payload = {
                "recipient_id": recipient_id,
                "buttons": buttons,
                "text": text,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/button_template'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # Update inbox short text
            inbox.last_updated = message.timestamp
            inbox.short_text = "[button template] sent..."
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)


        if message_type == "FB_GENERIC_TEMPLATE":
            templates = message["message_templates"]  # list

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Sender not found.", "status_code": 404})
            page_access_token = channel.page_access_token

            # Save message
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=user.account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.save_to_db()

            # Check request structure
            urls = []
            for item in templates:
                title = item["template_title"]
                image_url = item["image_url"]
                subtitle = item["subtitle"]
                url = item["template_url"]
                buttons = item["buttons"]  # list

                # Save template message
                template = MessageTemplate(
                    message_seq=message.seq,
                    template_title=title,
                    image_url=image_url,
                    subtitle=subtitle,
                    template_url=url
                )
                template.save_to_db()

                for button in buttons:
                    button_type = button["type"]

                    if button_type == "web_url":
                        # Each url post as button should be whitelisted
                        # Make sure we request whitelisting to fb before
                        # We send the message to FB Send Api
                        button_url = button["url"]
                        urls.append(button_url)
                        button_title = button["title"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            button_url=button_url,
                            button_title=button_title,
                            button_type=button_type
                        )
                        button.save_to_db()

                    elif button_type == "postback" or button_type == "phone_number":
                        button_title = button["title"]
                        payload = button["payload"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            button_url=button_url,
                            button_title=button_title,
                            button_type=button_type
                        )
                        button.save_to_db()

                    elif button_type == "element_share":
                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            button_type=button_type
                        )
                        button.save_to_db()
                    elif button_type == "payment":
                        pass
                    else:
                        return emit("message", {"message": "Invalid button type.", "status_code": 400})

            # Post to FB ===================================
            # All urls should be whitelisted
            whitelist_request = {
                "whitelisted_domains": urls
            }
            whitelist_url = "{0}/messenger_profile?access_token={1}".format(current_app.config["FB_GRAPH"], page_access_token)
            rv = requests.post(whitelist_url, json=whitelist_request)
            if rv.status_code != 200:
                return emit("message", {"message": "Url can't be whitelisted, make sure it use https.", "status_code": 400})

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "elements": templates,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/generic_template'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # Update inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = "[template] sent.."
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        if message_type == "LINE_TEXT":
            text = message["text"]

            # Get channel_access_token for send authentication
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found.", "status_code": 404})
            channel_access_token = channel.channel_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "text": text,
                "channel_access_token": channel_access_token
            }
            url = '{}/line/messages/text'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # A successful send api will return a JSON
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=user.account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.text = text
            message.save_to_db()

            # Update inbox
            inbox.short_text = text
            inbox.last_updated = message.timestamp
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        if message_type == "LINE_ATTACHMENT":
            # This url must be fetched from Cloudinary
            # image url must has max: 1 MB, HTTPS, JPEG, Max: 1024x1024
            # flow: frontend filter image -> post to Cloudinary and get the secure_url -> post the url to api -> break into 2 parts
            attachment_url = message['attachment_url']

            # Get channel_access_token for send authentication
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found.", "status_code": 404})
            channel_access_token = channel.channel_access_token

            # Add key channel_access_token to payload
            payload = {
                "recipient_id": recipient_id,
                "attachment_url": attachment_url,
                "channel_access_token": channel_access_token
            }
            url = '{}/line/messages/attachment'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # A successful send api will return a JSON
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=user.account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.attachment_url = attachment_url
            message.save_to_db()

            # Update inbox short text and last_updated
            inbox.short_text = "You sent an attachment.."
            inbox.last_updated = message.timestamp
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        if message_type == "LINE_TEMPLATE":
            templates = message["message_templates"]

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found.", "status_code": 404})
            channel_access_token = channel.channel_access_token

            # Save message
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.save_to_db()

            for item in templates:
                title = item["template_title"]
                thumbnailImageUrl = item["image_url"]
                text = item["subtitle"]
                actions = item["buttons"]  # list

                # Save template message
                template = MessageTemplate(
                    message_seq=message.seq,
                    template_title=title,
                    image_url=thumbnailImageUrl,
                    subtitle=text
                )
                template.save_to_db()

                for action in actions:
                    button_type = action["type"]

                    if button_type == "web_url":
                        # Each url post as button should be whitelisted
                        # Make sure we request whitelisting to fb before
                        # We send the message to FB Send Api
                        uri = action["url"]
                        label = action["title"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            button_url=uri,
                            button_title=label,
                            button_type=button_type
                        )
                        button.save_to_db()

                    elif button_type == "postback":
                        label = action["title"]
                        data = action["payload"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            payload=data,
                            button_title=label,
                            button_type=button_type
                        )
                        button.save_to_db()
                    else:
                        return emit("message", {"message": "Invalid button_type.", "status_code": 400})

            # ===========================================================================
            # If templates has only one item, then we send it as only template in LINE
            # If templates has several items on it, then we send it as carousel in LINE
            # We want to make the structure of the request consistent, while we also try to
            # Solve the differences in request API of messenger and LINE@
            # ===========================================================================
            if len(templates) == 1:
                # =============================================
                # SENDING TO WEBHOOK URL LINE TEMPLATE
                # =============================================
                payload = {
                    "recipient_id": recipient_id,
                    "elements": templates,
                    "channel_access_token": channel_access_token
                }
                url = '{}/line/messages/line_template'.format(WEBHOOK_URL)
                send_message.delay(url, payload)
            else:
                # =============================================
                # SENDING TO WEBHOOK URL LINE CAROUSEL
                # =============================================
                payload = {
                    "recipient_id": recipient_id,
                    "elements": templates,
                    "channel_access_token": channel_access_token
                }
                url = '{}/line/messages/line_carousel'.format(WEBHOOK_URL)
                send_message.delay(url, payload)

            # Update inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = "{0} sending a line tempalate".format(current_user.first_name)
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        if message_type == "LINE_IMAGE_CAROUSEL":
            templates = message["message_templates"]

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            channel_access_token = channel.channel_access_token

            # Save message
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.save_to_db()

            for item in templates:
                ImageUrl = item["image_url"]
                actions = item["buttons"]  # list

                # Save template message
                template = MessageTemplate(
                    message_seq=message.seq,
                    image_url=ImageUrl
                )
                template.save_to_db()

                for action in actions:
                    button_type = action["type"]

                    if button_type == "web_url":
                        # Each url post as button should be whitelisted
                        # Make sure we request whitelisting to fb before
                        # We send the message to FB Send Api
                        uri = action["url"]
                        label = action["title"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            button_url=uri,
                            button_title=label,
                            button_type=button_type
                        )
                        button.save_to_db()

                    elif button_type == "postback":
                        label = action["title"]
                        data = action["payload"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            payload=data,
                            button_title=label,
                            button_type=button_type
                        )
                        button.save_to_db()
                    else:
                        return abort(400, "Invalid button type")

            # =============================================
            # SENDING TO WEBHOOK URL LINE CAROUSEL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "elements": templates,
                "channel_access_token": channel_access_token
            }
            url = '{}/line/messages/line_image_carousel'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # Update inbox
            inbox.last_updated = message.timestamp

            # Update inbox short text and last_updated
            inbox.short_text = "You sent an image carousel.."
            inbox.last_updated = message.timestamp
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        if message_type == "TELEGRAM_TEXT":
            text = message["text"]

            # Get channel_access_token for send authentication
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found.", "status_code": 404})
            token = channel.token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "text": text,
                "token": token
            }
            url = '{}/telegram/messages/text'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # A successful send api will return a JSON
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=user.account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.text = text
            message.save_to_db()

            # Update inbox
            inbox.short_text = text
            inbox.last_updated = message.timestamp
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        if message_type == "TELEGRAM_ATTACHMENT":
            attachment_url = message["attachment_url"]

            # Get channel_access_token for send authentication
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found.", "status_code": 404})
            token = channel.token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "attachment_url": attachment_url,
                "token": token
            }
            url = '{}/telegram/messages/attachment'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # A successful send api will return a JSON
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=user.account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.attachment_url = attachment_url
            message.save_to_db()

            # Update inbox
            inbox.short_text = "You sent an attachment.."
            inbox.last_updated = message.timestamp
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        if message_type == "WEB_TEXT":
            """We use smooch for this type of message."""
            text = message["text"]

            # Get app id
            smooch = IntegrationModel.query.\
                     filter_by(account_id=account_id).\
                     filter_by(app_id=sender_id).\
                     filter_by(deleted=False).first()
            if smooch is None:
                return abort(404, "Channel not found.")
            sender_id = smooch.app_id
            secret = smooch.secret
            key_id = smooch.key_id

            # ===============================================
            # Generate jwt for smooch auth
            # ===============================================
            import jwt
            encoded_jwt = jwt.encode({
                "scope": "app"
            },
            secret,
            headers={
                "alg": 'HS256',
                "typ": 'JWT',
                "kid": key_id
            })
            auth = encoded_jwt.decode('utf-8')

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "app_user": recipient_id,
                "text": text,
                "app_id": sender_id,
                "auth": auth
            }
            url = '{}/smooch/messages/text'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # A successful send api will return a JSON
            message = MessageModel(
                id=uuid.uuid1().hex,
                message_type=message_type,
                avatar_url=avatar_url,
                sender_id=sender_id,
                recipient_id=recipient_id,
                account_id=account_id,
                inbox_id=inbox_id,
                status='sent',
                author="{} {}".format(current_user.first_name, current_user.last_name)
            )
            message.text = text
            message.save_to_db()

            # Update Inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = text
            inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            data_message = message_schema.dump(message).data
            data_message["client_message_id"] = client_message_id
            data_inbox = inbox_schema.dump(inbox).data
            emit_websocket_inbox.delay(data_inbox)

            # Injext inbox and message
            message_list.append(data_message)

        # Delay loop 1 s
        time.sleep(1)

    # Emit new message
    emit_websocket_message.delay(message_list, inbox_id)
