# pylint: disable=E1101, C0103, W0611, W0622, R0903

"""
MESSAGE MODELS
~~~~~~~~~~~~~~
This models is a home for multiple messaging platform data.
Make sure always take a note whenever you change the formats

Messenger:
Whenever a message created by Messenger webhook, it must contains sender_id and recipient_id.
The IDs is not expired, we can use it anytime for sending message back.

LINE:
Whenever a message created by LINE webhook, it must contains replyToken.
This token is used for replying the message, unfortunately, it will expired anytime soon,
It can be a problem because Chatkoo user probably take time to reply manually.
Alternately, we can use Push Message, but make sure PUSH_REPLY is activated
on Messaging API of the account.
"""
import datetime
import flask_whooshalchemy as wa
from flask import abort, current_app

from app.extensions.db import db
from app.utilities.time import timestamp


class TemplateButton(db.Model):
    """Define button for message type template."""
    __tablename__ = 'message_template_buttons'

    template_id = db.Column(db.Integer, db.ForeignKey('message_templates.id'))
    id = db.Column(db.Integer, primary_key=True)

    # Messenger
    button_url = db.Column(db.String)
    button_title = db.Column(db.String)
    button_type = db.Column(db.String)
    payload = db.Column(db.String)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
           db.session.rollback()
           raise


class MessageTemplate(db.Model):
    """Defind Messennger message type template."""
    __tablename__ = 'message_templates'

    message_seq = db.Column(db.Integer, db.ForeignKey('messages.seq'))
    id = db.Column(db.Integer, primary_key=True)

    # Messenger
    template_title = db.Column(db.String)
    image_url = db.Column(db.String)
    subtitle = db.Column(db.String)
    template_url = db.Column(db.String)

    buttons = db.relationship('TemplateButton', backref='message_template', lazy='dynamic')

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
           db.session.rollback()
           raise



class MessageModel(db.Model):
    """Defind MessageModel."""
    __tablename__ = 'messages'
    __searchable__ = ["text"]

    account_id = db.Column(db.String, db.ForeignKey('accounts.id'))
    inbox_id = db.Column(db.String, db.ForeignKey('inboxes.id'))

    id = db.Column(db.String) # for messenger, it must be mid
    seq = db.Column(db.Integer, primary_key=True, autoincrement=True) # for sequence of the message
    status = db.Column(db.String)  # sent or received
    message_type = db.Column(db.String) # it can be text, attachment, quick reply, or template
    avatar_url = db.Column(db.String) # avatar of who send the message
    author = db.Column(db.String)  # author of the message

    sender_id = db.Column(db.String(80)) # it can be user_id or page_id
    recipient_id = db.Column(db.String) # it can be user_id or page_id

    text = db.Column(db.String, nullable=True)
    attachment_url = db.Column(db.String, nullable=True) # it can be image or file

    deleted = db.Column(db.Boolean, default=False)
    timestamp = db.Column(db.Integer, default=timestamp)

    message_templates = db.relationship('MessageTemplate', backref='message', lazy='dynamic')


    def __repr__ (self):
        return '<MessageModel (status=%r, timestamp=%s)>' % (self.status, self.timestamp)


    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
           db.session.rollback()
           raise

# Register search index capability
wa.whoosh_index(current_app, MessageModel)

class MessageLogModel(db.Model):
    """Defind Messennger logs."""
    __tablename__ = 'message_logs'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True) # for messenger, it must be mid
    mid = db.Column(db.String)
    type = db.Column(db.String(20))
    session = db.Column(db.String)
    watermark = db.Column(db.String)
    campaign_id = db.Column(db.Integer)
    saved_reply_id = db.Column(db.Integer)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
           db.session.rollback()
           raise
