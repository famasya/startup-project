# pylint: disable=E1101, C0103, W0611, W0622, R0903, C0111, C0301, R0913

"""Schema for Message API endpoints"""
from flask import abort
from marshmallow import fields, post_dump
from flask_login import current_user
from .models import MessageModel, TemplateButton, MessageTemplate
from ...extensions.schema import ma


def message_object_list(results, url, start, limit):
    """Returning object for variant of pagination below."""
    start = int(start)
    limit = int(limit)

    obj = {}
    # check if page exists
    if len(results) == 0:
        obj['count'] = 0
        obj['start'] = start
        obj['limit'] = limit
        obj['results'] = []
        obj['previous'] = ''
        obj['next'] = ''

        return obj

    # make response
    obj['start'] = start
    obj['limit'] = limit

    sorted_results = [x for x in reversed(results)]

    # check if page exists
    count = len(sorted_results)
    if count < start:
        return abort(404, "Start exceeds total of the results.")

    obj['count'] = count

    # make URLs
    # make previous url
    if start == 1:
        obj['previous'] = ''

    start_copy = max(1, start - limit)
    limit_copy = start - 1
    obj['previous'] = url + '?start=%d&limit=%d' % (start_copy, limit_copy)

    # make next url
    if start + limit > count:
        obj['next'] = ''

    start_copy = start + limit
    obj['next'] = url + '?start=%d&limit=%d' % (start_copy, limit)

    # finally extract result according to bounds
    obj['results'] = sorted_results[(start - 1):(start - 1 + limit)]

    return obj


class TemplateButtonSchema(ma.ModelSchema):
    """Content Schema."""
    class Meta:
        model = TemplateButton
        exclude = ["template_id", "message_template"]


class TemplateSchema(ma.ModelSchema):
    """Content Schema."""
    class Meta:
        model = MessageTemplate
        exclude = ["message"]

    buttons = ma.Nested("TemplateButtonSchema", many=True)


class MessageSchema(ma.ModelSchema):
    """Content Schema."""
    class Meta:
        model = MessageModel
        exclude = [
            'account',
            'archived',
            'deleted'
        ]
    message_templates = ma.Nested("TemplateSchema", many=True)

    # Modify post serializing proccess
    @post_dump
    def post_process(self, in_data):
        """Register modify()."""
        modify_data = self.modify(in_data)

    # Add new author name for status message 'sent'
    def modify(self, in_data):
        """Modify json response for message."""
        # Insert inbox_id on the response
        in_data["inbox_id"] = in_data["inbox"]
        in_data.pop("inbox", None)

        # Filter response by status
        if in_data["status"] == "sent":
            if in_data["message_type"] == "FB_TEXT":
                in_data.pop("attachment_url", None)
                in_data.pop("message_templates", None)

            if in_data["message_type"] == "FB_ATTACHMENT":
                in_data.pop("text", None)
                in_data.pop("message_templates", None)

            if in_data["message_type"] == "FB_QUICK_REPLY":
                in_data.pop("attachment_url", None)
                # Drop null on button
                buttons = []
                in_data["quick_buttons"] = in_data.pop("message_templates")
                for item in in_data["quick_buttons"]:
                    item.pop("image_url", None)
                    item.pop("subtitle", None)
                    item.pop("template_url", None)
                    item.pop("template_title", None)
                    for button in item["buttons"]:
                        button.pop("button_type")
                        button.pop("button_url")
                        buttons.append(button)
                in_data["quick_buttons"] = buttons

            if in_data["message_type"] == "FB_BUTTON_TEMPLATE":
                in_data.pop("attachment_url", None)
                # Drop null on button
                for item in in_data["message_templates"]:
                    item.pop("image_url", None)
                    item.pop("subtitle", None)
                    item.pop("template_url", None)
                    for button in item["buttons"]:
                        button["type"] = button.pop("button_type")

                        if button["type"] == "postback" or button["type"] == "phone_number":
                            button.pop("button_url", None)
                            button["title"] = button.pop("button_title")
                        if button["type"] == "web_url":
                            button["title"] = button.pop("button_title")
                            button["url"] = button.pop("button_url")
                            button.pop("payload", None)
                        if button["type"] == "element_share":
                            button.pop("button_title", None)
                            button.pop("button_url", None)

            if in_data["message_type"] == "FB_GENERIC_TEMPLATE":
                in_data.pop("text", None)
                in_data.pop("attachment_url", None)

                # Drop null on button
                for item in in_data["message_templates"]:
                    for button in item["buttons"]:
                        button["type"] = button.pop("button_type")

                        if button["type"] == "postback" or button["type"] == "phone_number":
                            button.pop("button_url", None)
                            button["title"] = button.pop("button_title")
                        if button["type"] == "web_url":
                            button["title"] = button.pop("button_title")
                            button["url"] = button.pop("button_url")
                            button.pop("payload", None)
                        if button["type"] == "element_share":
                            button.pop("button_title", None)
                            button.pop("button_url", None)

            if in_data["message_type"] == "LINE_TEXT":
                in_data.pop("attachment_url", None)
                in_data.pop("message_templates", None)
            if in_data["message_type"] == "LINE_ATTACHMENT":
                in_data.pop("text", None)
                in_data.pop("message_templates", None)

            if in_data["message_type"] == "LINE_TEMPLATE":
                in_data.pop("text", None)
                in_data.pop("attachment_url", None)

                # Drop null on button
                for item in in_data["message_templates"]:
                    item.pop("template_url")
                    for button in item["buttons"]:
                        button["type"] = button.pop("button_type")

                        if button["type"] == "postback":
                            button.pop("button_url", None)
                            button["title"] = button.pop("button_title")
                        if button["type"] == "web_url":
                            button["title"] = button.pop("button_title")
                            button["url"] = button.pop("button_url")
                            button.pop("payload", None)

            if in_data["message_type"] == "LINE_IMAGE_CAROUSEL":
                in_data.pop("text", None)
                in_data.pop("attachment_url", None)

                # Drop null on button
                for item in in_data["message_templates"]:
                    item.pop("subtitle", None)
                    item.pop("template_title", None)
                    item.pop("template_url", None)
                    for button in item["buttons"]:
                        button["type"] = button.pop("button_type")

                        if button["type"] == "postback":
                            button.pop("button_url", None)
                            button["title"] = button.pop("button_title")
                        if button["type"] == "web_url":
                            button["title"] = button.pop("button_title")
                            button["url"] = button.pop("button_url")
                            button.pop("payload", None)

        if in_data["status"] == "received":
            if in_data["message_type"] == "FB_TEXT":
                in_data.pop("attachment_url", None)
                in_data.pop("message_templates", None)
            if in_data["message_type"] == "FB_ATTACHMENT":
                in_data.pop("text", None)
                in_data.pop("message_templates", None)
            if in_data["message_type"] == "LINE_TEXT":
                in_data.pop("attachment_url", None)
                in_data.pop("message_templates", None)
            if in_data["message_type"] == "LINE_ATTACHMENT":
                in_data.pop("text", None)
                in_data.pop("message_templates", None)


message_schema = MessageSchema()
messages_schema = MessageSchema(many=True)


class PageSchema(ma.Schema):
    """Define pagination schema"""
    class Meta:
        """Custom fields for pagination, results will contain Message list."""
        fields = ('start', 'limit', 'previous', 'next', 'count', 'results')

    results = ma.Nested('MessageSchema', many=True)

page_schema = PageSchema()
