# pylint: disable=W0622, C0103, R0914, C0301, E1101

"""Operations for Message API.

Request will be passed to the endpoint of each channel, it's located on
webhook-v2.

Basically, we have 2 methods of sending message, via request API and websocket.
This API can be used for third party integration only because client use
websocket for sending messages.

**Inbox Status**

When user for the first time reply message on Inbox with status 'open',
then inbox will automatically update to 'inprogress'.
"""
import uuid
import requests
import time
from urllib.parse import urlparse
from celery.utils.log import get_logger
from flask import current_app, session, jsonify
from flask_login import current_user
from ...utilities.abort import abort

from .tasks import send_message
from .models import MessageModel, MessageTemplate, TemplateButton
from .schemas import message_schema, page_schema, message_object_list
from ..inbox.sockets import (
    emit_counter_inbox, emit_websocket_inbox
)
from ..inbox.schemas import inbox_schema
from ..message.tasks import send_message, emit_websocket_message
from ..account.models import AccountModel
from ..channel.models import ChannelModel
from ..integration.models import IntegrationModel
from ..inbox.models import InboxModel, UserViewed, InboxSession
from ..customer.models import CustomerModel
import uuid
from ...extensions.db import db
from ...utilities.time import timestamp


def check_format(data):
    try:
        sender_id = data['sender_id']  # Identifier for channel
        recipient_id = data['recipient_id']  # Identifier for customer's channel
        messages = data['messages']  # list
    except (KeyError, TypeError):
        return abort(400, "Invalid request")

    for item in messages:
        try:
            message_type = item["message_type"].upper()
        except (KeyError, TypeError):
            return abort(400, "message_type is required.")

        if message_type == "FB_TEXT" or message_type == "LINE_TEXT" or message_type == "WEB_TEXT" or message_type == "TELEGRAM_TEXT":
            try:
                text = item["text"]
            except (KeyError, TypeError):
                return abort(400, "text is required.")

        elif message_type == "FB_ATTACHMENT" or message_type == "LINE_ATTACHMENT" or message_type == "TELEGRAM_ATTACHMENT":
            try:
                attachment_url = item["attachment_url"]
            except (KeyError, TypeError):
                return abort(400, "attachment_url is required.")

        elif message_type == "FB_QUICK_REPLY":
            try:
                text = item['text']
                quick_replies = item['quick_replies']  # list
            except (KeyError, TypeError):
                return abort(400, "text and quick_replies are required.")

            for quick_reply in quick_replies:
                try:
                    title = quick_reply["title"]
                    payload = quick_reply["payload"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid quick_replies request.")

        elif message_type == "FB_BUTTON_TEMPLATE":
            try:
                text = item["text"]
                buttons = item["buttons"]
            except (TypeError, KeyError):
                return abort(400, "text and buttons are required.")

            for button in buttons:
                try:
                    button_type = button["type"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid button type request.")

                if button_type == "web_url":
                    try:
                        button_url = button["url"]
                        button_title = button["title"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button request.")

                    # Validate webhook url, it must be https
                    url = urlparse(button_url)
                    if url.scheme != 'https':
                        return abort(400, "Invalid webhook url, it must be https.")

                elif button_type == "postback" or button_type == "phone_number":
                    try:
                        button_title = button["title"]
                        payload = button["payload"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button request.")

                elif button_type == "element_share":
                    pass

                else:
                    return abort(400, "Invalid button type.")

        elif message_type == "FB_GENERIC_TEMPLATE":
            try:
                templates = item["message_templates"]  # list
            except (KeyError, TypeError):
                return abort(400, "Invalid templates requests.")

            for template in templates:
                try:
                    template_title = template["template_title"]
                    image_url = template["image_url"]
                    subtitle = template["subtitle"]
                    template_url = template["template_url"]
                    buttons = template["buttons"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid templates request.")

                for button in buttons:
                    try:
                        button_type = button["type"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button type requests.")
                    if button_type == "web_url":
                        try:
                            button_url = button["url"]
                            button_title = button["title"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                    elif button_type == "postback" or button_type == "phone_number":
                        try:
                            button_title = button["title"]
                            payload = button["payload"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                    elif button_type == "element_share":
                        pass
                    else:
                        return abort(400, "Invalid button type.")

        elif message_type == "LINE_TEMPLATE":
            try:
                templates = item["message_templates"]  # list
            except (KeyError, TypeError):
                return abort(400, "Invalid templates requests.")

            for template in templates:
                try:
                    template_title = template["template_title"]
                    thumbnailImageUrl = template["image_url"]
                    text = template["subtitle"]
                    actions = template["buttons"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid templates request.")

                for action in actions:
                    try:
                        button_type = action["type"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button type requests.")
                    if button_type == "web_url":
                        try:
                            uri = action["url"]
                            label = action["title"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                    elif button_type == "postback":
                        try:
                            label = action["title"]
                            data = action["payload"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")
                    else:
                        return abort(400, "Invalid button type.")

        elif message_type == "LINE_IMAGE_CAROUSEL":
            try:
                templates = item["message_templates"]  # list
            except (KeyError, TypeError):
                return abort(400, "Invalid templates requests.")

            for template in templates:
                try:
                    thumbnailImageUrl = template["image_url"]
                    actions = template["buttons"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid templates request.")

                if len(actions) > 1:
                    return abort(400, "Multiple buttons are not allowed for this type of message.")

                for action in actions:
                    try:
                        button_type = action["type"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button type requests.")
                    if button_type == "web_url":
                        try:
                            uri = action["url"]
                            label = action["title"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                    elif button_type == "postback":
                        try:
                            label = action["title"]
                            data = action["payload"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")
                    else:
                        return abort(400, "Invalid button type.")

        else:
            return abort(400, "Invalid message_type")


def get_message_list(account_id, inbox_id, start, limit):
    """Get list of messages with pagination."""
    obj = db.session.query(AccountModel, InboxModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(InboxModel.id==inbox_id).\
          filter(InboxModel.deleted==False).first()
    try:
        account = getattr(obj, "AccountModel")
        inbox = getattr(obj, "InboxModel")
    except AttributeError:
        return abort(404, "Account or Inbox not found.")

    results = MessageModel.query.filter_by(account_id=account_id).filter_by(inbox_id=inbox_id).all()
    page_dump = message_object_list(
        results=results,
        url='/v2/messages/{}'.format(account_id),
        start=start,
        limit=limit
    )

    # Update user_viewed ===================================
    # Update the UserViewed whenever current_user request this list
    user_viewed = UserViewed.query.filter_by(user=current_user.username).first()
    if user_viewed is None:
        user_viewed = UserViewed(
            inbox_id=inbox_id,
            user=current_user.username,
            avatar_url=current_user.avatar_url
        )
        try:
            db.session.add(user_viewed)
            db.session.commit()
        except:
            db.session.rollback()
            raise
    # =======================================================

    resp = page_schema.jsonify(page_dump)
    resp.status_code = 200

    return resp


def get_message_detail(account_id, inbox_id, message_id):
    """get message detail"""
    # Get related object
    obj = db.session.query(AccountModel, InboxModel, MessageModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(InboxModel.id==inbox_id).\
          filter(InboxModel.deleted==False).\
          filter(MessageModel.id==message_id).\
          filter(MessageModel.deleted==False).first()
    try:
        account = getattr(obj, "AccountModel")
        inbox = getattr(obj, "InboxModel")
        message = getattr(obj, "MessageModel")
    except AttributeError:
        return abort(404, "Message not found.")

    # Inbox should be related to Account
    if account.id != inbox.account_id:
        return abort("Account and Inbox didn't match.")

    # Message should be related to Inbox
    if inbox.id != message.inbox_id:
        return abort(404, "Message and Inbox didn't match.")

    resp = message_schema.jsonify(message)
    resp.status_code = 200
    return resp


def create_message(data, account_id, inbox_id, is_bot=False, is_shortlink=False, user_data=None, session=None, campaign_id=None):
    """Create new message."""
    # App context to fetch webhook url from config
    WEBHOOK_URL = current_app.config['WEBHOOK_URL']

    # ================================================================
    # Check format, so that when invalid request, it won't save to db.
    # ================================================================
    check_format(data)

    sender_id = data['sender_id']  # Identifier for channel
    recipient_id = data['recipient_id']  # Identifier for customer's channel
    messages = data['messages']



    # Get related obj
    obj = db.session.query(AccountModel, InboxModel, CustomerModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(InboxModel.id==inbox_id).\
          filter(InboxModel.deleted==False).\
          filter(InboxModel.account_id==AccountModel.id).\
          filter(CustomerModel.id==InboxModel.customer_id).\
          filter(CustomerModel.deleted==False).first()
    try:
        inbox = getattr(obj, "InboxModel")
        customer = getattr(obj, "CustomerModel")
    except AttributeError:
        return abort(404, "Inbox or Customer not found.")

    # Get current_user to supply user's avatar_url
    if is_bot == False and is_shortlink == False:
        avatar_url = current_user.avatar_url
    else:
        avatar_url = "https://res.cloudinary.com/dgxxta0ke/image/upload/v1507283176/15045673_1209929185739936_367901151_n_pbfgxt.jpg"

    # Update status and user_assigned
    if inbox.status == 'open' or inbox.status == 'closed':
        if inbox.status == 'closed':
            inbox.bot_status = False
        inbox.status = 'inprogress'

        if is_bot == False and is_shortlink == False:
            inbox.user_assigned = current_user.username
            inbox.save_to_db()

            # Update inbox_session first_reply and first_handling
            inbox_sessions = InboxSession.query.filter_by(inbox_id=inbox.id).all()
            for item in inbox_sessions:
                if item.open_date and item.closed_date is None:
                    item.first_reply = timestamp()
                    item.first_handling = current_user.username
                    item.save_to_db()
        else:
            inbox.user_assigned = "Bot"
            inbox.save_to_db()

        # ===========================================
        # Emit counter when status changes
        # ===========================================
        emit_counter_inbox.delay(inbox.account_id)

    responses = []
    for message in messages:
        message_type = message["message_type"].upper()

        if user_data is not None:
            author = "{} {}".format(user_data["first_name"], user_data["last_name"])
        elif user_data is None and is_bot is False:
            author = "{} {}".format(current_user.first_name, current_user.last_name)
        else:
            author = "Bot"

        # Obj will be filtered by following message_type:
        # =====================
        # fb_text : messenger text message
        # fb_attachment : messenger attachment message
        # line_text : line text message
        # line_image : line image message
        if message_type == 'FB_TEXT':
            # fetch text from request
            text = message['text']
            reply_id = message['id']

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            page_access_token = channel.page_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "user_id": recipient_id,
                "text": text,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/text?reply_id={}&session={}&campaign_id={}'.format(WEBHOOK_URL, reply_id, session, campaign_id)
            send_message.delay(url, payload)

            if is_bot == False:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.text = text
                message.save_to_db()
            else:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.text = text
                message.save_to_db()

            # Update Inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = text
            inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        # Check if source is line, then send to line send endpoint
        if message_type == 'FB_ATTACHMENT':
            # fetch attachment_url from request
            attachment_url = message['attachment_url']

            # Get page_access_token
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            page_access_token = channel.page_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "user_id": recipient_id,
                "attachment_url": attachment_url,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/attachment'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            if is_bot == False:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.attachment_url = attachment_url
                message.save_to_db()
            else:
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.attachment_url = attachment_url
                message.save_to_db()

            # Update inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = "You sent an attachment.."
            inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)


        if message_type == 'FB_QUICK_REPLY':
            # fetch text from request
            text = message['text']
            quick_replies = message['quick_replies']

            if is_bot == False:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.text = text
                message.save_to_db()
            else:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.text = text
                message.save_to_db()

            # Save template message
            template = MessageTemplate(
                message_seq=message.seq
            )
            template.save_to_db()

            # Check quick reply request structure
            for item in quick_replies:
                try:
                    title = item["title"]
                    payload = item["payload"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid requests.")

                item["content_type"] = "text"

                button = TemplateButton(
                    template_id=template.id,
                    button_title=title,
                    payload=payload
                )
                button.save_to_db()

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            page_access_token = channel.page_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "quick_replies": quick_replies,
                "text": text,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/quick_reply'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # Update inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = "[quick reply] {0}".format(text)
            inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        if message_type == "FB_BUTTON_TEMPLATE":
            text = message["text"]
            buttons = message["buttons"]

            if is_bot == False:
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.text = text
                message.save_to_db()
            else:
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.text = text
                message.save_to_db()


            # Save template message
            template = MessageTemplate(
                message_seq=message.seq,
                template_title=text,
            )
            template.save_to_db()

            # check the structure
            for item in buttons:
                button_type = item["type"]
                # Use this to link the button to saved replies
                # When customer click this button, then bot will
                # trigger the actions

                if button_type == "postback" or button_type == "phone_number":
                    title = item['title']
                    payload = item['payload']

                    button = TemplateButton(
                        template_id=template.id,
                        button_title=title,
                        button_type=button_type,
                        payload=payload
                    )
                    button.save_to_db()
                # Use this to redirect customer when they click it
                elif button_type == "web_url":
                    title = item['title']
                    url = item['url']

                    button = TemplateButton(
                        template_id=template.id,
                        button_url=url,
                        button_title=title,
                        button_type=button_type
                    )
                    button.save_to_db()

                # Use this to let customer share the button to their friends
                elif button_type == "element_share":
                    button = TemplateButton(
                        template_id=template.id,
                        button_type=button_type
                    )
                    button.save_to_db()

                else:
                    return emit("message", {"message": "Invalid button type.", "status_code": 400})

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return emit("message", {"message": "Channel not found", "status_code": 404})
            page_access_token = channel.page_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================

            payload = {
                "recipient_id": recipient_id,
                "buttons": buttons,
                "text": text,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/button_template'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # Update inbox short text
            inbox.last_updated = message.timestamp

            if is_bot == False and is_shortlink == False:
                inbox.short_text = "{0} sending a quick reply".format(current_user.first_name)
                inbox.save_to_db()
            else:
                inbox.short_text = "Bot is sending a quick reply"
                inbox.save_to_db()

            # ===========================================
            # Emit back to client using celery
            # ===========================================
            message = message_schema.dump(message).data

            # Injext inbox and message
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        if message_type == "FB_GENERIC_TEMPLATE":
            templates = message["message_templates"]  # list

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            page_access_token = channel.page_access_token

            if is_bot == False:
                # Save message
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.save_to_db()
            else:
                # Save message
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.save_to_db()

            # Check request structure
            urls = []  # List of url for whitelisting
            for item in templates:
                title = item["template_title"]
                image_url = item["image_url"]
                subtitle = item["subtitle"]
                url = item["template_url"]
                buttons = item["buttons"]  # list

                # Save template message
                template = MessageTemplate(
                    message_seq=message.seq,
                    template_title=title,
                    image_url=image_url,
                    subtitle=subtitle,
                    template_url=url
                )
                template.save_to_db()
                urls.append(image_url)
                for button in buttons:
                    button_type = button["type"]

                    if button_type == "web_url":
                        # Each url post as button should be whitelisted
                        # Make sure we request whitelisting to fb before
                        # We send the message to FB Send Api
                        button_url = button["url"]
                        button_title = button["title"]

                        urls.append(button_url)

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            button_url=button_url,
                            button_title=button_title,
                            button_type=button_type
                        )
                        button.save_to_db()

                    elif button_type == "postback" or button_type == "phone_number":
                        button_title = button["title"]
                        payload = button["payload"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            payload=payload,
                            button_title=button_title,
                            button_type=button_type
                        )
                        button.save_to_db()

                    elif button_type == "element_share":
                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            button_type=button_type
                        )
                        button.save_to_db()
                    elif button_type == "payment":
                        pass
                    else:
                        return abort(400, "Invalid button type")

            # Post to FB ===================================
            # All urls should be whitelisted
            if len(urls) > 0:
                whitelist_request = {
                    "whitelisted_domains": urls
                }
                whitelist_url = "{0}/messenger_profile?access_token={1}".format(current_app.config["FB_GRAPH"], page_access_token)
                rv = requests.post(whitelist_url, json=whitelist_request)
                if rv.status_code != 200:
                    return abort(400, "Url can't be whitelisted, make sure it use https.")

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "elements": templates,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/generic_template'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # Update inbox
            inbox.last_updated = message.timestamp

            if is_bot == False and is_shortlink == False:
                inbox.short_text = "{0} sending a generic tempalate".format(current_user.first_name)
                inbox.save_to_db()
            else:
                inbox.short_text = "Bot sending a generic tempalate"
                inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        if message_type == 'LINE_TEXT':
            # fetch text from request
            text = message['text']

            # Get channel_access_token for send authentication
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            channel_access_token = channel.channel_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "text": text,
                "channel_access_token": channel_access_token
            }
            url = '{}/line/messages/text'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            if is_bot == False:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.text = text
                message.save_to_db()
            else:
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.text = text
                message.save_to_db()

            # Update inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = text
            inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        if message_type == 'LINE_ATTACHMENT':
            # fetch text from request
            # This url must be fetched from Cloudinary
            # image url must has max: 1 MB, HTTPS, JPEG, Max: 1024x1024
            # flow: frontend filter image -> post to Cloudinary and get the secure_url -> post the url to api -> break into 2 parts
            attachment_url = message['attachment_url']

            # Get channel_access_token for send authentication
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            channel_access_token = channel.channel_access_token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "attachment_url": attachment_url,
                "channel_access_token": channel_access_token
            }
            url = '{}/line/messages/attachment'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            if is_bot == False:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.attachment_url = attachment_url
                message.save_to_db()
            else:
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.attachment_url = attachment_url
                message.save_to_db()

            # Update inbox short text and last_updated
            inbox.last_updated = message.timestamp
            inbox.short_text = "You sent an attachment.."
            inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        if message_type == "LINE_TEMPLATE":
            templates = message["message_templates"]

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            channel_access_token = channel.channel_access_token

            if is_bot == False:
                # Save message
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.save_to_db()
            else:
                # Save message
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.save_to_db()

            for item in templates:
                title = item["template_title"]
                thumbnailImageUrl = item["image_url"]
                text = item["subtitle"]
                actions = item["buttons"]  # list

                # Save template message
                template = MessageTemplate(
                    message_seq=message.seq,
                    template_title=title,
                    image_url=thumbnailImageUrl,
                    subtitle=text
                )
                template.save_to_db()

                for action in actions:
                    button_type = action["type"]

                    if button_type == "web_url":
                        # Each url post as button should be whitelisted
                        # Make sure we request whitelisting to fb before
                        # We send the message to FB Send Api
                        uri = action["url"]
                        label = action["title"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            button_url=uri,
                            button_title=label,
                            button_type=button_type
                        )
                        button.save_to_db()

                    elif button_type == "postback":
                        label = action["title"]
                        data = action["payload"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            payload=data,
                            button_title=label,
                            button_type=button_type
                        )
                        button.save_to_db()
                    else:
                        return abort(400, "Invalid button type")

            # ===========================================================================
            # If templates has only one item, then we send it as only template in LINE
            # If templates has several items on it, then we send it as carousel in LINE
            # We want to make the structure of the request consistent, while we also try to
            # Solve the differences in request API of messenger and LINE@
            # ===========================================================================
            if len(templates) == 1:
                # =============================================
                # SENDING TO WEBHOOK URL LINE TEMPLATE
                # =============================================
                payload = {
                    "recipient_id": recipient_id,
                    "elements": templates,
                    "channel_access_token": channel_access_token
                }
                url = '{}/line/messages/line_template'.format(WEBHOOK_URL)
                send_message.delay(url, payload)
            else:
                # =============================================
                # SENDING TO WEBHOOK URL LINE CAROUSEL
                # =============================================
                payload = {
                    "recipient_id": recipient_id,
                    "elements": templates,
                    "channel_access_token": channel_access_token
                }
                url = '{}/line/messages/line_carousel'.format(WEBHOOK_URL)
                send_message.delay(url, payload)

            # Update inbox
            inbox.last_updated = message.timestamp

            if is_bot == False and is_shortlink == False:
                inbox.short_text = "{0} sending a line tempalate".format(current_user.first_name)
                inbox.save_to_db()
            else:
                inbox.short_text = "Bot sending a line tempalate"
                inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        if message_type == "LINE_IMAGE_CAROUSEL":
            templates = message["message_templates"]

            # Get page_access_token and add to base payload
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            channel_access_token = channel.channel_access_token

            if is_bot == False:
                # Save message
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.save_to_db()
            else:
                # Save message
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.save_to_db()

            for item in templates:
                ImageUrl = item["image_url"]
                actions = item["buttons"]  # list

                # Save template message
                template = MessageTemplate(
                    message_seq=message.seq,
                    image_url=ImageUrl
                )
                template.save_to_db()

                for action in actions:
                    button_type = action["type"]

                    if button_type == "web_url":
                        # Each url post as button should be whitelisted
                        # Make sure we request whitelisting to fb before
                        # We send the message to FB Send Api
                        uri = action["url"]
                        label = action["title"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            button_url=uri,
                            button_title=label,
                            button_type=button_type
                        )
                        button.save_to_db()

                    elif button_type == "postback":
                        label = action["title"]
                        data = action["payload"]

                        # Save button
                        button = TemplateButton(
                            template_id=template.id,
                            payload=data,
                            button_title=label,
                            button_type=button_type
                        )
                        button.save_to_db()
                    else:
                        return abort(400, "Invalid button type")

            # =============================================
            # SENDING TO WEBHOOK URL LINE CAROUSEL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "elements": templates,
                "channel_access_token": channel_access_token
            }
            url = '{}/line/messages/line_image_carousel'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # Update inbox
            inbox.last_updated = message.timestamp

            if is_bot == False:
                inbox.short_text = "{0} sending a line image carousel".format(current_user.first_name)
                inbox.save_to_db()
            else:
                inbox.short_text = "Bot sending a line image carousel"
                inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        if message_type == "TELEGRAM_TEXT":
            text = message["text"]

            # Get channel_access_token for send authentication
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            token = channel.token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "text": text,
                "token": token
            }
            url = '{}/telegram/messages/text'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            if is_bot == False:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.text = text
                message.save_to_db()
            else:
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.text = text
                message.save_to_db()

            # Update inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = text
            inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        if message_type == "TELEGRAM_ATTACHMENT":
            attachment_url = message['attachment_url']

            # Get channel_access_token for send authentication
            channel = ChannelModel.query.filter_by(account_id=account_id).filter_by(id=sender_id).filter_by(deleted=False).first()
            if channel is None:
                return abort(404, "Channel not found.")
            token = channel.token

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "recipient_id": recipient_id,
                "attachment_url": attachment_url,
                "token": token
            }
            url = '{}/telegram/messages/attachment'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            if is_bot == False:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.attachment_url = attachment_url
                message.save_to_db()
            else:
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.attachment_url = attachment_url
                message.save_to_db()

            # Update inbox short text and last_updated
            inbox.last_updated = message.timestamp
            inbox.short_text = "You sent an attachment.."
            inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        if message_type == "WEB_TEXT":
            """We use smooch for this type of message."""
            text = message["text"]

            # Get app id
            smooch = IntegrationModel.query.\
                     filter_by(account_id=account_id).\
                     filter_by(app_id=sender_id).\
                     filter_by(deleted=False).first()
            if smooch is None:
                return abort(404, "Channel not found.")
            sender_id = smooch.app_id
            secret = smooch.secret
            key_id = smooch.key_id

            # ===============================================
            # Generate jwt for smooch auth
            # ===============================================
            import jwt
            encoded_jwt = jwt.encode({
                "scope": "app"
            },
            secret,
            headers={
                "alg": 'HS256',
                "typ": 'JWT',
                "kid": key_id
            })
            auth = encoded_jwt.decode('utf-8')

            # =============================================
            # SENDING TO WEBHOOK URL
            # =============================================
            payload = {
                "app_user": recipient_id,
                "text": text,
                "app_id": sender_id,
                "auth": auth
            }
            url = '{}/smooch/messages/text'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            if is_bot == False:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author=author
                )
                message.text = text
                message.save_to_db()
            else:
                # A successful send api will return a JSON
                message = MessageModel(
                    id=uuid.uuid1().hex,
                    message_type=message_type,
                    avatar_url=avatar_url,
                    sender_id=sender_id,
                    recipient_id=recipient_id,
                    account_id=account_id,
                    inbox_id=inbox_id,
                    status='sent',
                    author="Bot"
                )
                message.text = text
                message.save_to_db()

            # Update Inbox
            inbox.last_updated = message.timestamp
            inbox.short_text = text
            inbox.save_to_db()

            # Inject item to responses list above
            message = message_schema.dump(message).data
            responses.append(message)

            if is_bot == True:
                # ===========================================
                # Emit back to client using celery
                # ===========================================
                data_inbox = inbox_schema.dump(inbox).data
                emit_websocket_inbox.delay(data_inbox)

        # Delay loop 1 s
        time.sleep(1)

    # Return items
    if is_bot == False and is_shortlink == False:
        resp = jsonify(responses)
        resp.status_code = 201
        return resp
    else:
        # Emit new message
        emit_websocket_message.delay(responses, inbox_id)
