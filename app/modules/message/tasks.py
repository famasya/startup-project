
"""Add celery tasks here."""
import requests
from flask import current_app
from flask_login import current_user

from app.extensions.flask_celery import make_celery
from app.extensions.socketio import socketio

celery = make_celery(current_app)


@celery.task
def send_message(url, data):
	requests.post(url, json=data)

@celery.task
def emit_websocket_message(data_message, inbox_id):
	# Emit back to client
	socketio.emit("receive_message", data_message, room=inbox_id, namespace="/messages")
