from sqlalchemy import exc

from app.extensions.db import db
from ...utilities import time

class PaymentModel(db.Model):

	__tablename__ = 'payment'

	account_id = db.Column(db.String, db.ForeignKey('accounts.id'))
	package_id = db.Column(db.Integer, db.ForeignKey('account_packages.id'))

	xendit_subscription_id = db.Column(db.String, primary_key=True)
	xendit_token = db.Column(db.String)
	xendit_token_id = db.Column(db.String)
	amount = db.Column(db.Integer)
	is_active = db.Column(db.Boolean, default=True)
	created = db.Column(db.Integer)
	updated = db.Column(db.Integer)
	reference = db.Column(db.String)
	frequency = db.Column(db.String(10))

	payments = db.relationship('PaymentTransactionModel', backref='payment', lazy='dynamic')

	def save_to_db(self):
	    """Save to database."""
	    try:
	        db.session.add(self)
	        db.session.commit()
	    except:
	       db.session.rollback()
	       raise

class PaymentTransactionModel(db.Model):

	__tablename__ = 'payment_transaction'

	id = db.Column(db.Integer, primary_key=True)
	subscription_id = db.Column(db.String, db.ForeignKey('payment.xendit_subscription_id'))
	status = db.Column(db.String)
	capture_amount = db.Column(db.Integer)
	created = db.Column(db.Integer)
	card_type = db.Column(db.String)
	charge_id = db.Column(db.String)
	eci = db.Column(db.String(3))
	masked_card_number = db.Column(db.String)

	def save_to_db(self):
	    """Save to database."""
	    try:
	        db.session.add(self)
	        db.session.commit()
	    except:
	       db.session.rollback()
	       raise
