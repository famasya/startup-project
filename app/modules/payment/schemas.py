from app.extensions.schema import ma
from .models import PaymentModel, PaymentTransactionModel
from ..account_package import schemas
from ..account_package.models import AccountPackage
from ..user.models import UserModel
from flask_login import current_user
from marshmallow import post_dump

class PaymentSchema(ma.ModelSchema):
	class Meta:
		model = PaymentModel
		exclude = ['xendit_token_id', 'payments']

payment_schema = PaymentSchema(many=True)

class SinglePaymentSchema(ma.ModelSchema):
	class Meta:
		model = PaymentModel
		exclude = ['xendit_token', 'xendit_token_id', 'payments']

single_payment_schema = SinglePaymentSchema()

class PaymentDetailsSchema(ma.ModelSchema):
	class Meta:
		model = PaymentTransactionModel
		exclude = ['id']

	@post_dump
	def add_new_fields(self, data):
		account_id = current_user.account_id
		email = UserModel.query.filter_by(account_id=account_id).filter_by(role='admin').first()
		data['billed_email'] = email.username

payment_details_schema = PaymentDetailsSchema()

class TransactionSchema(ma.ModelSchema):
	class Meta:
		model = PaymentTransactionModel
		exclude = ['id']

	@post_dump
	def add_new_fields(self, data):
		data['xendit_subscription_id'] = data['payment']
		data.pop("payment", None)

transaction_schema = TransactionSchema(many=True)
