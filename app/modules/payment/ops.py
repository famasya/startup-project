# pylint: disable= E1101, W0312

import requests
import base64
import json
from datetime import datetime, timedelta
from flask import jsonify, session, current_app
from ...utilities.abort import abort
from ...utilities.api_limiter import register_account_limit
from flask_login import current_user, login_user
from werkzeug.security import check_password_hash
from ...extensions.db import db
from ...utilities.time import timestamp
from ..account_package.models import AccountPackage as PackageModel
from ..account.models import AccountModel
from ..inbox.models import InboxModel
from ..user.models import UserModel
from .models import PaymentModel, PaymentTransactionModel
from operator import itemgetter
from .schemas import payment_schema, payment_details_schema, transaction_schema, single_payment_schema

XENDIT_KEY = (current_app.config['XENDIT_KEY']+':').encode()
AUTH = b'Basic '+base64.b64encode(XENDIT_KEY)




def reset_inbox_status(account_id):
	try:
		db.session.query(InboxModel).filter_by(account_id=account_id).\
		filter_by(deleted=False).filter_by(status='hold').\
		update({InboxModel.status: 'open'})
		
		db.session.commit()
	except:
		pass

	return ''

def create_subscription(data, account_id, is_modify=False):
	# If user is not admin, decline request
	if current_user.role != 'admin':
		abort(401, 'You are not allowed to make a subscription')

	try:
		xendit_token = data['xendit_token']
		xendit_token_id = data['xendit_token_id']
		package_id = data['package_id']
		frequency = data['frequency'].upper()
	except:
		abort(400, "Invalid request")

	# Check if any active subscription exists
	exist = PaymentModel.query.filter_by(account_id=account_id).filter_by(is_active=True).first()

	"""
	If subscription is not exist and this creation is not in the context of subscription modification,
	return 400 error
	"""
	if exist is not None and is_modify is False:
		return jsonify({'message': 'Subscription for this account exists'})

	get_price = PackageModel.query.filter_by(id=package_id).first()
	if get_price is None:
		return abort(404, "There's package {}".format(package_id))

	price = get_price.price

	current = datetime.now()
	subscribe_payload = {
		"token_id": xendit_token,
		"authentication_id": xendit_token_id,
		"amount": price,
		"start_date": current.isoformat(),
		"end_date": (current + timedelta(days=1095)).isoformat(),  # 3 years
		"frequency": frequency
	}
	subscribe_url = 'https://api.xendit.co/managed_subscriptions'
	subscribe_header = {
		'Authorization': AUTH.decode(),
		'content-type': 'application/json'
	}

	# Request subscription to Xendit API
	subscribe = requests.post(subscribe_url, json=subscribe_payload, headers=subscribe_header)
	subscription_data = subscribe.json()
	if subscribe.status_code == 200:
		if subscription_data['status'] == 'ACTIVE' and subscribe.status_code == 200:
			subscription_id = subscription_data['id']
			save_subscription(data, subscribe_payload, account_id, subscription_id, current)
		else:
			abort(400, subscription_data['message'])
	else:
		response = jsonify({'message': 'Cannot contact payment gateway', 'payload': subscription_data})
		response.status_code = 400
		return response

	reset_inbox_status(account_id)

	# Message based on current execution context
	if is_modify:
		return subscription_data
	else:
		payment = PaymentModel.query.filter_by(account_id=account_id).filter_by(is_active=True).first()
		resp = single_payment_schema.jsonify(payment)
		return resp


def activate_account(data):
	"""Activate account when it's expired."""
	try:
		username = data["username"]
		password = data["password"]
	except (KeyError, TypeError):
		return abort(400, "Invalid request.")

	user = UserModel.query.filter_by(username=username).filter_by(deleted=False).first()
	if not user or not check_password_hash(user.password_hash, password):
		abort(401, "Wrong username and password.")
	account_id = user.account_id

	if user.role != 'admin':
		abort(401, 'You are not allowed to make a modification')

	payment = PaymentModel.query.filter_by(account_id=account_id).filter_by(is_active=True).first()
	if payment:
		abort(400, "Active payment found.")

	# Add session
	login_user(user)

	# Create subscription
	data.pop("username", None)
	data.pop("password", None)
	subs = create_subscription(data, account_id)

	if type(subs) is not dict:
		return json.loads(subs.data.decode('utf8')), 400

	resp = {
        "api_key": user.api_key,
        "account_id": user.account_id,
        "user_id": user.id
    }
	return jsonify(resp)


def convert_to_idr(usd):
	# Convert USD to IDR based on current rate
	req = requests.get('http://api.fixer.io/latest?symbols=USD,IDR')

	if req.status_code == 200:
		api = req.json()
		if req.status_code == 200:
			return int(api['rates']['IDR'])
		else:
			abort(400, 'Failed to convert USD')
	else:
		abort(400, 'Cannot contact conversion API gateway')


def save_subscription(data, subscription_payload, account_id, subscription_id, current):
	# Save subcription return to database
	payment = PaymentModel(
		xendit_token=data['xendit_token'],
		xendit_token_id=data['xendit_token_id'],
		xendit_subscription_id=subscription_id,
		account_id=account_id,
		amount=subscription_payload['amount'],
		package_id=data['package_id'],
		frequency=data['frequency'],
		created=current.timestamp(),
		updated=current.timestamp(),
		reference='ACTIVE'
	)
	payment.save_to_db()

	# Register API call limit to redis
	account_package = PackageModel.query.filter_by(id=payment.package_id).first()
	register_account_limit(account_id, account_package.api_call_limit)

	# Update account payment_start
	account = AccountModel.query.filter_by(id=account_id).filter_by(deleted=False).first()
	account.payment_start = payment.created
	account.date_updated = timestamp()
	account.package_id = payment.package_id
	account.save_to_db()
	return True

def get_subscription(account_id):
	# Get subscription lists
	subscription = PaymentModel.query.filter_by(account_id=account_id).all()
	transaction = db.session.query(PaymentTransactionModel).outerjoin(PaymentModel).\
			filter(PaymentModel.account_id==account_id).all()
	subscription_item, error = payment_schema.dump(subscription)
	transaction_item, error = transaction_schema.dump(transaction)
	merged = subscription_item+transaction_item
	additional = []
	for i in merged:
		if 'charge_id' in i:
			i['reference'] = 'CHARGED'
		if 'reference' in i and i['reference'] == 'CANCELLED':
			i['reference'] = 'CREATED'
			additional.append({
				'created': i['updated'],
				'account': i['account'],
				'account_package': i['account_package'],
				'amount': i['amount'],
				'frequency': i['frequency'],
				'is_active': i['is_active'],
				'xendit_subscription_id': i['xendit_subscription_id'],
				'reference': 'CANCELLED',
				})
		elif 'reference' in i and i['reference'] == 'MIGRATED':
			i['reference'] = 'MIGRATED'
			i['created'] = i['updated']-1
			additional.append({
				'created': i['created'],
				'account': i['account'],
				'account_package': i['account_package'],
				'amount': i['amount'],
				'frequency': i['frequency'],
				'is_active': i['is_active'],
				'xendit_subscription_id': i['xendit_subscription_id'],
				'reference': 'CREATED',
				})

	merged = merged+additional
	chronological = sorted(merged, key=itemgetter('created'), reverse=True)
	return jsonify(chronological)

def get_subscription_details(account_id, charge_id):
	lists = PaymentTransactionModel.query.filter_by(charge_id=charge_id).first()
	if not lists:
		return abort(404, 'Transaction not found')
	return payment_details_schema.jsonify(lists)

def cancel_subscription(account_id, is_modify=False):
	if current_user.role != 'admin':
		abort(401, 'You are not allowed to make a cancellation')

	purchased = PaymentModel.query.filter_by(account_id=account_id).filter_by(is_active=True).first()

	# If no subscribed account, return 200
	if purchased is None:
		return jsonify({'message': 'No active subscription found'})

	id = purchased.xendit_subscription_id
	cancellation_header = {
		'Authorization': AUTH.decode(),
		'content-type': 'application/json'
	}
	cancellation_url = 'https://api.xendit.co/managed_subscriptions/{}/cancellation'.format(id)

	# Request cancellation to Xendit API
	unsubscribe = requests.post(cancellation_url, headers=cancellation_header)
	subscription_data = unsubscribe.json()
	if unsubscribe.status_code == 200:
		response = unsubscribe.json()
		"""
		If cancellation is in case of upgrading/downgrading, continue.
		Else, hold cancel status until end of service
		"""
		if unsubscribe.status_code == 200 and response['status'] == 'CANCELLED':
			if is_modify:
				purchased.is_active = False
				purchased.updated = datetime.now().timestamp()
				purchased.reference = 'MIGRATED'
			else:
				purchased.updated = datetime.now().timestamp()
				purchased.reference = 'ACTIVE_ON_HOLD'

			purchased.save_to_db()
			return jsonify(response)
		else:
			abort(400, response['message'])
	else:
		response = jsonify({'message': 'Cannot contact payment gateway', 'payload': subscription_data})
		response.status_code = 400
		return response


def modify_subscription(new_data, account_id):
	"""
	Data modification. When user wants to upgrade/downgrade their subscription, the system will cancel existing subscription, and generate new one
	"""
	if current_user.role != 'admin':
		abort(401, 'You are not allowed to make a modification')

	try:
		new_xendit_token_id = new_data['xendit_token_id']
		new_frequency = new_data['frequency']
		new_package_id = new_data['package_id']
	except:
		abort(400, 'Invalid request')

	data = PaymentModel.query.filter_by(account_id=account_id).filter_by(is_active=True).first()

	if data is None:
		return jsonify({'message': 'No active subscription found'})

	# If there are changes in frequency or package or CC information, consider it as change

	if data.frequency != new_frequency or data.package_id != new_package_id:
		# Change in frequency / package should followed by token update
		if data.xendit_token_id != new_xendit_token_id:
			cancel_subscription(account_id, True)
			subscription_data = create_subscription(new_data, account_id, True)
		else:
			return abort(400, 'New credential is needed')
	elif data.xendit_token_id != new_xendit_token_id:
		# Change in CC information should renew new token
		cancel_subscription(account_id, True)
		subscription_data = create_subscription(new_data, account_id, True)
	else:
		return abort(400, 'No changes were made')

	if type(subscription_data) is not dict:
		 return json.loads(subscription_data.data.decode('utf8')), 400
	else:
		return jsonify(subscription_data)
