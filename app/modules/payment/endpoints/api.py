from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ..ops import (
	activate_account, create_subscription,
	get_subscription, modify_subscription,
	cancel_subscription, get_subscription_details
)

api = Namespace('payment', description='Subscription / payment operation')

post_data = api.model('Payment', {
	'token_id': fields.String,
	'package_id': fields.String,
	'frequency': fields.String
})


@api.route('/activate_account')
class ActivateAccount(Resource):
	def post(self):
		data = request.json
		subs = activate_account(data)

		return subs


@api.route('/<account_id>')
class SubscriptionItem(Resource):
	@login_required
	def post(self, account_id):
		"""Register user to subscribe"""
		data = request.json
		subs = create_subscription(data, account_id)
		return subs

	@login_required
	def get(self, account_id):
		"""Get subscription status"""
		return get_subscription(account_id)

	@login_required
	def delete(self, account_id):
		"""Cancel subscription"""
		return cancel_subscription(account_id)

	@login_required
	def put(self, account_id):
		"""Modify existing user subscription"""
		data = request.json
		modify = modify_subscription(data, account_id)
		return modify


@api.route('/<account_id>/<charge_id>')
class SubscriptionItemList(Resource):
	@login_required
	def get(self, account_id, charge_id):
		"""Get subscription status, active only"""
		return get_subscription_details(account_id, charge_id)
