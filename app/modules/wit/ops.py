import requests
from flask import current_app
from ...utilities.abort import abort
from ...utilities.unique_url import universal_uuid_url64
from ..bot.models import WitApp


def create_witapp(name, lang, account_id):
    """Create new wit app.
    In this request, use our wit access token
    """
    new_name = ''.join(x for x in name.title() if not x.isspace())
    new_name = universal_uuid_url64(new_name)
    url = "https://api.wit.ai/apps"
    data = {
        "name": new_name,
        "lang": lang,
        "private": True
    }
    headers = { "Authorization": "Bearer {}".format(current_app.config["WIT_ACCESS_TOKEN"]) }
    req = requests.post(url, json=data, headers=headers)

    resp_data = req.json()

    if "errors" in resp_data:
        return abort(400, resp_data["errors"][0])

    return resp_data


def remove_app(wit_id):
    """Delete wit app"""
    wit = WitApp.query.filter_by(id=wit_id).filter_by(deleted=False).first()
    if wit is None:
        return abort(400, "Wrong wit_id")

    url = "https://api.wit.ai/apps/{}?v=20170307".format(wit.wit_app_id)
    headers = { "Authorization": "Bearer {}".format(wit.wit_access_token) }
    req = requests.delete(url, headers=headers)

    resp_data = req.json()

    if "errors" in resp_data:
        return abort(400, resp_data["errors"][0])

    return resp_data


def return_meaning(account_id, q):
    """Return meaning of sentence.
    In this request, use user's wit access token
    """
    # Check wit
    wit = WitApp.query.filter_by(account_id=account_id).filter_by(deleted=False).first()
    if wit is None:
        return abort(400, "This account doesn't have active bot.")
    url = "https://api.wit.ai/message?v=20170307&q={}".format(q)
    headers = { "Authorization": "Bearer {}".format(wit.wit_access_token) }
    req = requests.get(url, headers=headers)

    resp_data = req.json()

    if "errors" in resp_data:
        return abort(400, resp_data["errors"][0])

    return resp_data


def add_entity(account_id, id, doc):
    """Add new entity
    In this request, use user's wit access token
    """
    # Check wit
    wit = WitApp.query.filter_by(account_id=account_id).filter_by(deleted=False).first()
    if wit is None:
        return abort(400, "This account doesn't have active bot.")
    url = "https://api.wit.ai/entities?v=20170307"
    headers = { "Authorization": "Bearer {}".format(wit.wit_access_token) }

    if doc is None:
        data = {
            "id": id
        }
    else:
        data = {
            "id": id,
            "doc": doc
        }
    req = requests.post(url, json=data, headers=headers)

    resp_data = req.json()

    if "errors" in resp_data:
        return abort(400, resp_data["errors"][0])

    return resp_data


def list_values(account_id, entity_name):
    """Return list of values of given entity.
    In this request, use user's wit access token.
    """
    # Check wit
    wit = WitApp.query.filter_by(account_id=account_id).filter_by(deleted=False).first()
    if wit is None:
        return abort(400, "This account doesn't have active bot.")
    url = "https://api.wit.ai/entities/{}?v=20170307".format(entity_name)
    headers = { "Authorization": "Bearer {}".format(wit.wit_access_token) }
    req = requests.get(url, headers=headers)

    resp_data = req.json()

    if "errors" in resp_data:
        return abort(400, resp_data["errors"][0])

    return resp_data


def validate_entity(account_id, text, entity, value):
    """Match text with entity-value.
    In this request, use user's wit access token
    """
    # Check wit
    wit = WitApp.query.filter_by(account_id=account_id).filter_by(deleted=False).first()
    if wit is None:
        return abort(400, "This account doesn't have active bot.")
    url = "https://api.wit.ai/samples?v=20170307"
    headers = { "Authorization": "Bearer {}".format(wit.wit_access_token) }
    data = [{
        "text": text,
        "entities": [
        	{
            	"entity": entity,
                "value": value
        	}
        ]
    }]
    req = requests.post(url, json=data, headers=headers)

    resp_data = req.json()

    if "error" in resp_data:
        return abort(400, resp_data["error"])

    return resp_data


def remove_expression(account_id, entity, value, _text, background=False, access_token=None):
    """Delete expression"""
    # Check wit
    if background == False:
        wit = WitApp.query.filter_by(account_id=account_id).filter_by(deleted=False).first()
        if wit is None:
            return abort(400, "This account doesn't have active bot.")
        access_token = wit.wit_access_token

    url = "https://api.wit.ai/entities/{0}/values/{1}/expressions/{2}?v=20170307".format(entity, value, _text)
    headers = { "Authorization": "Bearer {}".format(access_token) }
    req = requests.delete(url, headers=headers)

    resp_data = req.json()

    if "error" in resp_data:
        return abort(400, resp_data["error"])

    return resp_data


def remove_entity(account_id, entity_name):
    """Delete permanently entity"""
    # Check wit
    wit = WitApp.query.filter_by(account_id=account_id).filter_by(deleted=False).first()
    if wit is None:
        return abort(400, "This account doesn't have active bot.")
    url = "https://api.wit.ai/entities/{0}?v=20170307".format(entity_name)
    headers = { "Authorization": "Bearer {}".format(wit.wit_access_token) }
    req = requests.delete(url, headers=headers)

    resp_data = req.json()

    if "error" in resp_data:
        return abort(400, resp_data["error"])

    return resp_data

def wit_modify_entity(account_id, data):
    """Modify wit"""
    payload = {
        "id": data['entity_name']
    }
    
    # Check wit
    wit = WitApp.query.filter_by(account_id=account_id).filter_by(deleted=False).first()
    if wit is None:
        return abort(400, "This account doesn't have active bot.")
    url = "https://api.wit.ai/entities/{0}?v=20170307".format(data['old_entity'])
    headers = {
        "Authorization": "Bearer {}".format(wit.wit_access_token)
    }

    req = requests.put(url, json=payload, headers=headers)

    resp_data = req.json()
    if "error" in resp_data:
        return abort(400, resp_data["error"])

    return resp_data
