import jwt, pprint, uuid
from flask import request, Blueprint, current_app, jsonify
from ...customer.models import CustomerModel, CustomerWoocommerce
from ...channel.models import ChannelModel
from ...account.models import AccountModel
from ...woocommerce.models import WoocommerceLog
from ...integration.models import IntegrationModel, NexmoTemplate
from ...integration.schemas import sms_template
from ...campaigns.models import WoocommerceCartLog, CampaignsQueue, CampaignsModel
from ....utilities.abort import abort
from ....extensions.db import db
from ...webhook_connector.tasks import send_woo_reminder
from ...campaigns.schemas import campaign_queue_schema, campaigns_queue_schema
from urllib.parse import urlparse
import nexmo
import ast
from celery.task.control import revoke

mod = Blueprint('woocommerce', __name__)

@mod.route('/<jwt_token>', methods=['POST'])
def webhook(jwt_token):
    """Define the webhook data and save it to database."""
    SECRET_KEY = current_app.config['SECRET_KEY']
    try:
        data = jwt.decode(jwt_token, SECRET_KEY, algorithm=['HS256'])
        # When the time expiration (5s) is exceeded, raised 401 error
    except (jwt.ExpiredSignatureError, jwt.InvalidTokenError, jwt.DecodeError):
        return abort(401)

    # Log whenever the messages received in debug mode
    if current_app.debug:
        current_app.logger.info("Request data: " + pprint.pformat(data))

    data = data['data']

    metadata = data.get("meta_data")
    status = data.get("status")
    user_ref = list(filter(lambda d: d["key"] == "user_ref", metadata))[0]["value"]
    if user_ref is None:
        return ''

    # Check if abandon cart task is active
    print(user_ref)
    queue = CampaignsQueue.query.filter_by(user_ref=str(user_ref)).\
            filter_by(status="QUEUED").\
            filter_by(triggered_by="WOOCOMMERCE").all()
    if len(queue) == 0:
        return ''

    WEBHOOK_URL = current_app.config['WEBHOOK_URL']
    fb_graph = current_app.config['FB_GRAPH']

    for queue_item in queue:
        queue_data = campaign_queue_schema.dump(queue_item).data
        print(queue_data)
        if queue_data["triggered_after"] == "LAST_CART" and queue_data["status"] == "QUEUED":
            # Cancel task
            revoke(queue_data["execution_id"], terminate=True)

            queue_item.status = "CANCELLED"
            queue_item.save_to_db()
        elif queue_data["triggered_after"] == "LAST_USER_CHECKOUT" and status in ("on-hold"):
            uuid_ = uuid.uuid1().hex
            recipient_id = queue_data["recipient_id"]
            page_access_token = queue_data["access_token"]
            saved_reply = queue_data["messages"][0]
            send_woo_reminder.apply_async(
                args=[saved_reply, WEBHOOK_URL, recipient_id, page_access_token, fb_graph, queue_data["execution_id"]],
                countdown=queue_data["send_after"],
                task_id=uuid_
            )
        elif queue_data["triggered_after"] == "LAST_ORDER_UPDATE" and status in ("pending", "processing"):
            uuid_ = uuid.uuid1().hex
            recipient_id = queue_data["recipient_id"]
            page_access_token = queue_data["access_token"]
            saved_reply = queue_data["messages"][0]
            send_woo_reminder.apply_async(
                args=[saved_reply, WEBHOOK_URL, recipient_id, page_access_token, fb_graph, queue_data["execution_id"]],
                countdown=queue_data["send_after"],
                task_id=uuid_
            )
        elif queue_data["triggered_after"] == "LAST_ORDER_COMPLETED" and status in ("completed"):
            uuid_ = uuid.uuid1().hex
            recipient_id = queue_data["recipient_id"]
            page_access_token = queue_data["access_token"]
            saved_reply = queue_data["messages"][0]
            send_woo_reminder.apply_async(
                args=[saved_reply, WEBHOOK_URL, recipient_id, page_access_token, fb_graph, queue_data["execution_id"]],
                countdown=queue_data["send_after"],
                task_id=uuid_
            )        


    return ''
