# pylint: disable=E1101, C0326

"""
BOT TRIGGERS
~~~~~~~~~~~~
This function is trying to check whether bot is active in the channel or not (representated by page_id for messenger)
When we find bot active, then this function will be triggered.
"""
from flask import current_app

from .tasks import (
    send_email_notif, send_webhook
)
from ..inbox.sockets import (
    emit_websocket_inbox, emit_counter_inbox
)
from ..inbox.models import InboxModel
from ..inbox.schemas import inbox_schema
from ..bot.models import BotModel
from ..channel.models import ChannelModel
from ..bot_rule.models import RuleModel
from ..bot_rule.schemas import rule_schema
from ..wit.ops import return_meaning
from ..saved_reply.models import SavedReplyModel
from ..saved_reply.schemas import saved_reply_schema, saved_replies_schema
from ..message.ops import create_message
from ..sequence.ops import trigger_sequence
from app.extensions.db import db
from pprint import pprint
import uuid

def is_bot_triggered(account_id, inbox_id, recipient_id, sender_id, text, channel_type=None, getting_started=False):
    """ Bot Triggers

    If in the channel, there is active bot, then for each message received
    will count as bot_session. For each module has bot_session on it, and will
    be triggered after message with the same bot_session received.
    """
    if channel_type == None:
        print("A")
        return '', 201

    if channel_type == "messenger":
        # Check if the channel has bot active
        # If no result, deleted, or not active, then pass it
        obj = db.session.query(ChannelModel, BotModel).\
              filter(ChannelModel.page_id==recipient_id).\
              filter(ChannelModel.deleted==False).\
              filter(BotModel.channel_id==ChannelModel.id).\
              filter(BotModel.deleted==False).first()
        try:
            bot = getattr(obj, "BotModel")
        except AttributeError:
            print("B")
            return '', 201

    # bot_status should be true to continue
    inbox = InboxModel.query.filter_by(id=inbox_id).filter_by(deleted=False).first()
    if inbox.bot_status is False:
        print(inbox.bot_status)
        print("C")
        return '', 201
    inbox.bot_session += 1
    inbox.save_to_db()

    # Check bot should be active
    if bot.is_active is False:
        print("D")
        return '', 201

    # If this is getting started postback, send welcome message
    saved_replies = SavedReplyModel.query.filter_by(bot_id=bot.id).filter_by(persistent=True).filter_by(deleted=False).all()
    saved_replies_dict = saved_replies_schema.dump(saved_replies).data

    welcome_message = [x for x in saved_replies_dict if x['ref'] == 'WELCOME_MESSAGE']
    default_answer = [x for x in saved_replies_dict if x['ref'] == 'DEFAULT_ANSWER']

    if getting_started:
        messages = welcome_message[0]['saved_reply']
    else:
        messages = default_answer[0]['saved_reply']

    session=uuid.uuid4().hex
    for message in messages:
        data = {
            "sender_id": recipient_id,
            "recipient_id": sender_id,
            "messages": [message]
        }
        create_message(data, account_id, inbox_id, is_bot=True, session=session)

    return '', 201
