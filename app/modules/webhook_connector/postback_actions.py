from urllib.parse import parse_qs, urlparse

from ..inbox.models import InboxModel
from ..inbox.schemas import inbox_schema
from ..inbox.sockets import (
    emit_websocket_inbox, emit_counter_inbox
)
from ..saved_reply.models import SavedReplyModel
from ..saved_reply.schemas import saved_reply_schema
from ..message.ops import create_message


def postback_action(account_id, inbox_id, recipient_id, sender_id, payload, channel_type=None):
    """When we receive any postback, this will trigger actions
    payload format ex: "?action=SEND_MESSAGE&reply_id=5"
    """
    # Get value from payload
    data = parse_qs(urlparse(payload).query, keep_blank_values=True)

    # Check if action exists
    try:
        action = data.get("action")[0]
    except:
        return '', 201

    if channel_type is None or payload is None or action is None:
        return '', 201

    if action == "SEND_MESSAGE":
        reply_id = data.get("reply_id")[0]
        if reply_id is None:
            return '', 201
        # Change to dict type
        reply = SavedReplyModel.query.\
                filter_by(id=reply_id).\
                filter_by(deleted=False).\
                filter_by(channel=channel_type).first()
        reply_dict = saved_reply_schema.dump(reply).data

        # Get saved_reply data
        messages = reply_dict["saved_reply"]
        data = {
            "sender_id": recipient_id,
            "recipient_id": sender_id,
            "messages": messages
        }
        create_message(data, account_id, inbox_id, is_bot=True)

    if action == "SEND_EMAIL":
        pass

    if action == "SEND_WEBHOOK":
        """For testing purposes, you can use endpoint '/messenger/webhook_test'
        See that endpoint on webhook_connector/messenger.
        To validate the request comes from Chatkoo, endpoint can validate
        the request from header 'account_id'
        """
        pass

    if action == "ASSIGN_HUMAN":
        """When this rule triggered, just change the status to
        inprogress, set the user_assigned into assign_username
        of the rule, and just turn off the bot.
        """
        assign_username = data.get("assign_username")[0]
        if assign_username is None:
            return '', 201
        inbox = InboxModel.query.\
                filter_by(account_id=account_id).\
                filter_by(id=inbox_id).\
                filter_by(deleted=False).first()
        if inbox is None:
            return '', 201

        inbox.status = 'inprogress'
        inbox.user_assigned = assign_username
        inbox.bot_status = False
        inbox.save_to_db()

        # ===========================================
        # Emit new inbox as counter
        # ===========================================
        data_inbox = inbox_schema.dump(inbox).data
        emit_counter_inbox.delay(account_id)
        emit_websocket_inbox.delay(data_inbox)
