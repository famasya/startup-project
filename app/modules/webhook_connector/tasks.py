# pylint: disable=C0103

"""Celery file to send email."""
import requests
from flask import render_template, current_app
from ...utilities.abort import abort
from celery import uuid
from ..message.ops import create_message
from ..message.tasks import send_message
from ..user.models import UserModel
from ..campaigns.models import CampaignsQueue, CampaignsModel
from ...extensions.flask_redis import conn
from flask_mail import Message
from itsdangerous import URLSafeTimedSerializer, SignatureExpired
from urllib.parse import urlparse

from app.extensions.mail import mail
from app.extensions.flask_celery import make_celery
import time

celery = make_celery(current_app)

@celery.task
def send_email_notif(data, subject, sender, recipients, template):
    """A celery task to send general email."""
    try:
        entity = data.get("entity")
        value = data.get("value")
        inbox_link = data.get("inbox_link")
    except:
        pass

    msg = Message(subject, sender=sender, recipients=recipients)
    msg.html = render_template(
        template,
        entity=entity,
        value=value,
        inbox_link=inbox_link
    )

    mail.send(msg)


@celery.task
def send_webhook(data, webhook_url, account_id):
    """Send POST request to webhook url"""
    headers = {"account_id": account_id}
    requests.post(webhook_url, json=data, headers=headers)

@celery.task(bind=True, default_retry_delay=3)
def send_shortlink_message(self, account_id, inbox_id, saved_reply, data, user_data):
    print("SENDING SHORTLINK...")
    data = {
        "sender_id": data['recipient']['id'],
        "recipient_id": data['sender']['id'],
        "messages": saved_reply
    }

    try:
        create_message(data, account_id, inbox_id, is_shortlink=True, user_data=user_data)
    except Exception as exc:
        self.retry(exc=exc, max_retries=3)

    print("SENDING SHORTLINK DONE...")
    return True

@celery.task
def create_reminder(account_id, data_inbox, api_url):
    query = UserModel.query.filter_by(account_id=account_id).\
            filter_by(deleted=False).all()

    offline_all = True
    users = []
    for user in query:
        users.append({
            "username": user.username,
            "first_name": user.first_name
        })
        if user.online == True:
            offline_all = False

    if offline_all:
        print("OFFLINE ALL...")
        task_id = uuid()
        conn.lpush('task_{}'.format(account_id), task_id)
        send_email_reminder.apply_async(
            args=[account_id, current_app.config["NOREPLY_EMAIL"], data_inbox, users, api_url, task_id], countdown=300, task_id=task_id)

    return True

@celery.task
def send_email_reminder(account_id, sender, data, users, api_url, task_id):

    for user in users:
        msg = Message('A friendly reminder', sender=sender, recipients=[user['username']])
        msg.html = render_template(
            'message_notification.html',
            inbox_link="{}/{}/{}".format(api_url, data['status'], data['id']),
            user_name=user['first_name']
        )
        mail.send(msg)
    conn.lrem('task_{}'.format(account_id), task_id, 1)

    return True

@celery.task(bind=True, default_retry_delay=3)
def send_abandon_cart_reminder(self, ref_data, user_ref, graph_url, page_access_token, uuid):
    elements = []
    store_url = ref_data["url"]
    cart_url = ref_data["cart_url"]+"#checkbox_false"
    urls = []
    for item in ref_data.get("items"):
        url = "{}/{}".format(store_url, item["slug"])
        image_url = item["image"]
        if urlparse(image_url).scheme == '':
            image_url = "https:"+image_url

        urls.append(url)
        elements.append({
            "title": item["name"],
            "subtitle": item["description"],
            "image_url": image_url,
            "default_action": {
                "type": "web_url",
                "url": url,
                "messenger_extensions": True,
                "webview_height_ratio": "tall",
                "fallback_url": store_url
            }
        })

    # Whitelist URLs
    whitelist_request = {
        "whitelisted_domains": urls
    }
    whitelist_url = "{0}/messenger_profile?access_token={1}".format(graph_url, page_access_token)
    rv = requests.post(whitelist_url, json=whitelist_request)
    if rv.status_code != 200:
        return abort(400, "Url can't be whitelisted, make sure it use https.")

    if len (elements) == 1:
        elements[0]["buttons"] = [{
                    "title": "Continue to cart",
                    "type": "web_url",
                    "url": "{}".format(cart_url)
                }]

        payload = {
            "template_type": "generic",
            "elements": elements
        }
    else:
        payload = {
            "template_type": "list",
            "top_element_style": "compact",
            "elements": elements,
            "buttons": [{
                "title": "Continue to cart",
                "type": "web_url",
                "url": "{}".format(cart_url)
            }]            
        }
    payload = {
        "recipient": {
            "user_ref": user_ref
        },
        "message": {
            "attachment": {
                "type": "template",
                "payload": payload
            }
        }
    }

    try:
        queue = CampaignsQueue.query.filter_by(execution_id=uuid).\
                filter_by(status="QUEUED").first()
        if queue is not None:
            queue.status = "EXECUTED"
            queue.save_to_db()

        # Update sent_campaign stats
        campaign = CampaignsModel.query.filter_by(id=queue.campaign_id).first()
        campaign.sent_campaign += 1
        campaign.save_to_db()
    except Exception as exc:
        self.retry(exc=exc, max_retries=3)

    post_url = "https://graph.facebook.com/v2.6/me/messages?access_token={}".format(page_access_token)
    r = requests.post(post_url, json=payload)

    print(r, r.text)
    print(payload)


@celery.task(bind=True, default_retry_delay=3)
def send_woo_reminder(self, data, WEBHOOK_URL, recipient_id, page_access_token, fb_graph, uuid):

    # Update sent_campaign stats
    try:
        queue = CampaignsQueue.query.filter_by(execution_id=uuid).\
                filter_by(status="QUEUED").first()
        if queue is not None:
            queue.status = "EXECUTED"
            queue.save_to_db()

        campaign = CampaignsModel.query.filter_by(id=queue.campaign_id).first()
        campaign.sent_campaign += 1
        campaign.save_to_db()
    except Exception as exc:
        self.retry(exc=exc, max_retries=3)

    saved_replies = data["saved_reply"]
    for reply in saved_replies:
        message_type = reply["message_type"]
        if message_type == "FB_TEXT":
            text = reply["text"]
            payload = {
                "user_id": recipient_id,
                "text": text,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/text'.format(WEBHOOK_URL)
            send_message.delay(url, payload)
        elif message_type == "FB_ATTACHMENT":
            attachment_url = reply["attachment_url"]
            payload = {
                "user_id": recipient_id,
                "attachment_url": attachment_url,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/attachment'.format(WEBHOOK_URL)
            send_message.delay(url, payload)
        elif message_type == "FB_QUICK_REPLY":
            text = reply["text"]
            quick_replies = reply["quick_replies"]
            payload = {
                "recipient_id": recipient_id,
                "quick_replies": quick_replies,
                "text": text,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/quick_reply'.format(WEBHOOK_URL)
            send_message.delay(url, payload)
        elif message_type == "FB_BUTTON_TEMPLATE":
            text = reply["text"]
            buttons = reply["buttons"]            
            payload = {
                "recipient_id": recipient_id,
                "buttons": buttons,
                "text": text,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/button_template'.format(WEBHOOK_URL)
            send_message.delay(url, payload)
        elif message_type == "FB_GENERIC_TEMPLATE":
            templates = reply["message_templates"]
            urls = []
            for template in templates:
                urls.append(template["image_url"])
                buttons = reply["buttons"]            
                for button in buttons:
                    if button["type"] == "web_url":
                        urls.append(button["url"])

            # Whitelist URL
            if len(urls) > 0:
                whitelist_request = {
                    "whitelisted_domains": urls
                }
                whitelist_url = "{0}/messenger_profile?access_token={1}".format(fb_graph, page_access_token)
                rv = requests.post(whitelist_url, json=whitelist_request)
                if rv.status_code != 200:
                    return abort(400, "Url can't be whitelisted, make sure it use https.")

            payload = {
                "recipient_id": recipient_id,
                "elements": templates,
                "page_access_token": page_access_token
            }
            url = '{}/messenger/messages/generic_template'.format(WEBHOOK_URL)
            send_message.delay(url, payload)

            # Send typing
            tp = {
                "recipient": {
                    "id": recipient_id
                },
                "sender_action": "typing_on"
            }
            


    return True
