import pprint
import jwt
from flask import request, Blueprint, current_app, jsonify
from ....utilities.abort import abort
from ...account.models import AccountModel
from ...payment.models import PaymentModel, PaymentTransactionModel
from ....extensions.db import db
from ....utilities.time import timestamp

mod = Blueprint('charging', __name__)

@mod.route('/<jwt_token>', methods=['POST'])
def charging_connector(jwt_token):
	"""Define the webhook data and save it to database."""
	SECRET_KEY = current_app.config['SECRET_KEY']
	try:
	    data = jwt.decode(jwt_token, SECRET_KEY, algorithm=['HS256'])
	    # When the time expiration (5s) is exceeded, raised 401 error
	except (jwt.ExpiredSignatureError, jwt.InvalidTokenError, jwt.DecodeError):
	    return abort(401)

	# Log whenever the messages received in debug mode
	if current_app.debug:
		current_app.logger.info("Request data: " + pprint.pformat(data))

	try:
		data = data['data']
		subscription_id = data['subscription_id']
		charge_id = data['id']
		status = data['status']
		capture_amount = data['capture_amount']
		card_type = data['card_type']
		eci = data['eci']
		masked_card_number = data['masked_card_number']
	except:
		abort(400)

	payment_transaction = PaymentTransactionModel(
		subscription_id=subscription_id,
		status=status,
		capture_amount=capture_amount,
		created=timestamp(),
		card_type=card_type,
		charge_id=charge_id,
		eci=eci,
		masked_card_number=masked_card_number
	)
	payment_transaction.save_to_db()

	# Update account date updated
	query = db.session.query(AccountModel, PaymentModel).\
			filter(PaymentModel.xendit_subscription_id==payment_transaction.subscription_id).\
			filter(PaymentModel.is_active==True).\
			filter(AccountModel.id==PaymentModel.account_id).\
			filter(AccountModel.deleted==False).first()
	try:
		account = getattr(query, "AccountModel")
		package = getattr(query, "PaymentModel")
	except AttributeError:
		return abort(404, "Account not found.")
	account.date_updated = timestamp()
	account.save_to_db()

	return '',201
