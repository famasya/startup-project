# pylint: disable=C0103, E1101

"""
WEBHOOK CONNECTOR
~~~~~~~~~~~~~~~~~
From webhook, we send data to this route, so we can locate the exact
data based on account_id and save it to database.

Note: current_user from flask_login can't be used here because
the request is from webhook-v2, not from user
"""
import uuid
import pprint
import jwt
from flask import request, Blueprint, current_app, jsonify
from ....utilities.abort import abort
from ....extensions.flask_redis import conn
from ....utilities.time import timestamp

from ..postback_actions import postback_action
from ..bot_triggers import is_bot_triggered
from ...user.models import UserModel
from ...channel.models import ChannelModel
from ...saved_reply.models import SavedReplyModel
from ...account.models import AccountModel
from ...campaigns.models import WoocommerceCartLog, CampaignsModel, CampaignsQueue
from ...campaigns.schemas import campaign_schema, campaigns_schema
from ...account_package.models import AccountPackage
from ...inbox.models import InboxModel, InboxSession
from ...message.ops import create_message
from ...message.tasks import send_message
from ...inbox.schemas import inbox_schema
from ...integration.models import IntegrationModel
from ...inbox.sockets import (
    emit_websocket_inbox, emit_counter_inbox
)
from ...message.models import MessageModel, MessageLogModel
from ...message.schemas import message_schema
from ...message.sockets import emit_websocket_message
from ...customer.models import CustomerModel
from ...saved_reply.ops import get_saved_reply_detail
from ...saved_reply.schemas import saved_reply_schema
from ...webhook_notification.messages import receive_message
from ....utilities.unique_url import (
    uuid_url64, customer_uuid_url64
)
from ....extensions.db import db
from ...zendesk.tasks import fetch_customer_data
from ...woocommerce.tasks import fetch_woocommerce_data
from ..tasks import send_shortlink_message, create_reminder, send_abandon_cart_reminder, send_woo_reminder
from ...message.tasks import send_message
import ast
import requests
from urllib.parse import urlparse
mod = Blueprint('messenger', __name__)


# Route: /messenger/hello
@mod.route('/hello')
def hello():
    """Just a secret message."""
    return 'If you see this message, then you are awesome!'


# Route: /messenger/webhook_test
@mod.route('/webhook_test', methods=['POST'])
def webhook_test():
    """Just for testing"""
    data = request.json

    # Log whenever the messages received in debug mode
    if current_app.debug:
        current_app.logger.info("Request data: " + pprint.pformat(data))
    return '', 200


# Route: /messenger/somesecretstringtogetaccesstoken
@mod.route('/<jwt_token>', methods=['GET'])
def get_access_token(jwt_token):
    SECRET_KEY = current_app.config['SECRET_KEY']

    # Decode retrieved JWT token
    try:
        data = jwt.decode(jwt_token, SECRET_KEY, algorithm=['HS256'])
        # When the time expiration (5s) is exceeded, raised 401 error
    except (jwt.ExpiredSignatureError, jwt.InvalidTokenError, jwt.DecodeError):
        return abort(401)

    try:
        recipient_id = data.get('recipient_id')  # page_id
    except AttributeError:
        return abort(400)
    messenger = ChannelModel.query.filter_by(page_id=recipient_id).first()
    if messenger:
        resp = {
            "page_access_token": messenger.page_access_token
        }
        return jsonify(resp), 200
    return jsonify({"message": "page_id not found"}), 404


def add_customer(account_id):
    # Add customer to current account
    query = db.session.query(AccountModel, AccountPackage).\
            filter(AccountModel.id == account_id).\
            filter(AccountModel.deleted == False).\
            filter(AccountModel.package_id == AccountPackage.id).first()

    try:
        account = getattr(query, "AccountModel")
        package = getattr(query, "AccountPackage")
    except:
        return ''

    # Check if current_customer is exceeding limit
    if account.current_customer >= package.customer_limit:
        status = 'hold'
    else:
        status = 'open'
        account.current_customer += 1
        account.save_to_db()

    return status


# Route: /messenger/connector
@mod.route('/connector/<jwt_token>', methods=['POST'])
def messenger_connector(jwt_token):
    """Define the webhook data and save it to database.
    there's two kind of data that we will receive:
    1. Messaging data: when customer send the message text or attachment
    2. Postback data: when customer tap into button that contain postback
    There're two type of postback data:
    1. when user tap into postback button in generic template, button template, or quick reply.
    2. when user trigger action, such as: click m.me link, click message us in ads, click get starter
    for the first time, or click page in discovery tab.
    """
    SECRET_KEY = current_app.config['SECRET_KEY']
    # Decode retrieved JWT token
    try:
        data = jwt.decode(jwt_token, SECRET_KEY, algorithm=['HS256'])
        # When the time expiration (5s) is exceeded, raised 401 error
    except (jwt.ExpiredSignatureError, jwt.InvalidTokenError, jwt.DecodeError):
        return abort(401)

    try:
        data = request.json['data']
    except:
        pass

    message_id = data.get('message_id')
    reply_id = data.get('reply_id')
    type_ = data.get('type')
    session = data.get('session')
    campaign_id = data.get('campaign_id')

    # Logs to correlate with stats
    if message_id is not None:
        if campaign_id == "None":
            campaign_id = 0

        logs = MessageLogModel(
            mid=message_id,
            type=type_,
            session=session,
            campaign_id=campaign_id,
            saved_reply_id=reply_id
        )
        logs.save_to_db()
        return '', 200

    # Get if it is an Echo
    is_echo = data.get("message").get("is_echo")
    if is_echo is not None:
        timestamp = data.get("timestamp")
        mid = data.get("message").get("mid")
        log = MessageLogModel.query.filter_by(mid=mid).first()
        if log is None:
            return '', 201

        log.watermark = timestamp
        log.save_to_db()

        # Increase saved_reply total sent
        saved_reply = SavedReplyModel.query.filter_by(id=log.saved_reply_id).first()
        print(log.saved_reply_id, saved_reply)
        if log.campaign_id == 0:            
            saved_reply.sent += 1
            saved_reply.save_to_db()

        return '', 200

    # Log whenever the messages received in debug mode
    if current_app.debug:
        current_app.logger.info("Request data: " + pprint.pformat(data))

    message_type = data.get("type")

    # Get data from webhook
    avatar_url = data.get("avatar_url")
    recipient_id = data.get("recipient").get("id")
    if message_type == "FB_CHECKBOX":
        return handle_fb_checkbox(data)
    elif message_type == "FB_CHECKBOX_CONFIRMED":
        data = trigger_campaigns(data)
        if data is None:
            return '', 200
        first_name = data.get("first_name")
        last_name = data.get("last_name")
        sender_id = data.get("sender").get("id")
    else:
        first_name = data.get("first_name")
        last_name = data.get("last_name")
        sender_id = data.get("sender").get("id")

    if message_type == "FB_TEXT" or message_type == "FB_ATTACHMENT":
        message_id = data.get("message").get("mid")
        seq = data.get("message").get("seq")

    # Find exact account_id from page_id
    messenger = ChannelModel.query.\
                filter_by(page_id=recipient_id).\
                filter_by(deleted=False).first()
    if not messenger or messenger.deleted:
        return abort(404, "messenger not found")

    account_id = messenger.account_id

    if message_type == 'FB_TEXT':
        text = data.get("message").get("text")
    if message_type == 'FB_ATTACHMENT':
        text = "New customer sent you an attachment..."
    if message_type == "FB_POSTBACK":
        text = data.get("postback").get("title")
    if message_type == "FB_SHORTLINK_RECURRENT" or message_type == "FB_POSTBACK_SHORTLINK":
        text = "SHORTLINK message..."
    elif message_type == "FB_GETTING_STARTED":
        text = "Starting user session"
    elif message_type == "FB_CHECKBOX_CONFIRMED":
        text = "User confirmation"

    # Check if customer and inbox has been saved before, if so then pass
    customer = CustomerModel.query.\
               filter_by(account_id=account_id).\
               filter_by(platform_id=sender_id).\
               filter_by(deleted=False).first()
    if customer is None:
        # New Customer object
        customer = CustomerModel(
            id=uuid.uuid1().hex,
            name='{} {}'.format(first_name, last_name),
            avatar_url=avatar_url,
            platform='messenger',
            platform_id=sender_id, # facebook user id
            account_id=account_id
        )

        status = add_customer(account_id)

        # New Inbox object
        inbox = InboxModel(
            id=uuid.uuid1().hex,
            account_id=account_id,
            customer_id=customer.id,
            short_text=text,
            status=status,
            via_channel=messenger.page_name
        )

        # New Inbox Session object
        new_session = InboxSession(
            inbox_id=inbox.id,
        )

        # Save to db
        try:
            db.session.add_all([customer, inbox, new_session])
            db.session.commit()
        except:
            db.session.rollback()
            raise

        # ===========================================
        # Emit new inbox as counter
        # ===========================================
        emit_counter_inbox.delay(account_id)

    else:
        inbox = InboxModel.query.\
                filter_by(account_id=account_id).\
                filter_by(customer_id=customer.id).first()
        if inbox is None:

            status = add_customer(account_id)

            # New Inbox object
            inbox = InboxModel(
                id=uuid.uuid1().hex,
                account_id=account_id,
                customer_id=customer.id,
                short_text=text,
                status=status,
                via_channel=messenger.page_name
            )

            # New Inbox Session object
            new_session = InboxSession(
                inbox_id=inbox.id,
            )

            # Save to db
            try:
                db.session.add_all([customer, inbox, new_session])
                db.session.commit()
            except:
                db.session.rollback()
                raise

            # ===========================================
            # Emit new inbox as counter
            # ===========================================
            emit_counter_inbox.delay(account_id)

    # when message_type postback, we don't save seq and message_id
    if message_type == "FB_POSTBACK" or message_type == "FB_SHORTLINK_RECURRENT" or message_type == "FB_POSTBACK_SHORTLINK" or message_type == "FB_GETTING_STARTED" or message_type == "FB_CHECKBOX_CONFIRMED":
        if message_type == "FB_SHORTLINK_RECURRENT" or message_type == "FB_POSTBACK_SHORTLINK":
            text = "Shortlink postback..."
        elif message_type == "FB_GETTING_STARTED":
            text = "Starting user session"
        elif message_type == "FB_CHECKBOX_CONFIRMED":
            text = "User confirmation"
        else:
            text = data.get("postback").get("title")

        message = MessageModel(
            message_type=message_type,
            avatar_url=avatar_url,
            sender_id=sender_id,
            recipient_id=recipient_id,
            status='received',
            account_id=account_id,
            inbox_id=inbox.id,
            author="{} {}".format(first_name, last_name)
        )
        message.save_to_db()

    else:
        message = MessageModel(
            id=message_id,
            message_type=message_type,
            avatar_url=avatar_url,
            sender_id=sender_id,
            recipient_id=recipient_id,
            status='received',
            seq=seq,
            account_id=account_id,
            inbox_id=inbox.id,
            author="{} {}".format(first_name, last_name)
        )
        message.save_to_db()

    # Update last_updated inbox
    inbox.last_updated = message.timestamp

    # Update customer last_seen
    customer.ping()

    # Check if inbox is closed, then update to open and add 1 of session
    if inbox.status == "closed":
        fetch_customer_data.delay(None, account_id, inbox.customer_id, inbox_id=inbox.id)
        fetch_woocommerce_data.delay(account_id, inbox.id)
        inbox.status = "open"
        inbox.status_session += 1
        inbox.save_to_db()

        # New Inbox Session object
        new_session = InboxSession(
            inbox_id=inbox.id,
        )
        new_session.save_to_db()

        # =============================================
        # Emit to socketio
        # =============================================
        emit_counter_inbox.delay(account_id)

    # If the data sent is text type
    if message_type == 'FB_TEXT':
        text = data.get("message").get("text")
        if "quick_reply" in data["message"]:
            payload = data.get("message").get("quick_reply").get("payload")
        else:
            payload = None

        message.text = text
        message.save_to_db()

        # Update inbox short text with the lastest message
        # If inbox_status is closed, then update it to open
        inbox.short_text = text
        inbox.save_to_db()

        # =============================================
        # Emit to socketio
        # =============================================
        # Emit inbox and message to client
        data_message = message_schema.dump(message).data
        message_list = [data_message]  # To make consistent, change into list
        data_inbox = inbox_schema.dump(inbox).data
        inbox_id = data_inbox["id"]
        emit_websocket_message.delay(message_list, inbox_id)
        emit_websocket_inbox.delay(data_inbox)


        if payload is None:
            # =============================================
            # Check if there is bot triggered
            # =============================================
            is_bot_triggered(account_id, inbox.id, recipient_id, sender_id, message.text, channel_type='messenger')

        else:
            # =============================================
            # Trigger postback action
            # =============================================
            postback_action(account_id, inbox.id, recipient_id, sender_id, payload, channel_type='messenger')


    # If the data sent is attachment type
    if message_type == 'FB_ATTACHMENT':
        attachment_url = data.get("message").get("attachments")[0].get("payload").get("url")

        message.attachment_url = attachment_url
        message.save_to_db()

        # Update inbox short text with the lastest message
        inbox.short_text = '{} sent you an attachment...'.format(customer.name)
        inbox.save_to_db()

        # =============================================
        # Emit to socketio
        # =============================================
        # Emit inbox and message to client
        data_message = message_schema.dump(message).data
        message_list = [data_message]  # To make consistent, change into list
        data_inbox = inbox_schema.dump(inbox).data
        inbox_id = data_inbox["id"]
        emit_websocket_message.delay(message_list, inbox_id)
        emit_websocket_inbox.delay(data_inbox)

    if message_type == "FB_POSTBACK" or message_type == "FB_SHORTLINK_RECURRENT" or message_type == "FB_POSTBACK_SHORTLINK" or message_type == "FB_GETTING_STARTED":
        """This type of message is when user click button that contain postback
        whether in button template, quick reply, or generic template"""

        if message_type == "FB_SHORTLINK_RECURRENT" or message_type == "FB_POSTBACK_SHORTLINK":
            title = "Shortlink postback..."
            payload = None
            if message_type == "FB_SHORTLINK_RECURRENT" and messenger.shortlink_recurrent != None:
                get_replies = get_saved_reply_detail(account_id, messenger.shortlink_recurrent, json_only=True)
            elif message_type == "FB_POSTBACK_SHORTLINK" and messenger.shortlink_postback != None:
                get_replies = get_saved_reply_detail(account_id, messenger.shortlink_postback, json_only=True)
            else:
                return '', 201

            saved_reply = get_replies["saved_reply"]
            user = {
                "first_name": "Auto",
                "last_name": "Reply"
            }
            send_shortlink_message.delay(account_id, inbox.id, saved_reply, data, user)
        elif message_type == "FB_GETTING_STARTED":
            is_bot_triggered(account_id, inbox.id, recipient_id, sender_id, message.text, channel_type='messenger', getting_started=True)
            title = "Getting started..."
        else:
            if data.get("standby") is not None:
                title = data.get("standby").get("title")
                payload = data.get("standby").get("payload")
            else:
                title = data.get("postback").get("title")
                payload = data.get("postback").get("payload")

        message.text = title
        message.save_to_db()

        # Update inbox short text with the lastest message
        inbox.short_text = '{} triggered a postback...'.format(customer.name)
        inbox.save_to_db()

        # =============================================
        # Trigger postback action
        # =============================================
        if message_type != "FB_GETTING_STARTED" and payload is not None:
            postback_action(account_id, inbox.id, recipient_id, sender_id, payload, channel_type='messenger')

        # =============================================
        # Emit to socketio
        # =============================================
        # Emit inbox and message to client
        data_message = message_schema.dump(message).data
        message_list = [data_message]  # To make consistent, change into list
        data_inbox = inbox_schema.dump(inbox).data
        inbox_id = data_inbox["id"]
        emit_websocket_message.delay(message_list, inbox_id)
        emit_websocket_inbox.delay(data_inbox)


    # Notify client webhook
    if message_type != "FB_CHECKBOX_CONFIRMED":
        receive_message(account_id, message, inbox)
        create_reminder.delay(account_id, data_inbox, current_app.config["API_URL"])

    return '', 201

def handle_fb_checkbox(data):
    ref_data = ast.literal_eval(data.get("optin").get("ref"))
    first_name = ref_data.get("first_name")
    last_name = ref_data.get("last_name")
    recipient_id = data.get("recipient").get("id")
    user_ref = data.get("optin").get("user_ref")
    account_id = ref_data.get("account_id")
    page_id = ref_data.get("page_id")

    WEBHOOK_URL = current_app.config['WEBHOOK_URL']

    channel = ChannelModel.query.filter_by(page_id=recipient_id).filter_by(deleted=False).first()
    if channel is None:
        return '', 200

    page_access_token = channel.page_access_token
    query = db.session.query(IntegrationModel, CampaignsModel).\
            filter(IntegrationModel.page_id == page_id).\
            filter(IntegrationModel.deleted == False).\
            filter(CampaignsModel.bot_id == IntegrationModel.bot_id).\
            filter(CampaignsModel.deleted == False).\
            filter(CampaignsModel.triggered_after == "CONFIRMATION_MSG").\
            with_entities(CampaignsModel).first()
    if query is None:
        return '', 200

    saved_reply_confirm = saved_reply_schema.dump(query.replies[0]).data
    quick_replies = saved_reply_confirm['saved_reply'][0]['quick_replies']
    quick_replies[0]["content_type"] = "text"
    payload = {
        "recipient_id": user_ref,
        "quick_replies": quick_replies,
        "text": saved_reply_confirm['saved_reply'][0]['text'],
        "page_access_token": page_access_token
    }
    url = '{}/messenger/messages/quick_reply?user_ref=true'.format(WEBHOOK_URL)

    # Get bot_id
    integration = IntegrationModel.query.filter_by(page_id=page_id).\
                  filter_by(type="WOOCOMMERCE").\
                  filter_by(deleted=False).first()
    if integration is None:
        print("No integration found")
        return '', 200

    send_message.delay(url, payload)

    bot_id = integration.bot_id

    # Save to woocommerce log
    cart_log = WoocommerceCartLog(
        user_ref=user_ref,
        bot_id=bot_id,
        data=ref_data
    )
    cart_log.save_to_db()

    return '', 201

def trigger_campaigns(data):

    orig_data = data
    sender_id = data.get("sender").get("id")
    recipient_id = data.get("recipient").get("id")
    user_ref = data.get("prior_message").get("identifier")
    WEBHOOK_URL = current_app.config['WEBHOOK_URL']
    fb_graph = current_app.config['FB_GRAPH']

    woo_cart = WoocommerceCartLog.query.filter_by(user_ref=user_ref).first()
    if woo_cart is None:
        return None

    ref_data = woo_cart.data

    channel = ChannelModel.query.filter_by(page_id=recipient_id).\
              filter_by(deleted=False).first()
    if channel is None:
        return None

    page_access_token = channel.page_access_token

    campaigns = CampaignsModel.query.filter_by(triggered_by="WOOCOMMERCE").\
                filter_by(bot_id=woo_cart.bot_id).filter_by(deleted=False).\
                filter_by(is_active=True).all()

    for campaign in campaigns:
        campaign_data = campaign_schema.dump(campaign).data
        campaign_data["content"]["message"][0]["saved_reply"] = sorted(campaign_data["content"]["message"][0]["saved_reply"], key=lambda k: k["order"])
        uuid_ = uuid.uuid1().hex
        send_after = campaign_data["content"]["send_after"]
        triggered_after = campaign_data["content"]["triggered_after"]
        if campaign_data["content"]["triggered_after"] == "LAST_CART":
            # If LAST_CART, create and queue tasks content immidiately
            data = campaign_data["content"]["message"][0]
            send_abandon_cart_reminder.apply_async(
                args=[ref_data, user_ref, current_app.config["FB_GRAPH"], page_access_token, uuid_],
                countdown=int(campaign_data["content"]["send_after"]),
                task_id=uuid_
            )
            queue = CampaignsQueue(
                campaign_id = campaign_data["id"],
                send_after=send_after,
                execution_id=uuid_,
                recipient_id=sender_id,
                triggered_by="WOOCOMMERCE",
                triggered_after=triggered_after,
                access_token=page_access_token,
                user_ref=user_ref,
                messages=campaign_data["content"]["message"]
            )
            queue.save_to_db()

            # Create task for woocommerce LAST_CART message
            task_id = uuid.uuid1().hex+"1"
            send_woo_reminder.apply_async(
                args=[data, WEBHOOK_URL, sender_id, page_access_token, fb_graph, task_id],
                countdown=int(campaign_data["content"]["send_after"]),
                task_id=task_id
            )
            queue = CampaignsQueue(
                campaign_id = campaign_data["id"],
                send_after=send_after,
                execution_id=task_id,
                recipient_id=sender_id,
                triggered_by="WOOCOMMERCE",
                triggered_after=triggered_after,
                access_token=page_access_token,
                user_ref=user_ref,
                messages=campaign_data["content"]["message"]
            )
            queue.save_to_db()

        else:
            # Create queue for another campaign type
            queue = CampaignsQueue(
                campaign_id = campaign_data["id"],
                send_after=send_after,
                execution_id=uuid_,
                recipient_id=sender_id,
                triggered_by="WOOCOMMERCE",
                triggered_after=triggered_after,
                access_token=page_access_token,
                user_ref=user_ref,
                messages=campaign_data["content"]["message"]
            )
            queue.save_to_db()


        # Update campaign customer
        campaign.total_customer += 1
        campaign.save_to_db()


    return orig_data
