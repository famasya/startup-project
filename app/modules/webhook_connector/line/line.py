# pylint: disable=C0103, E1101

"""
LINE CONNECTOR
~~~~~~~~~~~~~~~~~
From webhook, we send data to this route, so we can locate the exact
data based on account_id and save it to database.

Note: current_user from flask_login can't be used here because
the request is from webhook-v2, not from user
"""
import uuid
import requests
import pprint
import jwt
from flask import request, Blueprint, current_app
from ....utilities.abort import abort

from ..bot_triggers import is_bot_triggered
from ..postback_actions import postback_action
from ...channel.models import ChannelModel
from ...account.models import AccountModel
from ...account_package.models import AccountPackage
from ...inbox.schemas import inbox_schema
from ...inbox.models import InboxModel, InboxSession
from ...inbox.sockets import (
    emit_websocket_inbox, emit_counter_inbox
)
from ..tasks import create_reminder
from ...message.models import MessageModel
from ...message.schemas import message_schema
from ...message.sockets import emit_websocket_message
from ...customer.models import CustomerModel
from ...webhook_notification.messages import receive_message
from ...zendesk.tasks import fetch_customer_data
from ...woocommerce.tasks import fetch_woocommerce_data
from ....utilities.unique_url import uuid_url64
from ...image_processing.ops import upload_file

from ....extensions.db import db

mod = Blueprint('line', __name__)


# Route: /line/hello
@mod.route('/hello')
def hello():
    """Just a secret message."""
    return 'If you see this message, then you are awesome!'

def add_customer(account_id):
    # Add customer to current account
    query = db.session.query(AccountModel, AccountPackage).\
            filter(AccountModel.id == account_id).\
            filter(AccountModel.deleted == False).\
            filter(AccountModel.package_id == AccountPackage.id).first()

    try:
        account = getattr(query, "AccountModel")
        package = getattr(query, "AccountPackage")
    except:
        return ''

    # Check if current_customer is exceeding limit
    if account.current_customer >= package.customer_limit:
        status = 'hold'
    else:
        status = 'open'
        account.current_customer += 1
        account.save_to_db()

    return status

# Route: /line/connector
@mod.route('/connector/<jwt_token>', methods=['POST'])
def line_connector(jwt_token):
    """Define the webhook data and save it to database."""
    SECRET_KEY = current_app.config['SECRET_KEY']
    # Decode retrieved JWT token
    try:
        data = jwt.decode(jwt_token, SECRET_KEY, algorithm=['HS256'])
        # When the time expiration (5s) is exceeded, raised 401 error
    except (jwt.ExpiredSignatureError, jwt.InvalidTokenError, jwt.DecodeError):
        return abort(401)

    data = request.json['data']

    # Log whenever the messages received in debug mode
    if current_app.debug:
        current_app.logger.info("Request data: " + pprint.pformat(data))

    # Define the data
    sender_id = data.get("source").get("userId")
    recipient_id = data.get("recipient_id")
    message_type = data.get("message_type").upper()
    if message_type != "LINE_POSTBACK":
        message_id = data.get("message").get("id")

    # When user register the same channel that already in db
    # That existing channel will be deleted
    # Find exact account_id from page_id
    line = ChannelModel.query.filter_by(id=recipient_id).filter_by(deleted=False).first()
    if line is None:
        return abort(404, "Channel not found")

    # Get channel access token
    channel_access_token = line.channel_access_token
    account_id = line.account_id

    # Get user name and avatar_url
    url = 'https://api.line.me/v2/bot/profile/{0}'.format(sender_id)
    headers = {"Authorization": "Bearer {0}".format(channel_access_token)}
    r = requests.get(url, headers=headers)
    resp = r.json()

    try:
        user_name = resp['displayName']
        avatar_url = resp['pictureUrl']
    except (TypeError, KeyError):
        return abort(400, 'Invalid request')

    # Check if customer and inbox has been saved before, if so then pass
    customer = CustomerModel.query.\
               filter_by(account_id=account_id).\
               filter_by(platform_id=sender_id).first()
    if customer is None:
        # Save Customer
        customer = CustomerModel(
            id=uuid.uuid1().hex,
            platform='line',
            platform_id=sender_id, # line user id
            name=user_name,
            avatar_url=avatar_url,
            account_id=account_id
        )

        status = add_customer(account_id)

        # Save Inbox
        inbox = InboxModel(
            id=uuid.uuid1().hex,
            status=status,
            account_id=account_id,
            customer_id=customer.id,
            via_channel=line.channel_name
        )

        # New Inbox Session object
        new_session = InboxSession(
            inbox_id=inbox.id,
        )

        # Save to db
        try:
            db.session.add_all([customer, inbox, new_session])
            db.session.commit()
        except:
            db.session.rollback()
            raise

        # ===========================================
        # Emit new inbox as counter
        # ===========================================
        emit_counter_inbox.delay(account_id)

    else:
        inbox = InboxModel.query.\
                filter_by(account_id=account_id).\
                filter_by(customer_id=customer.id).\
                filter_by(via_channel=line.channel_name).\
                first()
        if inbox is None:

            status = add_customer(account_id)

            # Save Inbox
            inbox = InboxModel(
                id=uuid.uuid1().hex,
                status=status,
                account_id=account_id,
                customer_id=customer.id,
                via_channel=line.channel_name
            )

            # New Inbox Session object
            new_session = InboxSession(
                inbox_id=inbox.id,
            )

            # Save to db
            try:
                db.session.add_all([customer, inbox, new_session])
                db.session.commit()
            except:
                db.session.rollback()
                raise

            # ===========================================
            # Emit new inbox as counter
            # ===========================================
            emit_counter_inbox.delay(account_id)

    if message_type == "LINE_POSTBACK":
        message = MessageModel(
            message_type=message_type,
            sender_id=sender_id,
            recipient_id=recipient_id,
            avatar_url=avatar_url,
            status='received',
            account_id=account_id,
            inbox_id=inbox.id,
            author=user_name
        )
        message.save_to_db()
    else:
        message = MessageModel(
            id=message_id,
            message_type=message_type,
            sender_id=sender_id,
            recipient_id=recipient_id,
            avatar_url=avatar_url,
            status='received',
            account_id=account_id,
            inbox_id=inbox.id,
            author=user_name
        )
        message.save_to_db()

    # Update last_updated inbox
    inbox.last_updated = message.timestamp

    # Update customer last_seen
    customer.ping()

    # Check if inbox is closed, then update to open and add 1 of session
    if inbox.status == "closed":
        fetch_customer_data.delay(None, account_id, inbox.customer_id, inbox_id=inbox.id)
        fetch_woocommerce_data.delay(account_id, inbox.id)
        inbox.status = "open"
        inbox.status_session += 1
        inbox.save_to_db()

        # New Inbox Session object
        new_session = InboxSession(
            inbox_id=inbox.id,
        )
        new_session.save_to_db()

        # =============================================
        # Emit to socketio
        # =============================================
        emit_counter_inbox.delay(account_id)

    # If the data sent is text type
    if message_type == 'LINE_TEXT':
        text = data.get("message").get("text")

        message.text = text
        message.message_type = message_type
        message.save_to_db()

        # Update inbox short text with the lastest message
        inbox.short_text = text
        inbox.save_to_db()

        # =============================================
        # Emit to socketio
        # =============================================
        # Emit inbox and message to client
        data_message = message_schema.dump(message).data
        message_list = [data_message]  # To make consistent, change into list
        data_inbox = inbox_schema.dump(inbox).data
        inbox_id = data_inbox["id"]
        emit_websocket_message.delay(message_list, inbox_id)
        emit_websocket_inbox.delay(data_inbox)

        # =============================================
        # Check if there is bot triggered
        # =============================================
        is_bot_triggered(account_id, inbox.id, recipient_id, sender_id, message.text, channel_type='line')

    # If the data sent is attachment type
    if message_type == 'LINE_ATTACHMENT':
        # Get image binary format from LINE
        url = "https://api.line.me/v2/bot/message/{0}/content".format(message_id)
        headers = {"Authorization": "Bearer {0}".format(channel_access_token)}
        r = requests.get(url, headers=headers, stream=True)
        if r.status_code == 200:
            image = r.content

        # =============================================
        # Post it to Cloudinary
        # =============================================
        resp = upload_file(image)

        # Save it to database
        message.message_type = message_type
        message.attachment_url = resp['secure_url']
        message.save_to_db()

        # Update inbox short text with the lastest message
        inbox.short_text = "Customer sent you attachment..."
        inbox.save_to_db()

        # =============================================
        # Emit to socketio
        # =============================================
        # Emit inbox and message to client
        data_message = message_schema.dump(message).data
        message_list = [data_message]  # To make consistent, change into list
        data_inbox = inbox_schema.dump(inbox).data
        inbox_id = data_inbox["id"]
        emit_websocket_message.delay(message_list, inbox_id)
        emit_websocket_inbox.delay(data_inbox)

    if message_type == "LINE_POSTBACK":
        payload = data["postback"]["data"]

        message.text = "{{ Customer just tap your postback }}"
        message.message_type = message_type

        # Update inbox short text with the lastest message
        inbox.short_text = "Customer just tap a postback {}".format(payload)
        inbox.save_to_db()

        message.save_to_db()

        # =============================================
        # Trigger postback action
        # =============================================
        postback_action(account_id, inbox.id, recipient_id, sender_id, payload, channel_type='line')

        # =============================================
        # Emit to socketio
        # =============================================
        # Emit inbox and message to client
        data_message = message_schema.dump(message).data
        message_list = [data_message]  # To make consistent, change into list
        data_inbox = inbox_schema.dump(inbox).data
        inbox_id = data_inbox["id"]
        emit_websocket_message.delay(message_list, inbox_id)
        emit_websocket_inbox.delay(data_inbox)

    # Notify client webhook
    receive_message(account_id, message, inbox)
    create_reminder.delay(account_id, data_inbox, current_app.config["API_URL"])

    return '', 201
