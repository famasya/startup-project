# pylint: disable=E1101

"""List of Operation for Integration API"""
from itsdangerous import URLSafeSerializer
from flask import jsonify, current_app
from ...utilities.abort import abort
from werkzeug.security import generate_password_hash
from .models import IntegrationModel, NexmoTemplate
from .schemas import integration_schema, integrations_schema, sms_template
from ..account.models import AccountModel
from ..payment.models import PaymentModel
from ..bot.models import BotModel
from ..campaigns.ops import delete_campaign
from ..channel.models import ChannelModel
from ..saved_reply.models import SavedReplyModel
from ..account_package.models import AccountPackage
from ..campaigns.models import CampaignsModel
from ..campaigns.ops import create_campaign, delete_campaign
from ...utilities.unique_url import uuid_url64
from ...utilities.time import timestamp
from ...extensions.db import db
from urllib.parse import urlparse
from datetime import timedelta
import requests

def get_integration_list(account_id):
    """Get list of integrations."""
    objs = db.session.query(AccountModel, IntegrationModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(IntegrationModel.account_id==AccountModel.id).\
          filter(IntegrationModel.deleted==False).all()
    interations = []
    for obj in objs:
        try:
            intergration = getattr(obj, "IntegrationModel")
            interations.append(intergration)
        except AttributeError:
            pass

    resp = integrations_schema.jsonify(interations)
    return resp


def create_integration(data, account_id, integration_id=None, update=False):
    """Create new integration based on type"""
    WEBHOOK_URL = current_app.config['WEBHOOK_URL']
    account = AccountModel.query.\
              filter_by(id=account_id).\
              filter_by(deleted=False).first()
    if account is None:
        return abort(404, "Account not found.")

    if update is False:
        try:
            _type = data['type'].upper()
        except (KeyError, TypeError):
            return abort(400, "Invalid request")

        integration = IntegrationModel.query.filter_by(account_id=account_id).\
                      filter_by(deleted=False).filter_by(type=_type).first()
        if integration is not None:
            abort(400, "Integration type '{}' exists".format(_type))

    else:
        if integration_id is None:
            return abort(400, "integration_id is required.")
        integration = IntegrationModel.query.\
                      filter_by(account_id=account.id).\
                      filter_by(deleted=False).\
                      filter_by(id=integration_id).first()
        if integration is None:
            return abort(404, "Integration not found.")

        _type = integration.type

    if _type == "ZENDESK":
        try:
            url = data["url"]
            token = data["token"]
            email = data["email"]
        except (KeyError, TypeError):
            return abort(400, "Zendesk invalid request")

        if update is True:
            integration.url = url
            integration.token = token
            integration.email = email
            integration.date_updated = timestamp()
            integration.save_to_db()
        else:
            integration = IntegrationModel(
                type=_type,
                url=url,
                email=email,
                token=token,
                account_id=account_id
            )
            integration.save_to_db()

    elif _type == "SMOOCH":
        try:
            app_id = data["app_id"]
            secret = data["secret"]
            key_id = data["key_id"]
        except (KeyError, TypeError):
            return abort(400, "Smooch invalid request.")

        if update is True:
            integration.app_id = app_id
            integration.secret = secret
            integration.key_id = key_id
            integration.date_updated = timestamp()
            integration.save_to_db()
        else:
            integration = IntegrationModel(
                type=_type,
                account_id=account_id,
                app_id=app_id,
                key_id=key_id,
                secret=secret
            )

            # Use itsdangerous to generate unique string from channel_id webhook url
            s = URLSafeSerializer(current_app.config['SECRET_KEY'])
            unique_string = s.dumps(integration.id)
            webhook_url = '{0}/smooch/{1}'.format(WEBHOOK_URL, unique_string)
            integration.webhook_url = webhook_url
            integration.save_to_db()

    elif _type == "WOOCOMMERCE":
        try:
            url = data['url']
            updated_bot_id = data['bot_id']
        except:
            abort(400, 'Invalid woocommerce request')

        if urlparse(url).scheme != 'https':
            abort(400, 'URL must use SSL / HTTPS')

        if url[-1:] != '/':
            url += '/'

        if updated_bot_id is not None and updated_bot_id is not 0 and updated_bot_id is not "":
            query = db.session.query(BotModel, ChannelModel).\
                    filter(BotModel.account_id == account_id).\
                    filter(BotModel.id == updated_bot_id).\
                    filter(BotModel.deleted == False).\
                    filter(ChannelModel.id == BotModel.channel_id).\
                    filter(ChannelModel.deleted == False).first()

            if query is None:
                return abort(400, "Bot ID not found.")
            bot = getattr(query, "BotModel")
            bot_name = getattr(query, "BotModel").name
            updated_bot_id = getattr(query, "BotModel").id
            page_id = getattr(query, "ChannelModel").page_id

        else:
            updated_bot_id = None
            bot_name = "No bot deployed"
            page_id = None

        # Default campaign template
        campaigns = [{
                        "label": "Abandoned cart",
                        "campaign_type": "TRIGGER",
                        "channel": "messenger",
                        "bot_id": updated_bot_id,
                        "persistent": True,
                        "content": {
                            "triggered_by": "WOOCOMMERCE",
                            "triggered_after": "LAST_CART",
                            "send_after": 1800,
                            "message": {
                                "label": "Abandoned cart",
                                "channel": "messenger",
                                "bot_id": updated_bot_id,
                                "campaign": True,
                                "saved_reply": [
                                    {
                                        "message_type": "FB_TEXT",
                                        "text": "Hello, there's something in your cart. Would you like to complete your purchase? Thank you for shopping!"
                                    }
                                ]
                            }
                        }
                    },
                    {
                        "label": "Order receipt",
                        "campaign_type": "TRIGGER",
                        "channel": "messenger",
                        "bot_id": updated_bot_id,
                        "persistent": True,
                        "content": {
                            "triggered_by": "WOOCOMMERCE",
                            "triggered_after": "LAST_USER_CHECKOUT",
                            "send_after": 0,
                            "message": {
                                "label": "User checkout",
                                "channel": "messenger",
                                "bot_id": updated_bot_id,
                                "campaign": True,
                                "saved_reply": [
                                    {
                                        "message_type": "FB_TEXT",
                                        "text": "Thank you for your purchase. We will process your order soon."
                                    }
                                ]
                            }
                        }
                    },
                    {
                        "label": "Order update",
                        "campaign_type": "TRIGGER",
                        "channel": "messenger",
                        "bot_id": updated_bot_id,
                        "persistent": True,
                        "content": {
                            "triggered_by": "WOOCOMMERCE",
                            "triggered_after": "LAST_ORDER_UPDATE",
                            "send_after": 0,
                            "message": {
                                "label": "Order update",
                                "channel": "messenger",
                                "bot_id": updated_bot_id,
                                "campaign": True,
                                "saved_reply": [
                                    {
                                        "message_type": "FB_TEXT",
                                        "text": "Hello there! Your order is being processed. Thank you for your patience."
                                    }
                                ]
                            }
                        }
                    },
                    {
                        "label": "Order completed",
                        "campaign_type": "TRIGGER",
                        "channel": "messenger",
                        "bot_id": updated_bot_id,
                        "persistent": True,
                        "content": {
                            "triggered_by": "WOOCOMMERCE",
                            "triggered_after": "LAST_ORDER_COMPLETED",
                            "send_after": 0,
                            "message": {
                                "label": "Order completed",
                                "channel": "messenger",
                                "bot_id": updated_bot_id,
                                "campaign": True,
                                "saved_reply": [
                                    {
                                        "message_type": "FB_TEXT",
                                        "text": "Your order is completed. We are now shipping your order."
                                    }
                                ]
                            }
                        }
                    },
                    {
                        "label": "Confirmation message",
                        "campaign_type": "TRIGGER",
                        "channel": "messenger",
                        "bot_id": updated_bot_id,
                        "persistent": True,
                        "content": {
                            "triggered_by": "WOOCOMMERCE",
                            "triggered_after": "CONFIRMATION_MSG",
                            "send_after": 0,
                            "message": {
                                "label": "Confirmation message",
                                "channel": "messenger",
                                "bot_id": updated_bot_id,
                                "campaign": True,
                                "saved_reply": [
                                    {
                                        "message_type": "FB_QUICK_REPLY",
                                        "text": "Hello there! We notice you've subscribed to our service. Please reply by pressing button below",
                                        "quick_replies": [
                                            {
                                                "title": "Ok, I agree",
                                                "payload": "?action=CONFIRM_BTN",
                                                "content_type": "text"
                                            }    
                                        ]
                                    }
                                ]
                            }
                        }
                    }]

        if update is True:
            # Template for default campaign woocommerce integration
            current_bot_id = integration.bot_id

            # When user edit bot_id on Integration, then update bot_id on campaign too
            if updated_bot_id is not None:
                # Only when bot_id is different
                if current_bot_id != updated_bot_id:
                    # Check all campaign with Woocommerce
                    campaigns = CampaignsModel.query.\
                                filter_by(bot_id=updated_bot_id).\
                                filter_by(persistent=True).\
                                filter_by(triggered_by="WOOCOMMERCE").\
                                filter_by(deleted=False).all()
                    campaign_list = [x for x in campaigns]

                    # Update to new bot_id
                    db.session.query(CampaignsModel).filter_by(bot_id=current_bot_id).\
                    filter_by(deleted=False).update({
                        CampaignsModel.bot_id: updated_bot_id
                    })

                    for item in campaign_list:
                        # Delete all campaign related to integration
                        item.deleted = True
                        item.save_to_db()

                    for campaign in campaigns:
                        create_campaign(campaign, account_id, updated_bot_id)

            whitelist_domain(url, bot.channel_id)
            integration.bot_id = updated_bot_id
            integration.bot_name = bot_name
            integration.url = url
            integration.date_updated = timestamp()
            integration.save_to_db()
        else:

            whitelist_domain(url, bot.channel_id)
            integration = IntegrationModel(
                type=_type,
                bot_id=updated_bot_id,
                bot_name=bot_name,
                account_id=account_id,
                url=url,
                page_id = page_id
            )

            # Use itsdangerous to generate unique string from channel_id webhook url
            s = URLSafeSerializer(current_app.config['SECRET_KEY'])
            unique_string = s.dumps(integration.id)
            webhook_url = '{0}/woocommerce/{1}'.format(WEBHOOK_URL, unique_string)
            integration.webhook_url = webhook_url
            integration.save_to_db()

            # Create new default campaign for Abandoned Cart only when bot available
            if updated_bot_id is not None:
                for campaign in campaigns:
                    create_campaign(campaign, account_id, updated_bot_id)


    elif _type == "SMS":
        try:
            key = data["key"]
            secret = data["secret"],
            templates = data["templates"]
        except:
            abort(400, "Invalid request.")

        account = AccountModel.query.filter_by(id=account_id).filter_by(deleted=False).first()
        package = AccountPackage.query.filter_by(id=account.package_id).first()
        payment = PaymentModel.query.filter_by(account_id=account_id).filter_by(is_active=True).first()
        if account is None:
            abort(400, "Account is not active.")

        subs = timestamp() - account.payment_start
        time_left = timedelta(days=package.day_limit).total_seconds()
        if subs >= time_left or payment is None:
            abort(400, "No active payment found.")

        if update is True:
            integration.key_id = key
            integration.secret = secret
            integration.date_updated = timestamp()
            integration.save_to_db()
        else:
            integration = IntegrationModel(
                type=_type,
                key_id = key,
                secret = secret,
                account_id=account_id
            )
            integration.save_to_db()

            for template in templates:
                nexmo = NexmoTemplate(
                    type = template['type'],
                    integration_id = integration.id,
                    template = template['text'],
                )
                nexmo.save_to_db()

    else:
        return abort(400, "Invalid integration type")

    resp = integration_schema.jsonify(integration)
    resp.status_code = 201

    return resp


def get_integration_detail(account_id, _id):
    """Get detail Integration schema."""
    obj = db.session.query(AccountModel, IntegrationModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(IntegrationModel.id==_id).\
          filter(IntegrationModel.deleted==False).first()
    try:
        integration = getattr(obj, "IntegrationModel")
    except AttributeError:
        return abort(404, "Integration not found.")

    resp = integration_schema.jsonify(integration)
    return resp


def delete_integration(account_id, _id):
    """Soft delete Integration."""
    obj = db.session.query(AccountModel, IntegrationModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(IntegrationModel.id==_id).\
          filter(IntegrationModel.deleted==False).first()
    try:
        integration = getattr(obj, "IntegrationModel")
    except AttributeError:
        return abort(404, "Integration not found.")

    # When user disconnect integration, delete campaign too
    campaigns = CampaignsModel.query.filter_by(bot_id=integration.bot_id).\
                filter_by(deleted=False).all()

    for campaign in campaigns:
        delete_campaign(account_id, integration.bot_id, campaign.id, force=True)

    integration.deleted = True
    integration.save_to_db()

    return None, 204

def edit_sms_template(account_id, template_id, data):
    try:
        text = data['template']
        _type = data['type']
    except:
        abort(400, 'Invalid request.')

    template = db.session.query(NexmoTemplate, IntegrationModel).\
               filter(NexmoTemplate.id == template_id).\
               filter(NexmoTemplate.deleted == False).\
               filter(NexmoTemplate.integration_id == IntegrationModel.id).\
               filter(IntegrationModel.deleted == False).\
               filter(IntegrationModel.account_id == account_id).\
               with_entities(NexmoTemplate).first()

    if template is None:
        abort(404, "Template not found")

    template.type = _type
    template.template = text
    template.save_to_db()

    return sms_template.jsonify(template)

def delete_sms_template(account_id, template_id):

    template = db.session.query(NexmoTemplate, IntegrationModel).\
               filter(NexmoTemplate.id == template_id).\
               filter(NexmoTemplate.deleted == False).\
               filter(NexmoTemplate.integration_id == IntegrationModel.id).\
               filter(IntegrationModel.deleted == False).\
               filter(IntegrationModel.account_id == account_id).\
               with_entities(NexmoTemplate).first()

    if template is None:
        abort(404, "Template not found")

    template.deleted = True
    template.save_to_db()

    return '',204

def whitelist_domain(url, channel_id):
    fb_graph = current_app.config['FB_GRAPH']
    channel = ChannelModel.query.filter_by(id=channel_id).first()
    if channel is None:
        abort(400, "Channel not found")

    page_access_token = channel.page_access_token

    whitelist_request = {
        "whitelisted_domains": [url]
    }
    whitelist_url = "{0}/messenger_profile?access_token={1}".format(fb_graph, page_access_token)
    rv = requests.post(whitelist_url, json=whitelist_request)
    if rv.status_code != 200:
        print(rv.json())
        return abort(400, "Url can't be whitelisted, make sure it use https.")

    return True
