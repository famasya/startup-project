# pylint: disable=R0903, C0103

"""Schema for Integration API endpoints"""
from marshmallow import post_dump
from app.extensions.schema import ma

from .models import IntegrationModel, NexmoTemplate

class IntegrationSchema(ma.ModelSchema):
    """Define Zendesk schema"""
    class Meta:
        """This will return the json format for each field in ZendeskModel."""
        model = IntegrationModel
        exclude = ['deleted']

    templates = ma.Nested('NexmoTemplateSchema', many=True)

    @post_dump
    def modify(self, data):
        if data["type"] == "SMOOCH":
            for e in ["url", "token", "email"]:
                data.pop(e)
        if data["type"] == "ZENDESK":
            for e in ["app_id", "secret", "key_id"]:
                data.pop(e)
        if data["type"] == "SMS":
            for e in ["email", "token", "url", "webhook_url"]:
                data.pop(e)

        data["_link"] = {
            "item_detail": "/v2/integrations/{0}/{1}".format(data["account"], data['id'])
        }
        data.pop("account")

integration_schema = IntegrationSchema()
integrations_schema = IntegrationSchema(many=True)

class NexmoTemplateSchema(ma.ModelSchema):
    """Define SMS Template schema"""
    class Meta:
        model = NexmoTemplate
        exclude = ['integrations']

sms_template = NexmoTemplateSchema()
sms_templates = NexmoTemplateSchema(many=True)
