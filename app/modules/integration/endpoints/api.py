# pylint: disable=R0201, C0103, W0611, C0111, C0301

"""List of API endpoint for Integration module."""
from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ....utilities.api_limiter import api_limiter

from ..ops import (
    create_integration, get_integration_detail, delete_integration,
    get_integration_list, edit_sms_template, delete_sms_template
)


api = Namespace('integrations', description='Integration related operations')

# JSON FORMAT
post_zendesk = api.model('Integration', {
    'url': fields.String,
    'email': fields.String,
    'password': fields.String
})

# CREATE THIRD PARTY INTEGRATIONS
@api.route('/<account_id>')
class NewIntegration(Resource):
    @api.expect(post_zendesk)
    @login_required
    def post(self, account_id):
        api_limiter(account_id, request.referrer)
        '''Create an integration'''
        data = request.json
        resp = create_integration(data, account_id)

        return resp

# UPDATE AND DELETE THIRD PARTY INTEGRATIONS
@api.route('/<account_id>/<integration_id>')
class IntegrationItem(Resource):
    @login_required
    def get(self, account_id, integration_id):
        api_limiter(account_id, request.referrer)
        '''Get a zendesk integration'''
        return get_integration_detail(account_id, integration_id)


    @api.expect(post_zendesk)
    @login_required
    def put(self, account_id, integration_id):
        api_limiter(account_id, request.referrer)
        """Update zendesk integration."""
        data = request.json
        resp = create_integration(data, account_id, integration_id, update=True)

        return resp


    @login_required
    def delete(self, account_id, integration_id):
        api_limiter(account_id, request.referrer)
        '''Delete a zendesk integration'''
        resp = delete_integration(account_id, integration_id)
        return resp


# GET ALL INTEGRATIONS
@api.route('/<account_id>')
class ListIntegrations(Resource):
    @login_required
    def get(self, account_id):
        api_limiter(account_id, request.referrer)
        '''Get list of all integrations'''
        resp = get_integration_list(account_id)

        return resp

@api.route('/<account_id>/template/<template_id>')
class TemplatesOperation(Resource):
    @login_required
    def put(self, account_id, template_id):
        api_limiter(account_id, request.referrer)

        data = request.json
        resp = edit_sms_template(account_id, template_id, data)

        return resp

    @login_required
    def delete(self, account_id, template_id):
        api_limiter(account_id, request.referrer)

        resp = delete_sms_template(account_id, template_id)
        return resp
