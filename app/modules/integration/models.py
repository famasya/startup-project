# pylint: disable=E1101, C0103, W0611, W0622, R0903

"""This model using SQLAlchemy as ORM for Integration API endpoint."""
import datetime
from flask import abort
from sqlalchemy import exc
from sqlalchemy.orm.exc import NoResultFound

from app.extensions.db import db
from app.utilities.time import timestamp
from ...utilities.unique_url import universal_uuid_url64


class IntegrationModel(db.Model):
    """Define ZendeskModel."""
    __tablename__ = 'integrations'

    account_id = db.Column(db.String, db.ForeignKey('accounts.id'))

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String)

    # Zendesk
    url = db.Column(db.String)
    email = db.Column(db.String)
    token = db.Column(db.String)

    # Smooch
    secret = db.Column(db.String)
    key_id = db.Column(db.String)
    app_id = db.Column(db.String)
    webhook_url = db.Column(db.String)

    page_id = db.Column(db.String(30))

    # Woocommerce
    bot_id = db.Column(db.Integer, db.ForeignKey('bots.id'))
    bot_name = db.Column(db.String)

    deleted = db.Column(db.Boolean, default=False)
    date_created = db.Column(db.Integer, default=timestamp)
    date_updated = db.Column(db.Integer, default=timestamp)

    templates = db.relationship("NexmoTemplate", backref="integrations", lazy="dynamic")

    def save_to_db(self):
        """Save operation."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

class NexmoTemplate(db.Model):
    """Nexmo template"""
    __tablename__ = "sms_templates"

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String)
    integration_id = db.Column(db.Integer, db.ForeignKey("integrations.id"))
    template = db.Column(db.String)
    deleted = db.Column(db.Boolean, default=False)

    def save_to_db(self):
        """Save operation."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise
