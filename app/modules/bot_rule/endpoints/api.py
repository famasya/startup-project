# pylint: disable=R0201, C0103

"""List of API endpoint for Bot Rules."""
from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ....utilities.api_limiter import api_limiter

from ..ops import (
    create_rule, get_rule_list, post_webhook,
    update_rule, delete_rule, get_rule_item
)

api = Namespace('bot_rules', description='Bot Rules related operations')


post_bot = api.model('Bot', {
    'entity_name': fields.String,
    'value': fields.String,
})
rule_bot = api.inherit('Bot_rule', post_bot, {
    "rule_type": fields.String,
})

@api.route('/<account_id>/<int:bot_id>')
class BotRule(Resource):
    @api.doc('get_list_rules')
    @login_required
    def get(self, account_id, bot_id):
        api_limiter(account_id, request.referrer)
        """Return list of rules."""
        return get_rule_list(account_id, bot_id)


@api.route('/<account_id>/<int:bot_id>/<int:rule_id>')
class BotRuleItem(Resource):
    @api.doc('get_rule_detail')
    @login_required
    def get(self, account_id, bot_id, rule_id):
        api_limiter(account_id, request.referrer)
        """Return list of rules."""
        return get_rule_item(account_id, bot_id, rule_id)

    @login_required
    def delete(self, account_id, bot_id, rule_id):
        api_limiter(account_id, request.referrer)
        """Delete a rule."""
        # fetch data from json post
        resp = delete_rule(account_id, bot_id, rule_id)

        return resp


@api.route('/<account_id>/webhook_test')
class WebhookTest(Resource):
    @login_required
    def post(self, account_id):
        api_limiter(account_id, request.referrer)
        """Validate webhook url."""
        data = request.json
        resp = post_webhook(account_id, data)

        return resp
