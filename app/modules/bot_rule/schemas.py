# pylint: disable=E1101, C0103, W0611, W0622, R0903

"""Schema for Bot Rules API endpoints"""
from flask import abort
from marshmallow import post_dump
from .models import RuleModel, EmailNotif
from ..saved_reply.schemas import SavedReplySchema

from ...extensions.schema import ma


class EmailNotifSchema(ma.ModelSchema):
    """Define schema for email notif."""
    class Meta:
        model = EmailNotif
        exclude = ["bot_rules", "deleted"]


class RuleModelSchema(ma.ModelSchema):
    """Define schema for rule models."""
    class Meta:
        model = RuleModel
        exclude = ["bot", "deleted"]

    replies = ma.Nested('SavedReplySchema', many=True, exclude=("bot_rules",))
    emails = ma.Nested('EmailNotifSchema', many=True)

    @post_dump
    def modify(self, data):
        """Remove null key"""
        _type = data.get("rule_type")
        if _type is None:
            _type = data.get("message_type")

        if _type == "SEND_MESSAGE":
            data.pop("assign_username", None)
            data.pop("emails", None)
            data.pop("webhook_url", None)
            data.pop("sequence_id", None)

        if _type == "SEND_WEBHOOK":
            data.pop("assign_username", None)
            data.pop("emails", None)
            data.pop("replies", None)
            data.pop("sequence_id", None)

        if _type == "ASSIGN_HUMAN":
            data.pop("webhook_url", None)
            data.pop("emails", None)
            data.pop("replies", None)
            data.pop("sequence_id", None)

        if _type == "SEND_SEQUENCE":
            data.pop("webhook_url", None)
            data.pop("emails", None)
            data.pop("replies", None)
            data.pop("assign_username", None)

        if _type == "SEND_EMAIL":
            data.pop("webhook_url", None)
            data.pop("assign_username", None)
            data.pop("replies", None)
            data.pop("sequence_id", None)

            emails = []
            for item in data["emails"]:
                emails.append(item["email"])

            data["emails"] = emails

rule_schema = RuleModelSchema()
rules_schema = RuleModelSchema(many=True)
