# pylint: disable=E1101, C0103, W0611, W0622, R0903

"""
BOT RULES
~~~~~~~~~~~~
This rules will be used to match between entity/value and
bot actions.
"""
import datetime
from flask import abort
from ..saved_reply.models import SavedReplyModel

from ...extensions.db import db
from ...utilities.time import timestamp


class EmailNotif(db.Model):
    """Define email list for notification."""
    __tablename__ = "bot_rule_email"

    rule_id = db.Column(db.Integer, db.ForeignKey('bot_rules.id'))

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String)

    deleted = db.Column(db.Boolean, default=False)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


rel_actions = db.Table("rel_actions",
    db.Column("id", db.Integer, primary_key=True),
    db.Column("rule_id", db.Integer, db.ForeignKey("bot_rules.id")),
    db.Column("reply_id", db.Integer, db.ForeignKey("saved_replies.id"))
)
class RuleModel(db.Model):
    """Define rule model"""
    __tablename__ = "bot_rules"

    bot_id = db.Column(db.Integer, db.ForeignKey('bots.id'))

    id = db.Column(db.Integer, primary_key=True)
    rule_type = db.Column(db.String)

    # Trigger
    entity_name = db.Column(db.String)
    value = db.Column(db.String)

    # rule_type: ASSIGN_HUMAN
    assign_username = db.Column(db.String)

    # rule_type: SEND_WEBHOOK
    webhook_url = db.Column(db.String)

    # rule_type: SEQUENCE
    sequence_id = db.Column(db.String)

    # rule_type: SEND_MESSAGE
    replies = db.relationship('SavedReplyModel', secondary=rel_actions, backref=db.backref('bot_rules', lazy='dynamic'))

    # rule_type: SEND_EMAIL
    emails = db.relationship("EmailNotif", backref="bot_rule", lazy="dynamic")

    is_active = db.Column(db.Boolean, default=True)
    deleted = db.Column(db.Boolean, default=False)
    date_created = db.Column(db.Integer, default=timestamp())
    date_updated = db.Column(db.Integer, default=timestamp())

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise
