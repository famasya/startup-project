# pylint: disable=E1101, C0326

"""This is used to supply logic in the routes."""
from urllib.parse import urlparse
import requests
from flask import current_app, jsonify
from ...utilities.abort import abort

from .models import RuleModel, EmailNotif
from .schemas import rule_schema, rules_schema
from ..account.models import AccountModel
from ..user.models import UserModel
from ..sequence.models import SequenceModel
from ..bot.models import BotModel, Entity, Value, WitApp
from ..saved_reply.models import SavedReplyModel
from app.utilities.validator import is_valid_email
from operator import itemgetter

from app.extensions.db import db

def check_if_exists(account_id, entity_name, value, data):
    rule_type = data["rule_type"]
    rule = RuleModel.query.filter_by(entity_name=entity_name).\
           filter_by(value=value).filter_by(rule_type=rule_type).\
           filter_by(deleted=False).first()
    if rule is not None:
        abort(400, "Rule exists")
    return True

def create_rule(account_id, bot_id, data, update=False):
    """Create new rule."""
    try:
        rules = data["rules"]
        entity_name = data["entity_name"]
        value_name = data["value"]
    except (KeyError, TypeError, AttributeError):
        return abort(400, "Invalid request.")

    for rule in rules:
        try:
            rule_type = rule["rule_type"]
        except:
            return abort(400, "Rule type and rule id is required")

        # Check format
        if rule_type == "SEND_MESSAGE":
            try:
                replies = rule["replies"]  # list
            except (KeyError, TypeError):
                return abort(400, "replies is required.")
        else:
            abort(400, "Rule type must be a reply")

    # Check bot
    obj = db.session.query(AccountModel, BotModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(BotModel.account_id==AccountModel.id).\
          filter(BotModel.id==bot_id).\
          filter(BotModel.deleted==False).first()
    try:
        bot = getattr(obj, "BotModel")
    except AttributeError:
        return abort(404, "Bot not found.")

    # Check entity_name and value
    obj_value = db.session.query(Entity, Value).\
             filter(Entity.wit_id==bot.wit_id).\
             filter(Entity.deleted==False).\
             filter(Entity.entity_name==entity_name).\
             filter(Value.entity_id==Entity.id).\
             filter(Value.deleted==False).\
             filter(Value.value==value_name).first()
    try:
        entity = getattr(obj_value, "Entity")
        value = getattr(obj_value, "Value")
    except AttributeError:
        return abort(404, "Entity or value not found.")


    if update:
        new_rule_list = list(map(lambda i: i.get('id'), rules))
        # Check current rules
        current_rules = RuleModel.query.filter_by(entity_name=entity_name).filter_by(value=value_name).filter_by(deleted=False).all()

        for rule in current_rules:
            if rule.id not in new_rule_list:
                delete_rule(account_id, bot_id, rule.id)

    rule_list = []
    for rule_item in rules:
        create_new = False
        rule_type = rule_item["rule_type"]
        if rule_type == "SEND_MESSAGE":
            replies = rule_item["replies"]

            if update is False or "id" not in rule_item:
                # When "id" is not provided as key, assume as create new rule in current entity. It is not 
                # categorized as "update" because no id is attached. Thus, we have put it into new and update
                # state. The logic is: create_new is variable if rule exists. If it exists, abort it. Otherwise
                # create new rule, BUT append it to the update's return value
                create_new = True
                rule = RuleModel(
                    bot_id=bot_id,
                    rule_type=rule_type,
                    entity_name=entity_name,
                    value=value_name
                )
                rule.save_to_db()

            else:
                is_active = rule_item.get("is_active")
                if is_active is None:
                    is_active = True
                rule_id = rule_item["id"]

                rule = RuleModel.query.\
                       filter_by(bot_id=bot_id).\
                       filter_by(id=rule_id).\
                       filter_by(deleted=False).first()
                if rule is None:
                    return abort(404, "Rule not found.")

                rule.rule_type = rule_type
                rule.entity_name = entity_name
                rule.value = value_name
                rule.is_active = is_active
                rule.replies = []
                rule.save_to_db()

            # When this rule triggered, it will randomly send the saved replies
            for reply_id in replies:
                reply = SavedReplyModel.query.\
                        filter_by(bot_id=bot_id).\
                        filter_by(deleted=False).\
                        filter_by(id=reply_id).first()
                if reply is None:
                    return abort(404, "Reply not found.")

                # Check saved replies must match with bot channel
                if reply.channel != bot.channel:
                    return abort(400, "Reply format didn't match with bot channel.")

                rule.replies.append(reply)
                rule.save_to_db()

            if update or create_new:
                if create_new:
                    is_active = True 

                rule_list.append({
                    "id": rule.id,
                    "rule_type": rule_type,
                    "is_active": is_active,
                    "replies": rule.replies,
                })

        else:
            return abort(400, "Invalid rule_type. Only SEND_MESSAGE accepted")

    data, error = rules_schema.dump(rule_list)
    resp = {
        "entity_name": entity.entity_name,
        "value": value.value,
        "rules": data
    }
    return resp, 200


def get_rule_list(account_id, bot_id):
    """Return list of rules for specific bot_id"""
    objs = db.session.query(AccountModel, BotModel, RuleModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(BotModel.account_id==AccountModel.id).\
          filter(BotModel.deleted==False).\
          filter(BotModel.id==bot_id).\
          filter(RuleModel.bot_id==BotModel.id).\
          filter(RuleModel.deleted==False).all()

    # List of entities
    entities = db.session.query(AccountModel).\
            join(BotModel, BotModel.account_id==AccountModel.id).\
            join(WitApp, WitApp.id==BotModel.wit_id).\
            join(Entity, Entity.wit_id==WitApp.id).\
            join(Value, Value.entity_id==Entity.id).\
            filter(Entity.deleted==False).\
            filter(AccountModel.id==account_id).\
            filter(AccountModel.deleted==False).\
            filter(BotModel.id==bot_id).\
            filter(BotModel.deleted==False).\
            with_entities(Entity.entity_name, Value.value, Value.id).all()

    rules = []
    index = -1
    latest_id = 0
    for obj in objs:
        try:
            rule = getattr(obj, "RuleModel")
            if rule.entity_name in map(itemgetter('entity_name'), rules) and rule.value in map(itemgetter('value'), rules):
                # If the key is not changed, assume it as same with previous
                data, error = rule_schema.dump(rule)
                rules[index]["rules"].append(data)
            else:
                # If key is different with previous, create new list
                data, error = rule_schema.dump(rule)
                rules.append({
                    "id": rule.id,
                    "entity_name":rule.entity_name,
                    "value":rule.value,
                    "rules":[data]
                    })
                index += 1
                latest_id = rule.id
        except AttributeError:
            pass

    # Check if there's any entity_name and value pair is not in rules
    for entity_name, value, id in entities:
        if entity_name not in map(itemgetter('entity_name'), rules) or value not in map(itemgetter('value'), rules):
            latest_id += 1
            rules.append({
                "id": latest_id,
                "entity_name":entity_name,
                "value":value,
                "rules":[]
            })

    return jsonify(rules)


def get_rule_item(account_id, bot_id, rule_id):
    """Return rule item"""
    obj = db.session.query(AccountModel, BotModel, RuleModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(BotModel.id==bot_id).\
          filter(BotModel.deleted==False).\
          filter(RuleModel.id==rule_id).\
          filter(RuleModel.deleted==False).first()
    try:
        rule = getattr(obj, "RuleModel")
    except AttributeError:
        return abort(400, "Rule not found.")

    resp = rule_schema.jsonify(rule)

    return resp


def post_webhook(account_id, data):
    """User can test the webhook request by using this endpoint.
    The user's endpoint should return status code 200
    """
    try:
        webhook_url = data["webhook_url"]
    except (KeyError, TypeError):
        return abort(400, "Invalid request.")
    account = AccountModel.query.filter_by(id=account_id).filter_by(deleted=False).first()
    if account is None:
        return abort(404, "Account not found.")

    # Validate webhook url, it must be https
    url = urlparse(webhook_url)
    if url.scheme != 'https':
        return abort(400, "Invalid webhook url, it must be https.")

    data = {
        "trigger": "bot",
        "entity": "test",
        "value": "test",
        "text": "this is only the test",
        "channel": {
            "channel": "messenger",
            "channel_name": "test page",
            "channel_id": "test1234"
        },
        "customer": "customer fields"
    }
    headers = {"account_id": "account_id"}
    req = requests.post(webhook_url, json=data, headers=headers)

    if req.status_code != 200:
        return abort(400, "Endpoint doesn't return 200 status code")

    return jsonify({"message": "OK"})


def delete_rule(account_id, bot_id, rule_id):
    """Delete rule."""
    obj = db.session.query(AccountModel, BotModel, RuleModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(BotModel.id==bot_id).\
          filter(BotModel.deleted==False).\
          filter(RuleModel.id==rule_id).\
          filter(RuleModel.deleted==False).first()
    try:
        rule = getattr(obj, "RuleModel")
    except AttributeError:
        return abort(400, "Rule not found.")

    rule.deleted = True
    rule.save_to_db()

    return None, 204


def update_rule(account_id, bot_id, data, rule_id):
    """Update rule."""
    resp = create_rule(account_id, bot_id, data, rule_id, update=True)

    return resp
