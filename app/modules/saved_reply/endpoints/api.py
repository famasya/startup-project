# pylint: disable=R0201, C0103, W0611, C0111, C0301

"""List of API endpoint for SavedReply module."""
from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ....utilities.api_limiter import api_limiter

from ..ops import create_saved_reply, get_saved_reply_list, get_saved_reply_detail, \
                  update_saved_reply, delete_saved_reply, search_saved_reply

api = Namespace('saved_replies', description='SavedReply related operations')

# JSON FORMAT
post_saved_reply = api.model('SavedReply', {
    'label': fields.String,
    'saved_reply': fields.String
})


@api.route('/<account_id>/<bot_id>/search')
class SearchSavedReplies(Resource):
    @login_required
    def get(self, account_id, bot_id):
        api_limiter(account_id, request.referrer)
        '''Search saved reply'''
        text = request.args.get("text")
        start = request.args.get("start")
        limit = request.args.get("limit")
        channel = request.args.get("channel")
        return search_saved_reply(account_id, bot_id, text, start, limit, channel)


# API METHODS
@api.route('/<account_id>')
class NewSavedReply(Resource):
    @api.doc('list_saved_reply')
    @login_required
    def get(self, account_id):
        api_limiter(account_id, request.referrer)
        """List all saved_replys per account_id."""
        start = request.args.get("start")
        limit = request.args.get("limit")
        bot_id = request.args.get("bot_id")
        resp = get_saved_reply_list(account_id, start, limit, bot_id)
        return resp


    @api.doc('create_saved_reply')
    @api.expect(post_saved_reply)
    @login_required
    def post(self, account_id):
        api_limiter(account_id, request.referrer)
        """Create a saved_reply.
        Messenger text format:

        ```
        {
            "label": "welcome",
            "saved_reply": [{
                "message_type": "fb_text",
                "text": "hello there.."
            }]
        }
        ```

        Messenger attachment format:

        ```
        {
            "label": "welcome",
            "saved_reply": [{
                "message_type": "fb_attachment",
                "attachment_url": "example.com/hello.png"
            }]
        }
        ```

        Messenger quick reply format:

        ```
        {
            "label": "welcome",
            "saved_reply": [{
                "message_type": "fb_quick_reply",
                "text": "Hello there.",
                "quick_replies": ["human", "bot"]
            }]
        }
        ```

        Messenger button template format:

        ```
        {
            "label": "welcome",
            "saved_reply": [{
                "message_type": "fb_button_template",
                "text": "Hello there.",
                "buttons": [
                    {
                        "button_type": "web_url",
                        "button_url": "example.com/url",
                        "button_title": "View website"
                    },
                    {
                        "button_type": "postback",
                        "button_url": "example.com/url",
                        "payload": "DEVELOPER_DEFINED_PAYLOAD"
                    }
                ]
            }]
        }
        ```

        Messenger generic template format:

        ```
        {
            "label": "welcome",
            "saved_reply": [{
                "message_type": "fb_generic_template",
                "template_title": "Hello there.",
                "subtitle": "this is subtitle",
                "template_url": "example.com/avatar.png",
                "image_url": "example.com/image.png",
                "buttons": [
                    {
                        "button_type": "web_url",
                        "button_url": "example.com/url",
                        "button_title": "View website"
                    },
                    {
                        "button_type": "postback",
                        "button_url": "example.com/url",
                        "payload": "DEVELOPER_DEFINED_PAYLOAD"
                    }
                ]
            }]
        }
        ```

        LINE text format:

        ```
        {
            "label": "welcome",
            "saved_reply": [{
                "message_type": "line_text",
                "text": "Hello there."
            }]
        }
        ```

        LINE attachment format:

        ```
        {
            "label": "welcome",
            "saved_reply": [{
                "message_type": "line_attachment",
                "attachment_url": "Hello there."
            }]
        }
        ```
        """
        # fetch data from json post
        data = request.json
        quick = request.args.get("quick")
        resp = create_saved_reply(data, account_id, quick)

        return resp


# GET A MESSAGE
@api.route('/<account_id>/<bot_id>/<saved_reply_id>')
class SavedReplyItem(Resource):
    @login_required
    def get(self, account_id, bot_id, saved_reply_id):
        api_limiter(account_id, request.referrer)
        """Get a saved_reply."""
        resp = get_saved_reply_detail(account_id, bot_id, saved_reply_id)

        return resp


    @api.expect(post_saved_reply)
    @login_required
    def put(self, account_id, bot_id, saved_reply_id):
        api_limiter(account_id, request.referrer)
        """Update a saved_reply."""
        data = request.json
        resp = update_saved_reply(data, account_id, bot_id, saved_reply_id)

        return resp


    @login_required
    def delete(self, account_id, bot_id, saved_reply_id):
        api_limiter(account_id, request.referrer)
        """Delete a saved_reply."""
        return delete_saved_reply(account_id, bot_id, saved_reply_id)
