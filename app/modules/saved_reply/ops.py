# pylint: disable=W0622, C0103, E1101

"""Operations for SavedReply API."""
from flask import jsonify
from ...utilities.abort import abort
from urllib.parse import urlparse

from ...extensions.db import db

from .models import (
    SavedReplyModel, SavedQuickReply,
    SavedTemplate, SavedButton,
    SavedText, SavedAttachment,
    SavedQuickReplyButton, SavedButtonTemplate,
    SavedTemplateGallery, SavedEmails
)
from .schemas import (
    saved_reply_schema, saved_replies_schema, replies_paginated,
    page_schema
)
from ..account.models import AccountModel
from ..bot.models import BotModel
from ..sequence.models import SequenceModel
from ..sequence.schemas import sequence_schema
from ...utilities.time import timestamp
from operator import itemgetter
import pprint

def save_text(reply_id, message_type, text, order=None):
    text = SavedText(
        reply_id=reply_id,
        message_type=message_type,
        order=order,
        text=text
    )
    text.save_to_db()

def save_emails(reply_id, message_type, emails, order=None):
    emails = SavedEmails(
        reply_id=reply_id,
        message_type=message_type,
        order=order,
        emails=emails
    )
    emails.save_to_db()

def save_webhook(reply_id, message_type, webhook_url, order=None):
    webhook = SavedEmails(
        reply_id=reply_id,
        message_type=message_type,
        webhook_url=webhook_url,
        order=order
    )
    webhook.save_to_db()

def save_sequence(reply_id, message_type, sequence, order=None):
    sequence = SavedEmails(
        reply_id=reply_id,
        message_type=message_type,
        sequence=sequence,
        order=order
    )
    sequence.save_to_db()

def save_attachment(reply_id, message_type, attachment_url, order=None):
    text = SavedAttachment(
        reply_id=reply_id,
        message_type=message_type,
        order=order,
        attachment_url=attachment_url
    )
    text.save_to_db()


def save_quick_reply_button(quick_id, title, payload, order=None):
    quick_button = SavedQuickReplyButton(
        quick_reply_id=quick_id,
        title=title,
        order=order,
        payload=payload
    )
    quick_button.save_to_db()


def save_quick_reply(reply_id, message_type, text, order=None):
    quick_reply = SavedQuickReply(
        reply_id=reply_id,
        message_type=message_type,
        order=order,
        text=text
    )
    quick_reply.save_to_db()

    return quick_reply

def save_button(parent, buttons, order=None):
    for index, button in enumerate(buttons):
        button_type = button["type"]
        if button_type == "web_url":
            button_url = button["url"]
            button_title = button["title"]
            button = SavedButton(
                button_type=button_type,
                button_url=button_url,
                button_title=button_title
            )
            parent.buttons.append(button)
            parent.save_to_db()
        elif button_type == "postback" or button_type == "phone_number":
            button_title = button["title"]
            payload = button["payload"]
            button = SavedButton(
                button_type=button_type,
                button_title=button_title,
                payload=payload
            )
            parent.buttons.append(button)
            parent.save_to_db()
        elif button_type == "element_share":
            button = SavedButton(
                button_type=button_type,
                order=index
            )
            parent.buttons.append(button)
            parent.save_to_db()
        elif button_type == "payment":
            pass
        else:
            return abort(400, "Invalid button type.")


def check_format(data, modify=None):
    """It is used to check the format before we save it to db."""
    if modify == None:
        try:
            channel = data["channel"]
            label = data["label"]
            saved_reply = data["saved_reply"]
            bot_id = data["bot_id"]
        except (KeyError, TypeError):
            return abort(400, "Invalid request.")

        if not channel in ("messenger", "line", "telegram"):
            return abort(400, "Invalid channel type")
    else:
        try:
            label = data["label"]
            saved_reply = data["saved_reply"]
        except (KeyError, TypeError):
            return abort(400, "Invalid request.")

    for item in saved_reply:
        try:
            message_type = item["message_type"].upper()
        except (KeyError, TypeError):
            return abort(400, "message_type is required.")

        if message_type in ("FB_TEXT", "TELEGRAM_TEXT", "LINE_TEXT"):
            try:
                text = item["text"]
            except (KeyError, TypeError):
                return abort(400, "text is missing.")

            if len(text) > 640:
                return abort(400, "text limit is 640 chars, your text exceed the limit.")

        elif message_type in ("FB_ATTACHMENT", "LINE_ATTACHMENT", "TELEGRAM_ATTACHMENT"):
            try:
                attachment_url = item["attachment_url"]
            except (KeyError, TypeError):
                return abort(400, "attachment_url is missing.")

        elif message_type == "FB_QUICK_REPLY":
            try:
                text = item["text"]
                quick_replies = item["quick_replies"]  # list
            except (KeyError, TypeError):
                return abort(400, "Invalid request.")

            if len(text) > 640:
                return abort(400, "Text limit is 640 chars, your text exceeds the limit.")

            if len(quick_replies) > 3:
                return abort(400, "Quick Reply button limit is 3 buttons, your buttons exceed the limit.")

            for quick_reply in quick_replies:
                try:
                    title = quick_reply["title"]
                    payload = quick_reply["payload"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid request.")

        elif message_type == "FB_BUTTON_TEMPLATE":
            try:
                text = item["text"]
                buttons = item["buttons"]  # list
            except (KeyError, TypeError):
                return abort(400, "Invalid templates requests.")

            if len(text) > 640:
                return abort(400, "Text limit is 640 chars, your text exceed the limit.")

            if len(buttons) > 3:
                return abort(400, "Button limit is 3 buttons, your buttons exceed the limit.")

            for button in buttons:
                try:
                    button_type = button["type"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid button type requests.")
                if button_type == "web_url":
                    try:
                        button_url = button["url"]
                        button_title = button["title"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button requests.")

                    if len(button_title) > 20:
                        return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

                    # Validate webhook url, it must be https
                    url = urlparse(button_url)
                    if url.scheme != 'https':
                        return abort(400, "Invalid webhook url, it must be https.")

                elif button_type == "postback" or button_type == "phone_number":
                    try:
                        button_title = button["title"]
                        payload = button["payload"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button requests.")

                    if len(button_title) > 20:
                        return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

                elif button_type == "element_share":
                    pass

        elif message_type == "FB_GENERIC_TEMPLATE":
            try:
                templates = item["message_templates"]  # list
            except (KeyError, TypeError):
                return abort(400, "Invalid templates request.")

            if len(templates) > 10:
                return abort(400, "Horizontal templates limit is 10 element, your templates exceed the limit.")

            for template in templates:
                try:
                    template_title = template["template_title"]
                    image_url = template["image_url"]
                    subtitle = template["subtitle"]
                    template_url = template["template_url"]
                    buttons = template["buttons"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid templates request.")

                if len(template_title) > 80:
                    return abort(400, "Title limit is 80 chars, your title exceeds the limit.")

                if len(subtitle) > 80:
                    return abort(400, "Subtitle limit is 80 chars, your title exceeds the limit.")

                if len(buttons) > 3:
                    return abort(400, "Button limit is 3 buttons, your buttons exceed the limit.")

                for button in buttons:
                    try:
                        button_type = button["type"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button type requests.")
                    if button_type == "web_url":
                        try:
                            button_url = button["url"]
                            button_title = button["title"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                        if len(button_title) > 20:
                            return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

                    elif button_type == "postback" or button_type == "phone_number":
                        try:
                            button_title = button["title"]
                            payload = button["payload"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                        if len(button_title) > 20:
                            return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

                    elif button_type == "element_share":
                        pass

        elif message_type == "LINE_TEMPLATE":
            try:
                templates = item["message_templates"]  # list
            except (KeyError, TypeError):
                return abort(400, "Invalid templates requests.")

            if len(templates) > 5:
                return abort(400, "Horizontal templates limit is 5 element, your templates exceed the limit.")

            for template in templates:
                try:
                    template_title = template["template_title"]
                    thumbnailImageUrl = template["image_url"]
                    text = template["subtitle"]
                    actions = template["buttons"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid templates request.")

                if len(template_title) > 80:
                    return abort(400, "Title limit is 80 chars, your title exceeds the limit.")

                if len(text) > 80:
                    return abort(400, "Subtitle limit is 80 chars, your title exceeds the limit.")

                if len(actions) > 3:
                    return abort(400, "Button limit is 3 buttons, your buttons exceed the limit.")


                for action in actions:
                    try:
                        button_type = action["type"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button type requests.")
                    if button_type == "web_url":
                        try:
                            uri = action["url"]
                            label = action["title"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                        if len(label) > 20:
                            return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

                    elif button_type == "postback":
                        try:
                            label = action["title"]
                            data = action["payload"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                        if len(label) > 20:
                            return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

                    else:
                        return abort(400, "Invalid button type.")

        elif message_type == "LINE_IMAGE_CAROUSEL":
            try:
                templates = item["message_templates"]  # list
            except (KeyError, TypeError):
                return abort(400, "Invalid templates requests.")

            if len(templates) > 5:
                return abort(400, "Horizontal templates limit is 5 element, your templates exceed the limit.")

            for template in templates:
                try:
                    ImageUrl = template["image_url"]
                    actions = template["buttons"]
                except (KeyError, TypeError):
                    return abort(400, "Invalid templates request.")

                if len(actions) > 1:
                    return abort(400, "Multiple buttons are not allowed for this type of message.")

                for action in actions:
                    try:
                        button_type = action["type"]
                    except (KeyError, TypeError):
                        return abort(400, "Invalid button type requests.")
                    if button_type == "web_url":
                        try:
                            uri = action["url"]
                            label = action["title"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                        if len(label) > 20:
                            return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

                    elif button_type == "postback":
                        try:
                            label = action["title"]
                            data = action["payload"]
                        except (KeyError, TypeError):
                            return abort(400, "Invalid button requests.")

                        if len(label) > 20:
                            return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

                    else:
                        return abort(400, "Invalid button type.")

        elif message_type == "SEND_EMAIL":
            try:
                emails = item["emails"]
            except (KeyError, TypeError):
                return abort(400, "emails is required")

            if not isinstance(emails, list):
                abort(400, "emails must be list")

        elif message_type == "SEND_WEBHOOK":
            try:
                webhook_url = item["webhook_url"]
            except (KeyError, TypeError):
                return abort(400, "webhook_url is required")

            if not isinstance(emails, str):
                abort(400, "webhook_url must be list")

        elif message_type == "SEND_SEQUENCE":
            try:
                sequence = item["sequence"]
            except (KeyError, TypeError):
                return abort(400, "sequence is required")

            if not isinstance(emails, str):
                abort(400, "sequence must be list")

        else:
            return abort(400, "Invalid message_type")


def create_saved_reply(data, account_id, quick=None):
    """Create new saved_reply."""
    # Check format before saved to db
    if quick is None or quick is False:
        check_format(data)
        saved_reply = data["saved_reply"]  # list

        # Check persistence status
        if data.get("persistent") is None or data.get("persistent") is False:
            persistent = False
        else:
            persistent = True

        if data.get("ref") is None:
            ref = 'NO_REF'
        else:
            ref = data.get('ref')
    else:
        persistent = False
        ref = 'NO_REF'

    # Check campaign status
    if data.get("campaign") is None or data.get("campaign") is False:
        campaign = False
    else:
        campaign = True

    label = data["label"]
    if label is "":
        label = "New saved reply"
    channel = data["channel"]
    bot_id = data["bot_id"]

    # Check account
    account = AccountModel.query.filter_by(id=account_id).first()
    if account is None or account.deleted:
        return abort(404, 'Account not found')

    # Save reply first
    reply = SavedReplyModel(
        bot_id=bot_id,
        label=label,
        channel=channel,
        updated=timestamp(),
        persistent=persistent,
        ref=ref
    )
    if campaign is True:
        reply.campaign = True

    reply.save_to_db()

    reply_id = reply.id

    if quick is None or quick is False:
        for index, item in enumerate(saved_reply, 1):
            message_type = item["message_type"].upper()

            # Check structure each object and save it
            if message_type in ("FB_TEXT", "TELEGRAM_TEXT", "LINE_TEXT"):
                text = item["text"]
                save_text(reply_id, message_type, text, index)

            elif message_type in ("FB_ATTACHMENT", "LINE_ATTACHMENT", "TELEGRAM_ATTACHMENT"):
                attachment_url = item["attachment_url"]
                save_attachment(reply_id, message_type, attachment_url, index)

            elif message_type == "FB_QUICK_REPLY":
                text = item["text"]
                quick_replies = item["quick_replies"]  # list
                quick = save_quick_reply(reply_id, message_type, text, index)
                quick_id = quick.id

                for quick_reply in quick_replies:
                    title = quick_reply["title"]
                    payload = quick_reply["payload"]
                    save_quick_reply_button(quick_id, title, payload)

            elif message_type == "FB_BUTTON_TEMPLATE":
                text = item["text"]
                buttons = item["buttons"]  # list

                # Save the parent first
                button_template = SavedButtonTemplate(
                    reply_id=reply_id,
                    message_type=message_type,
                    text=text,
                    order=index
                )
                button_template.save_to_db()

                # Save the childs
                save_button(button_template, buttons)

            elif message_type == "FB_GENERIC_TEMPLATE":
                templates = item["message_templates"]  # list

                # Save parent first
                save_template = SavedTemplate(
                    reply_id=reply.id,
                    message_type=message_type,
                    order=index
                )
                save_template.save_to_db()

                for template in templates:
                    template_title = template["template_title"]
                    image_url = template["image_url"]
                    subtitle = template["subtitle"]
                    template_url = template["template_url"]
                    buttons = template["buttons"]

                    # Save child
                    gallery = SavedTemplateGallery(
                        template_id=save_template.id,
                        template_title=template_title,
                        image_url=image_url,
                        subtitle=subtitle,
                        template_url=template_url
                    )
                    gallery.save_to_db()

                    save_button(gallery, buttons)

            elif message_type == "LINE_TEMPLATE":
                templates = item["message_templates"]

                # Save parent first
                save_template = SavedTemplate(
                    reply_id=reply.id,
                    message_type=message_type,
                    order=index
                )
                save_template.save_to_db()

                for template in templates:
                    title = template["template_title"]
                    thumbnailImageUrl = template["image_url"]
                    text = template["subtitle"]
                    actions = template["buttons"]  # list

                    # Save child
                    gallery = SavedTemplateGallery(
                        template_id=save_template.id,
                        template_title=title,
                        image_url=thumbnailImageUrl,
                        subtitle=text
                    )
                    gallery.save_to_db()

                    save_button(gallery, actions)

            elif message_type == "LINE_IMAGE_CAROUSEL":
                templates = item["message_templates"]

                save_template = SavedTemplate(
                    reply_id=reply_id,
                    message_type=message_type,
                    order=index
                )
                save_template.save_to_db()

                for template in templates:
                    ImageUrl = template["image_url"]
                    actions = template["buttons"]  # list

                    gallery = SavedTemplateGallery(
                        template_id=save_template.id,
                        image_url=ImageUrl
                    )
                    gallery.save_to_db()

                    save_button(gallery, actions)

            elif message_type == "SEND_EMAIL":
                emails = item["emails"]
                save_emails(reply_id, message_type, emails, index)

            elif message_type == "SEND_WEBHOOK":
                webhook_url = item["webhook_url"]
                save_webhook(reply_id, message_type, webhook_url, index)

            elif message_type == "SEND_SEQUENCE":
                sequence = item["sequence"]
                save_sequence(reply_id, message_type, sequence, index)

            else:
                return abort(400, "Invalid message_type")

    if campaign is True:
        return reply
    else:
        resp = saved_reply_schema.dump(reply).data
        resp["saved_reply"] = sorted(resp["saved_reply"], key=lambda k: k["order"])
        return resp, 200


def get_saved_reply_list(account_id, start, limit, bot_id):
    """Get list of saved_reply."""
    account = AccountModel.query.filter_by(id=account_id).first()
    if account is None or account.deleted is True:
        return abort(404, "Account not found.")

    # Set default start and limit if not provided
    if start is None or limit is None:
        start = 1
        limit = 10

    # If channel is not provided, then return all saved_replies
    if bot_id is None:
        replies = db.session.query(SavedReplyModel, BotModel).\
                  filter(BotModel.account_id == account_id).\
                  filter(BotModel.id == SavedReplyModel.bot_id).\
                  filter(BotModel.deleted == False).\
                  filter(SavedReplyModel.deleted == False).\
                  with_entities(SavedReplyModel).all()
    else:
        replies = db.session.query(SavedReplyModel, BotModel).\
                  filter(BotModel.account_id == account_id).\
                  filter(BotModel.id == SavedReplyModel.bot_id).\
                  filter(BotModel.id == bot_id).\
                  filter(BotModel.deleted == False).\
                  filter(SavedReplyModel.deleted == False).\
                  with_entities(SavedReplyModel).all()

    pagination = replies_paginated(
        replies,
        '/v2/saved_replies/{}'.format(account_id),
        start,
        limit,
        bot_id=bot_id
    )
    resp = page_schema.jsonify(pagination)
    resp.status_code = 200
    return resp


def search_saved_reply(account_id, bot_id, text, start, limit, channel):
    """Index only label."""
    account = AccountModel.query.filter_by(id=account_id).first()
    if account is None or account.deleted is True:
        return abort(404, "Account not found.")

    if channel is None:
        replies = SavedReplyModel.query.filter_by(bot_id=bot_id).filter_by(deleted=False).whoosh_search(text).all()
    else:
        replies = SavedReplyModel.query.filter_by(bot_id=bot_id).filter_by(deleted=False).filter_by(channel=channel).whoosh_search(text).all()

    # Set default start and limit if not provided
    if start is None or limit is None:
        start = 1
        limit = 10

    pagination = replies_paginated(
        replies,
        '/v2/saved_replies/{}/{}/search?text={}'.format(account_id, bot_id, text),
        start,
        limit,
        is_search=True
    )
    resp = page_schema.jsonify(pagination)
    resp.status_code = 200
    return resp


def get_saved_reply_detail(account_id, bot_id, saved_reply_id, json_only=False):
    """Get detail saved_reply."""
    obj = db.session.query(AccountModel, SavedReplyModel, BotModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(BotModel.id==bot_id).\
          filter(BotModel.deleted==False).\
          filter(BotModel.account_id==AccountModel.id).\
          filter(SavedReplyModel.bot_id==BotModel.id).\
          filter(SavedReplyModel.id==saved_reply_id).\
          filter(SavedReplyModel.deleted==False).first()
    try:
        account = getattr(obj, "AccountModel")
        reply = getattr(obj, "SavedReplyModel")
    except AttributeError:
        return abort(404, "Saved reply not found.")

    if json_only:
        resp = saved_reply_schema.dump(reply).data
    else:
        resp = saved_reply_schema.jsonify(reply)
        resp.status_code = 200
        pprint.pprint(saved_reply_schema.dump(reply).data)
    return resp


def update_saved_reply(data, account_id, bot_id, saved_reply_id, campaign=None):
    """Update detail saved_reply.
    The idea is deleting existing saved reply, then save the new ones
    """
    # Check format before saved to db
    check_format(data, modify=True)

    label = data.get("label")
    saved_reply = data["saved_reply"]  # list

    # Check account and reply id
    obj = db.session.query(AccountModel, SavedReplyModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(SavedReplyModel.id==saved_reply_id).\
          filter(SavedReplyModel.deleted==False).first()
    try:
        reply = getattr(obj, "SavedReplyModel")
    except AttributeError:
        return abort(404, "Reply not found.")

    if reply.persistent is False:
        reply.label = label
    reply.updated = timestamp()
    reply_id = reply.id
    reply.save_to_db()

    # Delete exisiting saved_reply, and create new ones
    old_text = SavedText.query.filter_by(reply_id=reply_id).delete()

    old_attachment = SavedAttachment.query.filter_by(reply_id=reply_id).delete()

    quicks = SavedQuickReply.query.filter_by(reply_id=reply_id).all()
    for quick in quicks:
        quick_id = quick.id
        old_quick_buttons = SavedQuickReplyButton.query.filter_by(quick_reply_id=quick_id).delete()
    old_quick_replies = SavedQuickReply.query.filter_by(reply_id=reply_id).delete()

    button_templates = SavedButtonTemplate.query.filter_by(reply_id=reply_id).all()
    for template in button_templates:
        template.buttons = []
        template.save_to_db()
    old_button_templates = SavedButtonTemplate.query.filter_by(reply_id=reply_id).delete()

    # Delete existing generic template, line template, and line carousel
    templates = SavedTemplate.query.filter_by(reply_id=reply_id).all()
    for template in templates:
        template.message_templates = []
        template.save_to_db()
    old_templates = SavedTemplate.query.filter_by(reply_id=reply_id).delete()


    for index, item in enumerate(saved_reply, 1):
        message_type = item["message_type"]

        if message_type in ("FB_TEXT", "LINE_TEXT", "TELEGRAM_TEXT"):
            text = item["text"]

            save_text(reply_id, message_type, text, index)

        elif message_type == "FB_ATTACHMENT" or message_type == "LINE_ATTACHMENT":
            attachment_url = item["attachment_url"]

            save_attachment(reply_id, message_type, attachment_url, index)

        elif message_type == "FB_QUICK_REPLY":
            text = item["text"]
            quick_replies = item["quick_replies"]  # list

            quick = save_quick_reply(reply_id, message_type, text, index)
            quick_id = quick.id

            for quick_reply in quick_replies:
                title = quick_reply["title"]
                payload = quick_reply["payload"]

                save_quick_reply_button(quick_id, title, payload)

        elif message_type == "FB_BUTTON_TEMPLATE":
            text = item["text"]
            buttons = item["buttons"]  # list

            # Save the parent first
            button_template = SavedButtonTemplate(
                reply_id=reply_id,
                message_type=message_type,
                text=text,
                order=index
            )
            button_template.save_to_db()

            # Save the childs
            save_button(button_template, buttons)

        elif message_type == "FB_GENERIC_TEMPLATE":
            templates = item["message_templates"]  # list

            # Save parent first
            save_template = SavedTemplate(
                reply_id=reply.id,
                message_type=message_type,
                order=index
            )
            save_template.save_to_db()


            for template in templates:
                template_title = template["template_title"]
                image_url = template["image_url"]
                subtitle = template["subtitle"]
                template_url = template["template_url"]
                buttons = template["buttons"]

                # Save child
                gallery = SavedTemplateGallery(
                    template_id=save_template.id,
                    template_title=template_title,
                    image_url=image_url,
                    subtitle=subtitle,
                    template_url=template_url
                )
                gallery.save_to_db()

                save_button(gallery, buttons)

        elif message_type == "LINE_TEMPLATE":
            templates = item["message_templates"]

            # Save parent first
            save_template = SavedTemplate(
                reply_id=reply.id,
                message_type=message_type,
                order=index
            )
            save_template.save_to_db()

            for template in templates:
                title = template["template_title"]
                thumbnailImageUrl = template["image_url"]
                text = template["subtitle"]
                actions = template["buttons"]  # list

                # Save child
                gallery = SavedTemplateGallery(
                    template_id=save_template.id,
                    template_title=title,
                    image_url=thumbnailImageUrl,
                    subtitle=text
                )
                gallery.save_to_db()

                save_button(gallery, actions)

        elif message_type == "LINE_IMAGE_CAROUSEL":
            templates = item["message_templates"]

            # Save parent first
            save_template = SavedTemplate(
                reply_id=reply.id,
                message_type=message_type,
                order=index
            )
            save_template.save_to_db()

            for template in templates:
                ImageUrl = template["image_url"]
                actions = template["buttons"]  # list

                # Save child
                gallery = SavedTemplateGallery(
                    template_id=save_template.id,
                    image_url=ImageUrl,
                )
                gallery.save_to_db()

                save_button(gallery, actions)

    if campaign is True:
        return reply
    else:
        resp = saved_reply_schema.dump(reply).data
        resp["saved_reply"] = sorted(resp["saved_reply"], key=lambda k: k["order"])
        return resp, 200


def delete_saved_reply(account_id, bot_id, saved_reply_id):
    """Soft delete saved_reply."""
    find_account = AccountModel.find_by_id(account_id)
    if not find_account or find_account.deleted:
        return abort(404, "account not found")

    # Check relationship saved_reply_id, and account_id
    find_saved_reply = SavedReplyModel.query.filter_by(id=saved_reply_id).first()
    if find_saved_reply and not find_saved_reply.deleted:
        if find_saved_reply.persistent is True:
            abort(400, "Persistent reply cannot be deleted")

        if int(bot_id) == int(find_saved_reply.bot_id):
            find_saved_reply.deleted = True
            find_saved_reply.bot_rules = []
            find_saved_reply.save_to_db()

            return '', 204

        return abort(404, "account_id and saved_reply_id didn't match")
    return abort(404, "saved_reply not found")
