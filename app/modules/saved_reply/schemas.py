# pylint: disable=E1101, C0103, W0611, W0622, R0903

"""Schema for SavedReply API endpoints"""
from flask import jsonify
from flask_login import current_user
from ...utilities.abort import abort
from marshmallow import post_dump
from .models import (
    SavedReplyModel, SavedQuickReply,
    SavedButton, SavedTemplate,
    SavedText, SavedAttachment,
    SavedQuickReplyButton, SavedButtonTemplate,
    SavedTemplateGallery, SavedEmails, SavedWebhook, SavedSequence
)
from ...extensions.schema import ma


def replies_paginated(results, url, start, limit, is_search=False, bot_id=None):
    """Returning object for variant of pagination below."""
    start = int(start)
    limit = int(limit)

    obj = {}
    # check if page exists
    if len(results) == 0:
        obj['count'] = 0
        obj['start'] = start
        obj['limit'] = limit
        obj['results'] = []
        obj['previous'] = ''
        obj['next'] = ''

        return obj

    # make response
    obj['start'] = start
    obj['limit'] = limit

    sorted_results = [x for x in reversed(results)]

    # check if page exists
    count = len(sorted_results)
    if count < start:
        return abort(404, "Start exceeds total of the results.")

    obj['count'] = count

    # make URLs
    # make previous url
    if start == 1:
        obj['previous'] = ''

    start_copy = max(1, start - limit)
    limit_copy = start - 1
    if is_search:
        obj['previous'] = url + '&start=%d&limit=%d' % (start_copy, limit_copy)
    else:
        obj['previous'] = url + '?start=%d&limit=%d' % (start_copy, limit_copy)

    if bot_id is not None:
        obj['previous'] += '&bot_id={}'.format(bot_id)

    # make next url
    if start + limit > count:
        obj['next'] = ''

    start_copy = start + limit
    if is_search:
        obj['next'] = url + '&start=%d&limit=%d' % (start_copy, limit)
    else:
        obj['next'] = url + '?start=%d&limit=%d' % (start_copy, limit)

    if bot_id is not None:
        obj['next'] += '&bot_id={}'.format(bot_id)

    # finally extract result according to bounds
    obj['results'] = sorted_results[(start - 1):(start - 1 + limit)]

    return obj


class SavedQuickReplyButtonSchema(ma.ModelSchema):
    class Meta:
        model = SavedQuickReplyButton
        exclude = ["id", "saved_quick_reply", "order"]


class SavedQuickReplySchema(ma.ModelSchema):
    class Meta:
        model = SavedQuickReply
        exclude = ["saved_reply"]
    quick_buttons = ma.Nested("SavedQuickReplyButtonSchema", many=True)


class SavedButtonSchema(ma.ModelSchema):
    class Meta:
        model = SavedButton
        exclude = ["id", "saved_reply", "saved_template", "saved_button_template", "order"]

    @post_dump
    def modify(self, data):
        data["type"] = data.pop("button_type")

        # Delete null key
        if data["type"] == "postback" or data["type"] == "phone_number":
            data.pop("button_url", None)
            data["title"] = data.pop("button_title")
        if data["type"] == "web_url" or data["type"] == "uri":
            data["title"] = data.pop("button_title")
            data["url"] = data.pop("button_url")
            data.pop("payload", None)
        if data["type"] == "element_share":
            data.pop("button_title", None)
            data.pop("button_url", None)


class SavedButtonTemplateSchema(ma.ModelSchema):
    class Meta:
        model = SavedButtonTemplate

    buttons = ma.Nested("SavedButtonSchema", many=True)


class SavedTemplateGallerySchema(ma.ModelSchema):
    class Meta:
        model = SavedTemplateGallery

    buttons = ma.Nested("SavedButtonSchema", many=True)


class SavedTemplateSchema(ma.ModelSchema):
    class Meta:
        model = SavedTemplate
        exclude = ["saved_reply"]

    message_templates = ma.Nested("SavedTemplateGallerySchema", many=True)


class SavedTextSchema(ma.ModelSchema):
    class Meta:
        model = SavedText
        exclude = ["saved_reply"]


class SavedAttachmentSchema(ma.ModelSchema):
    class Meta:
        model = SavedAttachment
        exclude = ["saved_reply"]

class SavedEmailsSchema(ma.ModelSchema):
    class Meta:
        model = SavedEmails
        exclude = ["saved_reply"]

class SavedWebhookSchema(ma.ModelSchema):
    class Meta:
        model = SavedWebhook
        exclude = ["saved_reply"]

class SavedSequenceSchema(ma.ModelSchema):
    class Meta:
        model = SavedSequence
        exclude = ["saved_reply"]

class SavedReplySchema(ma.ModelSchema):
    """Define SavedReply schema"""
    class Meta:
        model = SavedReplyModel
        exclude = ["deleted", "bot_rules", "campaigns"]

    texts = ma.Nested("SavedTextSchema", many=True)
    attachments = ma.Nested("SavedAttachmentSchema", many=True)
    quick_replies = ma.Nested("SavedQuickReplySchema", many=True)
    message_templates = ma.Nested("SavedTemplateSchema", many=True)
    button_templates = ma.Nested("SavedButtonTemplateSchema", many=True)
    emails = ma.Nested("SavedEmailsSchema", many=True)
    webhook = ma.Nested("SavedWebhookSchema", many=True)
    sequence = ma.Nested("SavedSequenceSchema", many=True)

    # Modify response
    @post_dump
    def add_new_fields(self, data):
        try:
            account_id = current_user.account_id
        except:
            account_id = None

        bot_id = data["bot"]
        item_id = data["id"]
        data["_link"] = {
            "item_detail": "/v2/saved_replies/{0}/{1}/{2}".format(account_id, bot_id, item_id)
        }

        # Add the content into saved_reply
        data["saved_reply"] = []
        if len(data["texts"]) > 0:
            for item in data["texts"]:
                data["saved_reply"].append(item)
        if len(data["attachments"]) > 0:
            for item in data["attachments"]:
                data["saved_reply"].append(item)
        if len(data["quick_replies"]) > 0:
            for item in data["quick_replies"]:
                item["quick_replies"] = item.pop("quick_buttons")
                data["saved_reply"].append(item)
        if len(data["button_templates"]) > 0:
            for item in data["button_templates"]:
                data["saved_reply"].append(item)
        if len(data["message_templates"]) > 0:
            for item in data["message_templates"]:
                # Delete null fields based on message_type
                if item["message_type"] == "LINE_TEMPLATE":
                    for template in item["message_templates"]:
                        template.pop("template_url")
                        template.pop("saved_template")
                if item["message_type"] == "LINE_IMAGE_CAROUSEL":
                    for template in item["message_templates"]:
                        template.pop("template_url")
                        template.pop("template_title")
                        template.pop("subtitle")
                        template.pop("saved_template")
                data["saved_reply"].append(item)
        if len(data["emails"]) > 0:
            for item in data["emails"]:
                data["saved_reply"].append(item)
        if len(data["webhook"]) > 0:
            for item in data["webhook_url"]:
                data["saved_reply"].append(item)
        if len(data["sequence"]) > 0:
            for item in data["sequence"]:
                data["saved_reply"].append(item)

        # Sort saved reply item
        from operator import itemgetter
        if len(data['saved_reply']) > 0:
            newlist = sorted(data['saved_reply'], key=itemgetter('order'))
            data['saved_reply'] = newlist


        data.pop("texts", None)
        data.pop("attachments", None)
        data.pop("quick_replies", None)
        data.pop("message_templates", None)
        data.pop("button_templates", None)
        data.pop("emails", None)
        data.pop("webhook", None)
        data.pop("sequence", None)

saved_reply_schema = SavedReplySchema()
saved_replies_schema = SavedReplySchema(many=True)


class PageSchema(ma.Schema):
    """Define pagination schema"""
    class Meta:
        """Custom fields for pagination, results will contain customer list."""
        fields = ('start', 'limit', 'previous', 'next', 'count', 'results')

    results = ma.Nested('SavedReplySchema', many=True)

page_schema = PageSchema()
