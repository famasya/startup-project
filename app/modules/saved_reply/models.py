# pylint: disable=E1101, C0103, W0611, W0622, R0903

"""This model using SQLAlchemy as ORM for SavedReply API endpoint."""
import datetime
import flask_whooshalchemy as wa
from flask import abort, current_app

from app.extensions.db import db
from app.utilities.time import timestamp


class SavedQuickReplyButton(db.Model):
    __tablename__ = "saved_quick_reply_buttons"

    quick_reply_id = db.Column(db.Integer, db.ForeignKey("saved_quick_replies.id"))
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.Integer)

    title = db.Column(db.String)
    payload = db.Column(db.String)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


class SavedQuickReply(db.Model):
    """This for only messenger type message."""
    __tablename__ = "saved_quick_replies"

    reply_id = db.Column(db.Integer, db.ForeignKey("saved_replies.id"))
    id = db.Column(db.Integer, primary_key=True)

    text = db.Column(db.String)
    order = db.Column(db.Integer)
    message_type = db.Column(db.String)

    # Childs
    quick_buttons = db.relationship('SavedQuickReplyButton', backref='saved_quick_reply', lazy='dynamic')

    def __repr__ (self):
        return '<QuickReply (quick_reply=%r)>' % (self.quick_reply)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


rel_buttons = db.Table("rel_buttons",
    db.Column("id", db.Integer, primary_key=True),
    db.Column("button_id", db.Integer, db.ForeignKey("saved_buttons.id")),
    db.Column("reply_id", db.Integer, db.ForeignKey("saved_replies.id")),
    db.Column("gallery_id", db.Integer, db.ForeignKey("saved_templates_gallery.id")),
    db.Column("button_template_id", db.Integer, db.ForeignKey("saved_button_templates.id")),
)
class SavedButton(db.Model):
    """Define button for message type template.
    Button types:
    * web_url: when clicked, it redirect to somewhere
    * postback: when clicked, it will post a payload

    This button has many to many relationship, so that it can reuse
    for message type generic_template and button_template
    """
    __tablename__ = 'saved_buttons'
    id = db.Column(db.Integer, primary_key=True)

    button_url = db.Column(db.String)
    button_title = db.Column(db.String)
    button_type = db.Column(db.String)  # web_url, postback
    payload = db.Column(db.String)
    order = db.Column(db.Integer)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


class SavedButtonTemplate(db.Model):
    __tablename__ = "saved_button_templates"
    reply_id = db.Column(db.Integer, db.ForeignKey('saved_replies.id'))
    id = db.Column(db.Integer, primary_key=True)

    message_type = db.Column(db.String)
    text = db.Column(db.String)
    order = db.Column(db.Integer)

    buttons = db.relationship('SavedButton', secondary=rel_buttons, backref=db.backref('saved_button_template', lazy='dynamic'))

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


class SavedTemplateGallery(db.Model):
    """Child of SavedTemplate."""
    __tablename__ = 'saved_templates_gallery'

    template_id = db.Column(db.Integer, db.ForeignKey('saved_templates.id'))
    id = db.Column(db.Integer, primary_key=True)

    # Messenger
    template_title = db.Column(db.String)
    image_url = db.Column(db.String)
    subtitle = db.Column(db.String)
    template_url = db.Column(db.String)

    # Child many to many
    buttons = db.relationship('SavedButton', secondary=rel_buttons, backref=db.backref('saved_template', lazy='dynamic'))

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


class SavedTemplate(db.Model):
    """Defind Messennger message type template."""
    __tablename__ = 'saved_templates'

    reply_id = db.Column(db.Integer, db.ForeignKey('saved_replies.id'))
    id = db.Column(db.Integer, primary_key=True)
    order = db.Column(db.Integer)
    message_type = db.Column(db.String)

    message_templates = db.relationship('SavedTemplateGallery', backref="saved_template", lazy="dynamic")

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


class SavedText(db.Model):
    """Saved reply text."""
    __tablename__ = 'saved_texts'

    reply_id = db.Column(db.Integer, db.ForeignKey('saved_replies.id'))
    id = db.Column(db.Integer, primary_key=True)

    text = db.Column(db.String)
    order = db.Column(db.Integer)
    message_type = db.Column(db.String)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


class SavedAttachment(db.Model):
    """Saved reply attachment."""
    __tablename__ = 'saved_attachments'

    reply_id = db.Column(db.Integer, db.ForeignKey('saved_replies.id'))
    id = db.Column(db.Integer, primary_key=True)

    order = db.Column(db.Integer)
    attachment_url = db.Column(db.String)
    message_type = db.Column(db.String)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

class SavedEmails(db.Model):
    """Saved reply emails"""
    __tablename__ = "saved_emails"

    reply_id = db.Column(db.Integer, db.ForeignKey('saved_replies.id'))
    id = db.Column(db.Integer, primary_key=True)

    emails = db.Column(db.JSON)
    order = db.Column(db.Integer)
    message_type = db.Column(db.String)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

class SavedWebhook(db.Model):
    """Saved reply webhook"""
    __tablename__ = "saved_webhook"

    reply_id = db.Column(db.Integer, db.ForeignKey('saved_replies.id'))
    id = db.Column(db.Integer, primary_key=True)

    webhook_url = db.Column(db.String)
    message_type = db.Column(db.String)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

class SavedSequence(db.Model):
    """Saved reply sequence"""
    __tablename__ = "saved_sequence"

    reply_id = db.Column(db.Integer, db.ForeignKey('saved_replies.id'))
    id = db.Column(db.Integer, primary_key=True)

    sequence = db.Column(db.String(6))
    message_type = db.Column(db.String)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

class SavedReplyModel(db.Model):
    """Defind SavedReplyModel."""
    __tablename__ = 'saved_replies'
    __searchable__ = ["label"]

    bot_id = db.Column(db.Integer, db.ForeignKey('bots.id'))
    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String)
    campaign = db.Column(db.Boolean, default=False)
    persistent = db.Column(db.Boolean, default=False)
    campaign = db.Column(db.Boolean, default=False)
    channel = db.Column(db.String)  # channel: messenger, line
    ref = db.Column(db.String, default='NO_REF')  # for additional references, eg: welcome message, default answer
    sent = db.Column(db.Integer)
    opened = db.Column(db.Integer)

    # Childs
    attachments = db.relationship('SavedAttachment', backref='saved_reply', lazy='dynamic')
    texts = db.relationship('SavedText', backref='saved_reply', lazy='dynamic')
    quick_replies = db.relationship('SavedQuickReply', backref='saved_reply', lazy='dynamic')
    message_templates = db.relationship('SavedTemplate', backref='saved_reply', lazy='dynamic')
    button_templates = db.relationship('SavedButtonTemplate', backref='saved_reply', lazy='dynamic')
    emails = db.relationship('SavedEmails', backref='saved_reply', lazy='dynamic')
    webhook = db.relationship('SavedWebhook', backref='saved_reply', lazy='dynamic')
    sequence = db.relationship('SavedSequence', backref='saved_reply', lazy='dynamic')

    deleted = db.Column(db.Boolean, default=False)
    created = db.Column(db.Integer, default=timestamp)
    updated = db.Column(db.Integer)


    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


# Register search index capability
wa.whoosh_index(current_app, SavedReplyModel)
