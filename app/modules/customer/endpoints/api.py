# pylint: disable=R0201, C0103, W0611, C0111, C0301

"""List of API endpoint for Customer module"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ....utilities.api_limiter import api_limiter

from ..parsers import pagination_arguments
from ..ops import (
    get_customer, update_customer,
    get_customer_list, delete_customer
)


api = Namespace('customers', description='Customer related operations')

# JSON Format
post_customer = api.model('Customer', {
    "name": fields.String,
    "avatar_url": fields.String,
    "active": fields.Boolean(default=True),
    "platform": fields.String,
    "platform_id": fields.String,
    "email": fields.String
})


@api.param('account_id', 'The account identifier')
@api.route('/<account_id>')
class ListCustomer(Resource):
    @api.expect(pagination_arguments)
    @login_required
    def get(self, account_id):
        api_limiter(account_id, request.referrer)
        """Get list of customers."""
        start = request.args.get('start', 1)
        limit = request.args.get('limit', 10)

        resp = get_customer_list(account_id, start, limit)
        return resp


@api.param('account_id', 'The account identifier')
@api.route('/<account_id>/<customer_id>')
class CustomerItem(Resource):
    @login_required
    def get(self, account_id, customer_id):
        api_limiter(account_id, request.referrer)
        print(request.referrer)
        """Get a customer detail."""
        resp = get_customer(account_id, customer_id)
        return resp


    @api.expect(post_customer)
    @login_required
    def put(self, account_id, customer_id):
        api_limiter(account_id, request.referrer)
        """Update a customer detail."""
        data = request.json
        resp = update_customer(data, account_id, customer_id)

        return resp


    @login_required
    def delete(self, account_id, customer_id):
        """Soft delete a customer."""
        resp = delete_customer(account_id, customer_id)

        return resp
