# pylint: disable=C0103

"""Create parsers pagination to make arguments in Swagger API UI."""
from flask_restplus import reqparse

pagination_arguments = reqparse.RequestParser()
pagination_arguments.add_argument('start', type=int, required=False,
                                  default=1, help='Page number')
pagination_arguments.add_argument('limit', type=int, required=False,
                                  choices=[2, 10, 20, 30, 40, 50],
                                  default=10, help='Results per page {error_msg}')
