# pylint: disable=E1101, C0103, W0611, W0622, R0903

"""This model using SQLAlchemy as ORM for Customer API endpoint."""

import datetime
import flask_whooshalchemy as wa
from flask import abort, current_app
from ..woocommerce.models import WoocommerceLog

from app.extensions.db import db
from app.utilities.time import timestamp
from ..zendesk.models import ZendeskTicketLog

class CustomerModel(db.Model):
    """Define CustomerModel."""
    __tablename__ = 'customers'
    __searchable__ = ["name"]

    account_id = db.Column(db.String, db.ForeignKey('accounts.id'))

    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String(80))
    avatar_url = db.Column(db.String)
    email = db.Column(db.String, nullable=True)
    platform = db.Column(db.String(80))
    platform_id = db.Column(db.String)
    active = db.Column(db.Boolean, default=True)
    last_seen = db.Column(db.Integer, default=timestamp)  # It will update each time rest api receive message from specific customer
    deleted = db.Column(db.Boolean, default=False)
    date_created = db.Column(db.Integer, default=timestamp)

    inboxes = db.relationship('InboxModel', backref='customer', lazy='dynamic')
    zendesk_customers = db.relationship('CustomerZendesk', backref='customer', lazy='dynamic')
    woocommerce_customers = db.relationship('CustomerWoocommerce', backref='customer', lazy='dynamic')
    woocommerce_log = db.relationship('WoocommerceLog', backref='customer', lazy='dynamic')


    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

    def ping(self):
        """Marks the customer as recently seen."""
        self.last_seen = timestamp()
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise


    @classmethod
    def find_by_id(cls, id):
        """Find model object by id."""
        return cls.query.filter_by(id=id).first()


    @classmethod
    def find_by_account_id(cls, id):
        """Find model object by account_id."""
        return cls.query.filter_by(account_id=id).first()


# Register whoosh for search capability
wa.whoosh_index(current_app, CustomerModel)

class CustomerMetadata(db.Model):
    __tablename__ = 'customer_metadata'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String)
    channel_type = db.Column(db.String)
    status = db.Column(db.String)
    channel_id = db.Column(db.String, db.ForeignKey('channels.id'))
    account_id = db.Column(db.String, db.ForeignKey('accounts.id'))
    inbox_id = db.Column(db.String, db.ForeignKey('inboxes.id'))

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

class CustomerZendesk(db.Model):
    __tablename__ = 'customer_zendesk'

    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.String, db.ForeignKey('customers.id'))
    email = db.Column(db.String)
    name = db.Column(db.String)
    url = db.Column(db.String)
    zendesk_id = db.Column(db.String)
    phone = db.Column(db.String)
    timezone = db.Column(db.String)
    details = db.Column(db.String)
    tickets = db.Column(db.JSON)
    notes = db.Column(db.Boolean)

    zendesk_ticket_logs = db.relationship('ZendeskTicketLog', backref='message', lazy='dynamic')

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

class CustomerWoocommerce(db.Model):
    __tablename__ = 'customer_woocommerce'

    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.String, db.ForeignKey('customers.id'))
    order_data = db.Column(db.JSON)
    date_created = db.Column(db.Integer, default=timestamp)
    date_updated = db.Column(db.Integer, default=timestamp)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise
