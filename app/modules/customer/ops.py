# pylint: disable=E1101

"""List of Operation for Customer API."""
from flask import jsonify, Response
from ...utilities.abort import abort
# from flask_restplus import abort
from .models import CustomerModel, CustomerMetadata
from .schemas import (
    customer_schema, page_schema, customer_metadata
)
from ..account.models import AccountModel
from ..inbox.models import InboxModel
from ..inbox.ops import fetch_customer_data_sync
from ..woocommerce.ops import get_information
from app.utilities.unique_url import customer_uuid_url64
from app.utilities.pagination import get_paginated_item_list
from app.extensions.db import db


def get_customer_list(account_id, start, limit):
    """Get list of customer with pagination."""
    page_dump = get_paginated_item_list(CustomerModel,
                                        account_id,
                                        '/v2/customers/{}'.format(account_id),
                                        start,
                                        limit)

    resp = page_schema.jsonify(page_dump)
    resp.status_code = 200

    return resp


def get_customer(account_id, customer_id):
    """Get a customer detail."""
    obj = db.session.query(AccountModel, CustomerModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(CustomerModel.id==customer_id).\
          filter(CustomerModel.deleted==False).first()

    try:
        account = getattr(obj, "AccountModel")
        customer = getattr(obj, "CustomerModel")
    except AttributeError:
        return abort(404, 'Customer not found.')

    related = CustomerMetadata.query.filter_by(account_id=account_id).filter_by(email=customer.email).all()
    related_account, error = customer_metadata.dump(related)

    # Check customer and account should be related
    if account.id != customer.account_id:
        return abort(404, "Account and Customer didn't match.")

    cust, error = customer_schema.dump(customer)
    cust['related'] = related_account
    return cust, 200


def update_customer(data, account_id, customer_id):
    """Update a customer detail."""
    email = data.get("email")

    obj = db.session.query(AccountModel, CustomerModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(CustomerModel.id==customer_id).\
          filter(CustomerModel.deleted==False).first()
    try:
        account = getattr(obj, "AccountModel")
        customer = getattr(obj, "CustomerModel")
    except AttributeError:
        return abort(404, "Customer not found.")

    # Check customer and account should be related
    if account.id != customer.account_id:
        return abort(404, "Account and Customer didn't match.")

    customer.email = email
    customer.save_to_db()

    resp = customer_schema.dump(customer).data
    zendesk_data = fetch_customer_data_sync(email, account_id, customer.id)
    woo_data = get_information(account_id, None, customer_id=customer.id)
    resp['data'] = {
        "zendesk": zendesk_data,
        "woocommerce": woo_data
    }
    return resp


def delete_customer(account_id, customer_id):
    """Soft delete a customer."""
    obj = db.session.query(AccountModel, CustomerModel).\
          filter(AccountModel.id==account_id).\
          filter(AccountModel.deleted==False).\
          filter(CustomerModel.id==customer_id).\
          filter(CustomerModel.deleted==False).first()
    try:
        account = getattr(obj, "AccountModel")
        customer = getattr(obj, "CustomerModel")
    except AttributeError:
        return abort(404, "Customer not found.")

    # Check customer and account should be related
    if account.id != customer.account_id:
        return abort(404, "Account and Customer didn't match.")

    customer.deleted = True
    customer.save_to_db()
    account.current_customer -= 1
    account.save_to_db()
    return None, 204
