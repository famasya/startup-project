# pylint: disable=E1101, C0103, W0611, W0622, R0903

"""Serialize database fields for Customer module."""
from marshmallow import post_dump
from app.extensions.schema import ma
from flask import current_app

from .models import CustomerModel, CustomerMetadata, CustomerZendesk, CustomerWoocommerce

class CustomerSchema(ma.ModelSchema):
    """Define customer schema"""
    class Meta:
        """This will return the json format for each field in CustomerModel."""
        model = CustomerModel
        exclude = ['inboxes', 'deleted', 'zendesk_customers', 'woocommerce_customers', 'woocommerce_log']

    # Inject new key _link into obj after it has been serialized
    # _link is a key to help frontend navigate next api
    @post_dump
    def add_new_fields(self, in_data):
        account_id = in_data["account"]
        customer_id = in_data["id"]
        in_data["_link"] = {
            "item_detail": "/v2/customer/{0}/{1}".format(account_id, customer_id)
        }

customer_schema = CustomerSchema()
customers_schema = CustomerSchema(many=True)


class PageSchema(ma.Schema):
    """Define pagination schema"""
    class Meta:
        """Custom fields for pagination, results will contain customer list."""
        fields = ('start', 'limit', 'previous', 'next', 'count', 'results')

    results = ma.Nested('CustomerSchema', many=True)

page_schema = PageSchema()

class CustomerMetadataSchema(ma.ModelSchema):
    class Meta:
        model = CustomerMetadata
        fields = ('inbox_id','channel_type', 'status', 'email')

    @post_dump
    def add_new_fields(self, in_data):
        status = in_data["status"]
        inbox_id = in_data["inbox_id"]
        in_data["url"] = "{}/conversations/{}/{}".format(current_app.config["CLIENT_URL"] , status, inbox_id)

customer_metadata = CustomerMetadataSchema(many=True)

class CustomerZendeskSchema(ma.ModelSchema):
    class Meta:
        model = CustomerZendesk
        exclude = ["zendesk_ticket_logs"]

customers_zendesk = CustomerZendeskSchema(many=True)
customer_zendesk = CustomerZendeskSchema()

class CustomerWoocommerceSchema(ma.ModelSchema):
    class Meta:
        model = CustomerWoocommerce
        exclude = ["customer"]

    @post_dump
    def add_new_fields(self, in_data):
        filtered_data = []
        try:
            for item in in_data["order_data"]:
                data = {
                    "order_key": item["order_key"],
                    "currency": item["currency"],
                    "created_gmt": item["date_created_gmt"],
                    "total": item["total"],
                    "billing": item["billing"],
                    "shipping": item["shipping"],
                    "completed_gmt": item["date_completed_gmt"],
                    "payment_method": item["payment_method_title"],
                    "product": item["line_items"],
                }
                filtered_data.append(data)
            in_data.pop("order_data", None)
            in_data["order_histories"] = filtered_data
        except:
            pass

customer_woocommerce = CustomerWoocommerceSchema()
