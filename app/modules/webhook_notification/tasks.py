import requests
from flask import render_template, current_app
from app.extensions.flask_celery import make_celery

celery = make_celery(current_app)

@celery.task(bind=True, retry_backoff=True)
def send_webhook_notification(self, account, channel, message, inbox, customer):

    # Check if it has webhook
    headers = {
        "X-Chatkoo-Token": account["webhook_token"]
    }

    message_type = message["message_type"].lower()

    if "attachment" in message_type:
        message = {
            "type": message_type,
            "id": message["id"],
            "author": message["author"],
            "attachment_url": message["attachment_url"],
            "received": message["timestamp"],
        }
    else:
        message = {
            "type": message_type,
            "id": message["id"],
            "author": message["author"],
            "text": message["text"],
            "received": message["timestamp"],
        }

    payload = {
        "trigger": "message:received",
        "account": account["id"],
        "message": message,
        "metadata": {
            "channel": channel,
            "inbox": inbox,
            "customer": customer
        }
    }

    # Send it
    url = account["webhook_url"]

    # If it fails, retry for exponential time
    try:
        requests.post(url, json=payload, headers=headers)
    except:
        self.retry(max_retries=3)

    return True
