from ..account.models import AccountModel
from ..message.schemas import message_schema
from .tasks import send_webhook_notification


def receive_message(account_id, message, inbox):
    """It will post to registered account webhook."""
    account = AccountModel.query.\
            with_entities(AccountModel.webhook_url, AccountModel.webhook_token).\
            filter_by(id=account_id).first()
    if account.webhook_url:
        account_data = {
            "id": account_id,
            "webhook_url": account.webhook_url,
            "webhook_token": account.webhook_token
        }
        data_message = message_schema.dump(message).data
        inbox_data = {
            "id": inbox.id,
            "status": inbox.status,
            "bot_status": inbox.bot_status,
        }
        customer_data = {
            "id": inbox.customer.id,
            "name": inbox.customer.name,
            "avatar": inbox.customer.avatar_url,
        }
        channel_data = {
            "id": data_message["recipient_id"],
            "platform": inbox.customer.platform,
            "name": inbox.via_channel
        }

        send_webhook_notification.delay(account_data, channel_data, data_message, inbox_data, customer_data)


def bot_triggered():
    pass
