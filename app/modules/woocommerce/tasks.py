from .ops import get_information
from flask import current_app
from app.extensions.flask_celery import make_celery

celery = make_celery(current_app)

@celery.task(bind=True, default_retry_delay=5)
def fetch_woocommerce_data(self, account_id, inbox_id):
	try:
		get_information(account_id, inbox_id, task=True)
	except Exception as exc:
		self.retry(exc=exc, max_retries=3)
