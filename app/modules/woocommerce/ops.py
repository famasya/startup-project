from ..inbox.models import InboxModel
from ..account.models import AccountModel
from ..customer.models import CustomerModel, CustomerWoocommerce
from ..integration.models import IntegrationModel
from ...extensions.db import db
from ...utilities.abort import abort
from ...utilities.time import timestamp
from woocommerce import API
import requests

def  get_information(account_id, inbox_id, customer_id=None, task=False):

	wooco = db.session.query(IntegrationModel, AccountModel).\
			filter(AccountModel.id == account_id).\
			filter(AccountModel.deleted == False).\
			filter(IntegrationModel.account_id == AccountModel.id).\
			filter(IntegrationModel.deleted == False).\
			filter(IntegrationModel.type == "WOOCOMMERCE").\
			with_entities(IntegrationModel).first()

	try:	
		url = wooco.url
	except:
		if task:
			return ''
		else:
			abort(404, "No woocommerce integration found")

	if url[-1] != "/":
		url += "/"

	wcapi = API(
		url=url,
		consumer_key=wooco.key_id,
		consumer_secret=wooco.secret,
		wp_api=True,
		version="wc/v2"
	)

	try:
		orders = wcapi.get("orders").json()
	except:
		return ""

	if customer_id == None:
		customer = db.session.query(CustomerModel, InboxModel).\
				   filter(InboxModel.id == inbox_id).\
				   filter(InboxModel.deleted == False).\
				   filter(InboxModel.customer_id == CustomerModel.id).\
				   filter(CustomerModel.deleted == False).\
				   with_entities(CustomerModel).first()
	else:
		customer = CustomerModel.query.filter_by(id=customer_id).filter_by(deleted=False).first()

	if customer.email is None:
		return "", 204

	email = customer.email
	try:
		customer_orders = list(filter(lambda item: item['billing']['email'] == email, orders))
	except:
		return "", 204	

	# Check if order records for customer exists
	order_records = CustomerWoocommerce.query.filter_by(customer_id=customer.id).first()

	if order_records is None:
		customer_order = CustomerWoocommerce(
			customer_id = customer.id,
			order_data = customer_orders
		)
		customer_order.save_to_db()
	else:
		order_records.order_data = customer_orders
		order_records.date_updated = timestamp()
		order_records.save_to_db()

	return customer_orders
