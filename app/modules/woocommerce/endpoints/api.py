from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ....utilities.api_limiter import api_limiter
from ..ops import get_information

api = Namespace('bots', description='Woocommerce related operations')

@api.route('/<account_id>/<inbox_id>')
class Woocommerce(Resource):
	def get(self, account_id, inbox_id):
		'''Get woocommerce information'''
		api_limiter(account_id, request.referrer)
		return get_information(account_id, inbox_id)
