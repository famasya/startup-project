import datetime
import flask_whooshalchemy as wa
from flask import abort, current_app

from app.extensions.db import db
from app.utilities.time import timestamp

class WoocommerceLog(db.Model):
    __tablename__ = 'woocommerce_log'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    email = db.Column(db.String)
    phone = db.Column(db.String)
    payload = db.Column(db.JSON)
    customer_id = db.Column(db.String, db.ForeignKey('customers.id'))
    date_created = db.Column(db.Integer, default=timestamp)
    date_updated = db.Column(db.Integer, default=timestamp)

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise
