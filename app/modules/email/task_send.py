# pylint: disable=C0103

"""Celery file to send email."""
from flask import render_template, abort, current_app
from flask_mail import Message
from itsdangerous import URLSafeTimedSerializer, SignatureExpired

from app.extensions.mail import mail
from app.modules.account.models import AccountModel
from app.extensions.flask_celery import make_celery

celery = make_celery(current_app)

@celery.task
def send_email(subject, sender, recipients, text_body):
    """A celery task to send general email."""
    msg = Message(subject, sender=sender, recipients=[recipients])
    msg.body = text_body

    mail.send(msg)
