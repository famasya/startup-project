
"""Add celery tasks here."""
import requests
from flask import current_app
from flask_login import current_user
from ..customer.models import CustomerMetadata, CustomerZendesk
from ..integration.models import IntegrationModel
import base64, json
from app.extensions.flask_celery import make_celery

celery = make_celery(current_app)


@celery.task(bind=True, default_retry_delay=5)
def fetch_customer_data(self, email, account_id, customer_id, inbox_id=None):
    print("FETCHING...")
    # Check if email is not set
    if email is None:
        try:
            query = CustomerMetadata.query.filter_by(account_id=account_id).filter_by(inbox_id=inbox_id).first()
        except Exception as exc:
            self.retry(exc=exc, max_retries=3)

        if query:
            email = query.email
        elif query is None:
            return True 

    # Check if integration exists
    integration = IntegrationModel.query.\
            filter_by(account_id=account_id).\
            filter_by(type="ZENDESK").\
            filter_by(deleted=False).first()

    # If no integration found, return true instead of returning error. Zendesk is not mandatory
    if integration is None:
        return True

    # Zendesk search credentials
    key = "{}/token:{}".format(integration.email, integration.token)
    encoded = str(base64.b64encode(key.encode("utf-8")), "utf-8")
    url = "{}/api/v2/users/search.json".format(integration.url)
    querystring = {
        "query":email
    }
    headers = {
        "Authorization": "Basic {}".format(encoded),
        "cache-control": "no-cache"
    }

    response = requests.get(url, headers=headers, params=querystring)

    try:
        data = json.loads(response.text)["users"][0]
    except:
        return True

    zendesk_id = data["id"]
    url = data["url"]
    name = data["name"]
    phone = data["phone"]
    notes = data["notes"]
    email = data["email"]
    timezone = data["time_zone"]
    details = data["details"]

    # Search existing tickets based on given zendesk_id
    search_user_ticket_url = "{}/api/v2/users/{}/tickets/requested.json".format(integration.url, zendesk_id)
    ticket_response = requests.get(search_user_ticket_url, headers=headers)
    tickets = []
    for ticket in json.loads(ticket_response.text)["tickets"]:
        tickets.append({
            "id": ticket["id"],
            "via": ticket["via"],
            "created_at": ticket["created_at"],
            "subject": ticket["subject"],
            "description": ticket["description"],
            "status": ticket["status"],
        })

    # If existing zendesk customer has been saved in the database, update it. Otherwise, create new record
    existing_account = CustomerZendesk.query.filter_by(email=email).first()
    if existing_account is None:
        customer_zendesk = CustomerZendesk(
            customer_id=customer_id,
            zendesk_id=zendesk_id,
            name=name,
            url=url,
            phone=phone,
            notes=notes,
            timezone=timezone,
            details=details,
            email=email,
            tickets=tickets
        )
        customer_zendesk.save_to_db()
    else:
        existing_account.phone = 123
        existing_account.details = details
        existing_account.notes = notes
        existing_account.tickets = tickets
        existing_account.save_to_db()

    print("FETCHING DONE....")
    return True
