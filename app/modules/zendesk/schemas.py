from ...extensions.schema import ma
from .models import ZendeskTicketLog

class TicketLog(ma.ModelSchema):

    class Meta:
        model = ZendeskTicketLog
        exclude = ["message"]

ticket = TicketLog()
