import requests
from flask import current_app
from ...utilities.abort import abort
from ..integration.models import IntegrationModel
from ..message.models import MessageModel
from ..customer.models import CustomerZendesk, CustomerModel
from .schemas import ticket
from ..inbox.models import InboxModel, InboxSession
from ...extensions.db import db
from .models import ZendeskTicketLog
import base64
import json
import time, datetime

def create_ticket(account_id, inbox_id, data):
	# Check mandatory ticket format
	try:
		subject = data["subject"]
		body = data["body"]
	except:
		abort(401, "Invalid request")

	# Check for optional ticket fields
	if "type" in data:
		_type = data["type"]
	else:
		_type = None		
	if "tags" in data:
		tags = data["tags"]
	else:
		tags = None		
	if "priority" in data:
		priority = data["priority"]
	else:
		priority = None		
	if "requester_id" in data:
		requester_id = data["requester_id"]
	else:
		requester_id = None		

	# Check if zendesk integration has been created
	integration = IntegrationModel.query.filter_by(account_id=account_id).filter_by(type='ZENDESK').first()
	if integration is None:
		abort(404, "No zendesk integration found")

	# Customer must set their email in advance before create zendesk ticket
	customer_query = db.session.query(CustomerZendesk, InboxModel).\
						filter(InboxModel.account_id==account_id).\
						filter(InboxModel.id==inbox_id).\
						filter(InboxModel.customer_id==CustomerZendesk.customer_id).first()

	print(customer_query)
	try:
		customer_zendesk = getattr(customer_query, "CustomerZendesk")
		inbox = getattr(customer_query, "InboxModel")
	except:
		abort(404, "No associated zendesk account found. Please check if you have set customer email")

	# Grab current active chat as ticket attachment
	chats = db.session.query(InboxSession, MessageModel).\
			filter(InboxSession.inbox_id==inbox_id).\
			filter(InboxSession.closed_date==None).\
			filter(MessageModel.inbox_id==inbox_id).\
			filter(MessageModel.timestamp>=InboxSession.open_date).\
			with_entities(MessageModel).all()
	if chats is None:
		abort(404, "No active chat found")

	inbox_url = '{}/conversations/{}/{}'.format(current_app.config['CLIENT_URL'], inbox.status, inbox_id)
	# Format chat messages in HTML
	messages = "<p>{}</p>Inbox link: <a href='{}'>{}</a><br><br><br><small>Sent from Chatkoo dashboard</small><br><hr /><p><strong>Recent messages transcript</strong></p>".format(body, inbox_url, inbox_url)
	for message in chats:
		if message.status == "sent":
			messages = messages+"<div style='display: block; padding: 3px;'><small> [{}] <strong>Agent</strong> : {} </small></div>".format(datetime.datetime.fromtimestamp(message.timestamp).strftime('%c'), message.text)
		else:
			messages = messages+"<div style='display: block; padding: 3px;'><small> [{}] <strong>Customer</strong> : {} </small></div>".format(datetime.datetime.fromtimestamp(message.timestamp).strftime('%c'), message.text)

	# Zendesk credentials
	key = "{}/token:{}".format(integration.email, integration.token)
	encoded = str(base64.b64encode(key.encode("utf-8")), "utf-8")
	url = "{}/api/v2/tickets.json".format(integration.url)
	headers = {
		"Authorization": "Basic {}".format(encoded),
		"cache-control": "no-cache"
	}

	# Requesting zendesk ticket
	payload = {
		"ticket": {
			"subject": subject,
			"comment": {
				"html_body": messages
			},
			"priority": priority,
			"tags": tags,
			"type": _type,
			"requester_id": requester_id
		}
	}
	response = requests.post(url, json=payload, headers=headers)

	if response.status_code != 201:
		abort(500, "Cannot contact zendesk API")

	# If zendesk ticket has been created, save to the logs
	log = ZendeskTicketLog(
		customer_zendesk_id=customer_zendesk.id,
		timestamp=time.time(),
		subject=subject,
		body=body,
		priority=priority,
		tags=tags,
		_type=_type,
		requester_id=requester_id
		)

	log.save_to_db()
	result, error = ticket.dump(log)

	return result, 200
