from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ....utilities.api_limiter import api_limiter
from ..ops import create_ticket

api = Namespace('zendesk', description='Zendesk related operations')

@api.route('/<account_id>/<inbox_id>/tickets')
class TicketRequest(Resource):
	def post(self, inbox_id, account_id):
		'''Create ticket to zendesk using API'''
		api_limiter(account_id, request.referrer)
		data = request.json
		return create_ticket(account_id, inbox_id, data)
