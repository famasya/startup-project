from app.extensions.db import db
from app.utilities.time import timestamp

class ZendeskTicketLog(db.Model):

	__tablename__ = "zendesk_ticket_log"

	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	customer_zendesk_id = db.Column(db.Integer, db.ForeignKey('customer_zendesk.id'))
	timestamp = db.Column(db.String)
	subject = db.Column(db.String)
	body = db.Column(db.String)
	priority = db.Column(db.String)
	tags = db.Column(db.String)
	_type = db.Column(db.String)
	requester_id = db.Column(db.String)

	def save_to_db(self):
	    """Save to database."""
	    try:
	        db.session.add(self)
	        db.session.commit()
	    except:
	       db.session.rollback()
	       raise
