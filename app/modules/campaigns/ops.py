# pylint: disable=E1101

from flask import current_app, render_template, jsonify
from .models import CampaignsModel
from .tasks import total_reach_messenger, send_broadcast
from .schemas import (
	campaigns_schema, campaign_schema,
	campaign_paginated, page_schema
)
from ..account.models import AccountModel
from ..saved_reply.ops import (
	create_saved_reply, update_saved_reply, check_format
)
from ..customer.models import CustomerModel
from ..channel.models import ChannelModel
from ..webhook_connector.tasks import send_woo_reminder
from ..bot.models import BotModel
from ...utilities.abort import abort
import requests
from app.extensions.db import db


def create_campaign(data, account_id, update=None, campaign_id=None, quick=None, check_type=False):
	# Check data format
	WEBHOOK_URL = current_app.config['WEBHOOK_URL']
	fb_graph = current_app.config['FB_GRAPH']
	try:
		label = data["label"]
		type_ = data["campaign_type"]
		bot_id = data["bot_id"]
		channel = data["channel"]
		content = data["content"]
	except:
		abort(400, "Invalid format.")

	# Set default label
	if label is None or label == "":
		label = "New campaign"

	# Check format in content
	if quick is None or quick is False:
		try:
			triggered_by = content["triggered_by"]
			triggered_after = content["triggered_after"]
			send_after = content["send_after"]
			message = content["message"]
		except:
			abort(400, "Invalid format on content")
		check_format(message)

		# Check persistence status
		if data.get("persistent") is None or data.get("persistent") is False:
			persistent = False
		else:
			persistent = True
	else:
		persistent = False

	if update is True:
		try:
			is_active = data["is_active"]
		except:
			abort(400, "Invalid is_active")
		campaign_id = campaign_id

	# Check account_id and bot_id relation
	obj = db.session.query(AccountModel, BotModel).\
		  filter(AccountModel.id==account_id).\
		  filter(AccountModel.deleted==False).\
		  filter(BotModel.id==bot_id).\
		  filter(BotModel.account_id==AccountModel.id).\
		  filter(BotModel.deleted==False).first()
	try:
		account = getattr(obj, "AccountModel")
		bot = getattr(obj, "BotModel")
	except AttributeError:
		return abort(404, "Bot not found.")

	# Get page_access_token
	channelObj = db.session.query(BotModel, ChannelModel).\
			  filter(BotModel.id == bot_id).\
			  filter(ChannelModel.id == BotModel.channel_id).\
			  filter(ChannelModel.deleted == False).\
			  with_entities(ChannelModel.page_access_token).first()
	if channelObj is None:
		abort(400)

	page_access_token = channelObj.page_access_token

	if update is True:
		# Update campaign
		campaign = CampaignsModel.query.\
				   filter_by(id=campaign_id).\
				   filter_by(bot_id=bot_id).\
				   filter_by(deleted=False).first()
		if campaign is None:
			return abort(404, "Campaign not found.")
		campaign.label = label
		campaign.campaign_type = type_
		campaign.channel = channel
		campaign.triggered_by = triggered_by
		campaign.triggered_after = triggered_after
		campaign.send_after = send_after
		campaign.is_active = is_active

		# Delete existing related replies first
		campaign.replies = []

		# Update saved reply
		if len(campaign.replies) > 0:
			# Get first index in replies for reply_id
			reply_id = campaign.replies[0].id
			reply = update_saved_reply(message, account_id, bot_id, reply_id, campaign=True)
		else:
			reply = create_saved_reply(message, account_id)

		if triggered_after == "SEND_NOW":
			broadcast_ids = create_broadcast(message, WEBHOOK_URL, fb_graph, page_access_token)
			campaign.broadcast_ids = broadcast_ids
			broadcast_data = {
				"ids": broadcast_ids,
				"fb_graph": fb_graph,
				"page_access_token": page_access_token,
				"campaign_id": campaign.id,
				"bot_id": campaign.bot_id
			}
			send_broadcast.apply_async([broadcast_data], countdown=int(campaign.send_after))
			campaign.sent_campaign = 1

		campaign.replies.append(reply)
		campaign.save_to_db()

	else:
		# CREATE NEW CAMPAIGN
		if quick is None or quick is False:
			campaign = CampaignsModel(
				label=label,
				campaign_type=type_,
				triggered_by=triggered_by,
				triggered_after=triggered_after,
				send_after=send_after,
				bot_id=bot_id,
				is_active=True,
				persistent=persistent,
				channel=channel
			)
			campaign.save_to_db()

			# Add new saved replies
			reply = create_saved_reply(message, account_id)

			campaign.replies.append(reply)
			campaign.save_to_db()
		else:
			campaign = CampaignsModel(
				label=label,
				campaign_type=type_,
				triggered_by="USER",
				triggered_after="SEND_NOW",
				send_after=0,
				bot_id=bot_id,
				is_active=True,
				persistent=persistent,
				channel=channel
			)
			campaign.save_to_db()

		# Update broadcast estimate and delay 20s
		if channel == 'messenger':
			celery_data = {
				'page_access_token': page_access_token,
				'campaign_id': campaign.id,
				'bot_id': bot_id
			}
			total_reach_messenger.delay(celery_data)

		campaign.save_to_db()

	resp = campaign_schema.jsonify(campaign)
	return resp


def get_campaign_list(account_id, start, limit, bot_id):
	"""Get list of campaigns."""
	account = AccountModel.query.filter_by(id=account_id).first()
	if account is None or account.deleted is True:
		return abort(404, "Account not found.")

	# Set default start and limit if not provided
	if start is None or limit is None:
		start = 1
		limit = 10

	# If channel is not provided, then return all saved_replies
	if bot_id is None:
		campaigns = db.session.query(CampaignsModel, BotModel).\
					filter(BotModel.account_id == account_id).\
					filter(BotModel.id == CampaignsModel.bot_id).\
					filter(BotModel.deleted == False).\
					filter(CampaignsModel.deleted == False).\
					with_entities(CampaignsModel).all()
	else:
		campaigns = db.session.query(CampaignsModel, BotModel).\
					filter(BotModel.account_id == account_id).\
					filter(BotModel.id == CampaignsModel.bot_id).\
					filter(BotModel.id == bot_id).\
					filter(BotModel.deleted == False).\
					filter(CampaignsModel.deleted == False).\
					with_entities(CampaignsModel).all()

	pagination = campaign_paginated(campaigns, '/v2/campaigns/{}'.format(account_id),start, limit, bot_id=bot_id)

	resp = page_schema.jsonify(pagination)
	resp.status_code = 200
	return resp


def delete_campaign(account_id, bot_id, campaign_id, force=False):
	"""Soft delete saved_reply."""
	find_account = AccountModel.find_by_id(account_id)
	if not find_account or find_account.deleted:
		return abort(404, "account not found")

	# Check relationship saved_reply_id, and account_id
	campaign = CampaignsModel.query.filter_by(id=campaign_id).first()
	if campaign and not campaign.deleted:
		if campaign.persistent is True and force is False:
			abort(400, "Persistent reply cannot be deleted")

		if int(bot_id) == int(campaign.bot_id):
			campaign.deleted = True
			if len(campaign.replies) > 0:
				campaign.replies[0].deleted = True
				campaign.replies[0].save_to_db()
			campaign.save_to_db()

			return '', 204

		return abort(404, "account_id and campaign didn't match")
	return abort(404, "campaign not found")


def search_campaign(account_id, bot_id, text, start, limit, channel):
	"""Index only label."""
	account = AccountModel.query.filter_by(id=account_id).first()
	if account is None or account.deleted is True:
		return abort(404, "Account not found.")

	if channel is None:
		campaigns = CampaignsModel.query.filter_by(bot_id=bot_id).filter_by(deleted=False).whoosh_search(text).all()
	else:
		campaigns = CampaignsModel.query.filter_by(bot_id=bot_id).filter_by(deleted=False).filter_by(channel=channel).whoosh_search(text).all()

	# Set default start and limit if not provided
	if start is None or limit is None:
		start = 1
		limit = 10

	pagination = campaign_paginated(
		campaigns,
		'/v2/campaigns/{}/{}/search?text={}'.format(account_id, bot_id, text),
		start,
		limit,
		is_search=True
	)
	resp = page_schema.jsonify(pagination)
	resp.status_code = 200
	return resp

def create_broadcast(message, WEBHOOK_URL, fb_graph, page_access_token):
	broadcast_ids = []
	saved_replies = message["saved_reply"]
	for reply in saved_replies:
		message_type = reply["message_type"]
		if message_type == "FB_TEXT":
			payload = {
				"text": reply["text"]
			}
		elif message_type == "FB_ATTACHMENT":
			payload = {
				"attachment": {
					"type": "image",
					"payload": {
						"url": reply["attachment_url"],
						"is_reusable": True
					}
				}
			}
		elif message_type == "FB_QUICK_REPLY":
			payload = {
				"quick_replies": reply["quick_replies"],
				"text": reply["text"]
			}
		elif message_type == "FB_BUTTON_TEMPLATE":
			payload = {
				"attachment": {
					"type": "template",
					"payload": {
						"template_type": "button",
						"buttons": reply["buttons"],
						"text": reply["text"]						
					}
				}
			}
		elif message_type == "FB_GENERIC_TEMPLATE":
			templates = reply["message_templates"]

			urls = []
			for template in templates:
				template["title"] = template.pop("template_title")
				if template.get("template_url") is not None:
					template.pop("template_url")

				urls.append(template["image_url"])
				buttons = template["buttons"]
				for button in buttons:
					if button["type"] == "web_url":
						urls.append(button["url"])

			print(templates)
			if len(urls) > 0:
				whitelist_request = {
					"whitelisted_domains": urls
				}
				whitelist_url = "{0}/messenger_profile?access_token={1}".format(fb_graph, page_access_token)
				rv = requests.post(whitelist_url, json=whitelist_request)
				if rv.status_code != 200:
					return abort(400, "Url can't be whitelisted, make sure it use https.")

			payload = {
				"attachment": {
					"type": "template",
					"payload": {
						"template_type": "generic",
						"elements": templates
					}
				}
			}


		data = {
			"messages": [
				payload
			]
		}
		url = "{}/message_creatives?access_token={}".format(fb_graph, page_access_token)
		req = requests.post(url, json=data)
		if req.status_code != 200:
			print(req.text, data)
			abort(400, "Failed to make broadcast messages")

		resp_data = req.json()
		broadcast_ids.append(resp_data["message_creative_id"])

	return broadcast_ids
