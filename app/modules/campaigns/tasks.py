
"""Add celery tasks here."""
import requests
from flask import current_app
from .models import CampaignsModel
from ...utilities.abort import abort
from ..wit.ops import add_entity, validate_entity, remove_expression
from app.extensions.flask_celery import make_celery
import time

celery = make_celery(current_app)

@celery.task
def total_reach_messenger(data):
    fb_graph = 'https://graph.facebook.com/v2.11'
    campaign_id = data['campaign_id']
    bot_id = data['bot_id']
    page_access_token = data['page_access_token']

	# Get the ID first
    url = "{}/me/broadcast_reach_estimations?access_token={}".format(fb_graph, page_access_token)
    req = requests.post(url)
    if "error" in req.json():
        abort(400, "Cannot get reach_estimation_id")
    reach_estimation_id = req.json()['reach_estimation_id']

    # Delay 20s
    time.sleep(20)

	# Use the ID to get est reach, delay 20 s
    url = "{}/{}?access_token={}".format(fb_graph, reach_estimation_id, page_access_token)
    req = requests.get(url)
    if "error" in req.json():
        abort(400, "Cannot get estimate broadcast")
    total_customer = int(req.json()["reach_estimation"])

    # Update campaign
    campaign = CampaignsModel.query.\
               filter_by(id=campaign_id).\
               filter_by(bot_id=bot_id).\
               filter_by(deleted=False).first()
    if campaign is None:
        return abort(404, "Campaign not found.")
    campaign.total_customer = total_customer
    campaign.save_to_db()

@celery.task
def send_broadcast(data):
    sent_id = []
    for broadcast in data["ids"]:
        payload = {
            "message_creative_id": broadcast
        }
        url = "{}/broadcast_messages?access_token={}".format(data["fb_graph"], data["page_access_token"])
        req = requests.post(url, json=payload)
        if "error" in req.json():
            abort(400, "Cannot create broadcast")
        sent_id.append(req.json()["broadcast_id"])


    time.sleep(20)
    total_reach = 0
    for id in sent_id:
        metric_url = "https://graph.facebook.com/v2.11/{}/insights/messages_sent?access_token={}".format(id, data["page_access_token"])
        g = requests.get(metric_url)
        total_reach += int(g.json()["data"][0]["values"][0]["value"])

    campaign = CampaignsModel.query.\
               filter_by(id=data["campaign_id"]).\
               filter_by(bot_id=data["bot_id"]).\
               filter_by(deleted=False).first()
    if campaign is None:
        return abort(404, "Campaign not found.")
    campaign.sent_campaign = total_reach
    campaign.save_to_db()
