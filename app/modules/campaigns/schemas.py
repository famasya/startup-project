# pylint: disable=E1101, C0103, W0611, W0622, R0903

"""Schema for campaigns API endpoints"""
from flask import abort
from marshmallow import post_dump
from .models import CampaignsModel, CampaignsQueue
from ..saved_reply.schemas import SavedReplySchema

from ...extensions.schema import ma


def campaign_paginated(results, url, start, limit, is_search=False, bot_id=None):
    """Returning object for variant of pagination below."""
    start = int(start)
    limit = int(limit)

    obj = {}
    # check if page exists
    if len(results) == 0:
        obj['count'] = 0
        obj['start'] = start
        obj['limit'] = limit
        obj['results'] = []
        obj['previous'] = ''
        obj['next'] = ''

        return obj

    # make response
    obj['start'] = start
    obj['limit'] = limit

    sorted_results = [x for x in reversed(results)]

    # check if page exists
    count = len(sorted_results)
    if count < start:
        return abort(404, "Start exceeds total of the results.")

    obj['count'] = count

    # make URLs
    # make previous url
    if start == 1:
        obj['previous'] = ''

    start_copy = max(1, start - limit)
    limit_copy = start - 1
    if is_search:
        obj['previous'] = url + '&start=%d&limit=%d' % (start_copy, limit_copy)
    else:
        obj['previous'] = url + '?start=%d&limit=%d' % (start_copy, limit_copy)

    if bot_id is not None:
        obj['previous'] += '&bot_id={}'.format(bot_id)

    # make next url
    if start + limit > count:
        obj['next'] = ''

    start_copy = start + limit
    if is_search:
        obj['next'] = url + '&start=%d&limit=%d' % (start_copy, limit)
    else:
        obj['next'] = url + '?start=%d&limit=%d' % (start_copy, limit)

    if bot_id is not None:
        obj['next'] += '&bot_id={}'.format(bot_id)

    # finally extract result according to bounds
    obj['results'] = sorted_results[(start - 1):(start - 1 + limit)]

    return obj


class CampaignsSchema(ma.ModelSchema):
    class Meta:
        model = CampaignsModel
        exclude = ["deleted", "campaigns_queue"]

    @post_dump
    def modify(self, data):
        """Modify api responses"""
        data["message"] = data.pop("replies")  # Change key 'replies' to 'message'

        data["content"] = {}
        data["content"]["message"] = data["message"]
        data["content"]["triggered_by"] = data["triggered_by"]
        data["content"]["triggered_after"] = data["triggered_after"]
        data["content"]["send_after"] = data["send_after"]

        data.pop("message")
        data.pop("triggered_by")
        data.pop("triggered_after")
        data.pop("send_after")

        # If quick is True, then message list will be empty, just res with empty content
        message = data["content"]["message"]
        triggered_by = data["content"]["triggered_by"]
        triggered_after = data["content"]["triggered_after"]
        send_after = data["content"]["send_after"]


    replies = ma.Nested('SavedReplySchema', many=True, exclude=("bot_rules",))

campaign_schema = CampaignsSchema()
campaigns_schema = CampaignsSchema(many=True)

class CampaignsQueueSchema(ma.ModelSchema):
    class Meta:
        model = CampaignsQueue
        exclude = ["campaigns_queue"]

campaign_queue_schema = CampaignsQueueSchema()
campaigns_queue_schema = CampaignsQueueSchema(many=True)

class PageSchema(ma.Schema):
    """Define pagination schema"""
    class Meta:
        """Custom fields for pagination, results will contain customer list."""
        fields = ('start', 'limit', 'previous', 'next', 'count', 'results')

    results = ma.Nested('CampaignsSchema', many=True)

page_schema = PageSchema()
