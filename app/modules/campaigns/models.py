# pylint: disable=E1101, C0103, W0611, W0622, R0903

import datetime
import flask_whooshalchemy as wa
from flask import abort, current_app

from app.extensions.db import db
from app.utilities.time import timestamp
from ..saved_reply.models import SavedReplyModel

rel_campaigns = db.Table("rel_campaigns",
    db.Column("id", db.Integer, primary_key=True),
    db.Column("campaign_id", db.Integer, db.ForeignKey("campaigns.id")),
    db.Column("reply_id", db.Integer, db.ForeignKey("saved_replies.id"))
)

class CampaignsModel(db.Model):
    """Define Campaigns."""
    __tablename__ = 'campaigns'
    __searchable__ = ["label"]

    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String)
    campaign_type = db.Column(db.String(20))
    channel = db.Column(db.String)
    triggered_by = db.Column(db.String)
    triggered_after = db.Column(db.String)
    send_after = db.Column(db.String)
    persistent = db.Column(db.Boolean)
    is_active = db.Column(db.Boolean, default=True)
    deleted = db.Column(db.Boolean, default=False)
    bot_id = db.Column(db.Integer, db.ForeignKey("bots.id"))
    broadcast_ids = db.Column(db.JSON)
    total_customer = db.Column(db.Integer, default=0)
    sent_campaign = db.Column(db.Integer, default=0)

    replies = db.relationship('SavedReplyModel', secondary=rel_campaigns, backref=db.backref('campaigns', lazy='dynamic'))
    campaigns_queue = db.relationship('CampaignsQueue', backref='campaigns', lazy='dynamic')

    def save_to_db(self):
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

# Register search index capability
wa.whoosh_index(current_app, CampaignsModel)

class WoocommerceCartLog(db.Model):
    """Define WoocommerceCartLog."""
    __tablename__ = "woocommerce_cart_log"

    id = db.Column(db.Integer, primary_key=True)
    bot_id = db.Column(db.Integer, db.ForeignKey("bots.id"))
    created = db.Column(db.Integer, default=timestamp)
    execution = db.Column(db.Integer)
    status = db.Column(db.String(20))
    user_ref = db.Column(db.String)
    data = db.Column(db.JSON)

    def save_to_db(self):
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise

class CampaignsQueue(db.Model):
    """Define WoocommerceCartLog."""
    __tablename__ = "campaigns_queue"

    id = db.Column(db.Integer, primary_key=True)
    campaign_id = db.Column(db.Integer, db.ForeignKey("campaigns.id"))
    send_after = db.Column(db.Integer)
    execution_id = db.Column(db.String)
    triggered_by = db.Column(db.String(20))
    triggered_after = db.Column(db.String(40))
    status = db.Column(db.String(40), default="QUEUED")
    recipient_id = db.Column(db.String)
    access_token = db.Column(db.String)
    user_ref = db.Column(db.String)
    messages = db.Column(db.JSON)

    def save_to_db(self):
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise
