from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ..ops import create_campaign, get_campaign_list, delete_campaign, search_campaign
from ....utilities.api_limiter import api_limiter

api = Namespace('campaigns', description='Campaigns endpoint')


@api.route('/<account_id>/<bot_id>/search')
class SearchCampaign(Resource):
    @login_required
    def get(self, account_id, bot_id):
        '''Search camapaign'''
        api_limiter(account_id, request.referrer)
        text = request.args.get("text")
        start = request.args.get("start")
        limit = request.args.get("limit")
        channel = request.args.get("channel")
        return search_campaign(account_id, bot_id, text, start, limit, channel)

@api.route('/<account_id>')
class CreateCampaign(Resource):
	@api.doc('campaigns_list')
	@login_required
	def get(self, account_id):
		"""Get campaign list."""
		start = request.args.get('start')
		limit = request.args.get('limit')
		bot_id = request.args.get('bot_id')
		return get_campaign_list(account_id, start, limit, bot_id)

	@api.doc('create_new_campaign')
	@login_required
	def post(self, account_id):
		data = request.json
		quick = request.args.get('quick')
		return create_campaign(data, account_id, quick=quick, check_type=True)

@api.route('/<account_id>/<bot_id>/<campaign_id>')
class CampaignDetail(Resource):
	@login_required
	def put(self, account_id, bot_id, campaign_id):
		data = request.json
		return create_campaign(data, account_id, update=True, campaign_id=campaign_id)

	@login_required
	def delete(self, account_id, bot_id, campaign_id):
		return delete_campaign(account_id, bot_id, campaign_id)
