import datetime
import flask_whooshalchemy as wa
from flask import abort, current_app

from app.extensions.db import db
from app.utilities.time import timestamp

class SequenceModel(db.Model):
    __tablename__ = 'sequence'

    id = db.Column(db.Integer, primary_key=True)
    account_id = db.Column(db.String, db.ForeignKey('accounts.id'))
    payload = db.Column(db.JSON, default=False)
    label = db.Column(db.String)
    channel = db.Column(db.String)
    date_created = db.Column(db.Integer, default=timestamp)
    date_updated = db.Column(db.Integer, default=timestamp)
    deleted = db.Column(db.Boolean, default=False)

    # sequence_logs = db.relationship('SequenceLog', backref='sequence', lazy='dynamic')

    def save_to_db(self):
        """Save to database."""
        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            raise
