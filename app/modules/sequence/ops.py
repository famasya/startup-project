from .models import SequenceModel
from .schemas import sequence_schema, sequences_schema
from ..saved_reply.models import SavedReplyModel, SavedText, SavedAttachment, SavedQuickReply, SavedQuickReplyButton
from ..saved_reply.schemas import saved_reply_schema
from ..account.models import AccountModel
from ..channel.models import ChannelModel
from ...extensions.db import db
from ...utilities.abort import abort
from ...utilities.time import timestamp
from ..message.tasks import send_message
from flask import current_app, session, jsonify
from flask_login import current_user
import requests, pprint
from celery import uuid
from sqlalchemy.orm.attributes import flag_modified

def trigger_sequence(account_id, data):
	try:
		sequence_id = data["sequence_id"]
		channel_id = data["channel_id"]
		sender_id = data["sender_id"]
		recipient_id = data["recipient_id"]
	except:
		abort(400, "Invalid format.")

	query = db.session.query(SequenceModel, AccountModel).\
			filter(SequenceModel.id == sequence_id).\
			filter(SequenceModel.account_id == account_id).\
			filter(SequenceModel.account_id == account_id).\
			filter(SequenceModel.deleted == False).\
			filter(SequenceModel.account_id == AccountModel.id).\
			filter(AccountModel.deleted == False).first()

	try:
		sequence = getattr(query, "SequenceModel")
		account = getattr(query, "AccountModel")
	except:
		abort(404, "Sequence not found")

	sequences = sequence.payload
	WEBHOOK_URL = current_app.config['WEBHOOK_URL']

	channel = ChannelModel.query.filter_by(id=channel_id).filter_by(page_id=sender_id).filter_by(deleted=False).first()
	if channel is None:
		abort(404, "Channel not found")

	tz = account.timezone
	# If timezone is faster than UTC, then schedule it by tomorrow - timezone. Otherwise, just add the delay. 
	# Assumed time in server is UTC
	if tz < 0:
		additional_delay = (24+tz)*3600
	else:
		additional_delay = tz*3600

	for sequence in sequences:
		interval = sequence["interval"]
		messages = sequence["messages"]
		for message in messages:

			_type = message["message_type"]
			if _type == "FB_TEXT":
				payload = {
					"user_id": recipient_id,
					"text": message["text"],
					"page_access_token": channel.page_access_token
				}
				url = '{}/messenger/messages/text'.format(WEBHOOK_URL)

			elif _type == "FB_ATTACHMENT":
				payload = {
					"user_id": recipient_id,
					"attachment_url": message["attachment_url"],
					"page_access_token": channel.page_access_token				
				}
				url = '{}/messenger/messages/attachment'.format(WEBHOOK_URL)

			elif _type == "FB_QUICK_REPLY":
				payload = {
					"recipient_id": recipient_id,
					"quick_replies": message["quick_replies"],
					"text": message["text"],
					"page_access_token": channel.page_access_token
				}
				url = '{}/messenger/messages/quick_reply'.format(WEBHOOK_URL)

			elif _type == "FB_BUTTON_TEMPLATE":
				payload = {
					"recipient_id": recipient_id,
					"buttons": message["buttons"],
					"text": message["text"],
					"page_access_token": channel.page_access_token
				}
				url = '{}/messenger/messages/button_template'.format(WEBHOOK_URL)

			elif _type == "FB_GENERIC_TEMPLATE":
				urls = []
				templates = message["message_templates"]
				for item in templates:
					if "template_url" in item:
						urls.append(item["template_url"])
					for button in item["buttons"]:
						if button["type"] == "web_url":
							urls.append(button["url"])

				whitelist_request = {
					"whitelisted_domains": urls
				}
				whitelist_url = "{0}/messenger_profile?access_token={1}".format(current_app.config["FB_GRAPH"], channel.page_access_token)
				rv = requests.post(whitelist_url, json=whitelist_request)
				if rv.status_code != 200:
					return abort(400, "Url can't be whitelisted, make sure it use https.")

				payload = {
					"recipient_id": recipient_id,
					"elements": templates,
					"page_access_token": channel.page_access_token
				}
				url = '{}/messenger/messages/generic_template'.format(WEBHOOK_URL)

			else:
				pass

			delay = interval+additional_delay
			send_message.apply_async([url, payload], countdown=delay)

		# Set additional_delay value from sum of previous interval
		additional_delay += interval

	return data

def get_sequence(account_id, sequence_id=None):
	account = AccountModel.query.filter_by(deleted=False).first()
	if account is None:
		abort(400, "Account not found")

	if sequence_id == None:
		sequences = SequenceModel.query.filter_by(deleted=False).filter_by(account_id=account_id).all()
		resp = sequences_schema.jsonify(sequences)
	else:
		sequence = SequenceModel.query.filter_by(deleted=False).filter_by(account_id=account_id).filter_by(id=sequence_id).first()
		resp = sequence_schema.jsonify(sequence)

	return resp

def update_sequence(data, account_id, sequence_id):
	try:
		label = data['label']
		sequences = data['sequence']
	except:
		abort(400, "Invalid format.")

	sequence = SequenceModel.query.filter_by(id=sequence_id).filter_by(deleted=False).first()
	if sequence is None:
		abort(400, "Sequence is not found")

	saved_sequences = sequence.payload
	latest_sequence_item_id = 1
	latest_message_id = 1

	# Get latest message_id
	for i in saved_sequences:
		for j in i.get("messages"):
			current_id = j.get("message_id")
			if int(current_id) > latest_message_id:
				latest_message_id = current_id
	latest_message_id += 1

	# Get latest sequence_item_id
	latest_sequence_item_id = max(x["sequence_item_id"] for x in saved_sequences if x.get("sequence_item_id") != None)+1
	for seq in sequences:
		# If sequence_item_id provided, then it is intended to modify current sequence item
		if "sequence_item_id" in seq:

			# Filter sequence item with sequence_item_id
			item_id = seq["sequence_item_id"]
			try:
				item = list(filter(lambda i: i['sequence_item_id'] == item_id, saved_sequences))[0]
			except:
				abort(400, "Wrong sequence item format")

			# If user update the interval, update the interval
			if "interval" in seq:
				item["interval"] = seq["interval"]

			for message in seq["messages"]:

				# If message_id provided, then it will modify existing message
				if "message_id" in message:
					try:
						message_item = list(filter(lambda i: i.get("message_id") == int(message["message_id"]), item["messages"]))[0]
					except:
						abort(400, "Wrong message format")
					message_type = message_item["message_type"]

					current_type = message.get("message_type").upper()
					if current_type is None:
						abort(400, "message_type is requeired")

					if message_type == "FB_TEXT" and current_type == "FB_TEXT":
						message_item["text"] = message["text"]
					elif message_type == "FB_ATTACHMENT" and current_type == "FB_ATTACHMENT":
						message_item["attachment_url"] = message["attachment_url"]
					elif message_type == "FB_QUICK_REPLY" and current_type == "FB_QUICK_REPLY":
						message_item["text"] = message["text"]
						message_item["quick_replies"] = message["quick_replies"]
					elif message_type == "FB_BUTTON_TEMPLATE" and current_type == "FB_BUTTON_TEMPLATE":
						message_item["text"] = message["text"]
						message_item["buttons"] = message["buttons"]						
					elif message_type == "FB_GENERIC_TEMPLATE" and current_type == "FB_GENERIC_TEMPLATE":
						message_item["text"] = message["text"]
						message_item["message_templates"] = message["message_templates"]
					else:
						abort(400, "message_type didn't match")		

				# If no message_id provided, then it will create new message
				else:
					# Set latest message_id + 1
					message["message_id"] = latest_message_id
					item["messages"].append(message)
					latest_message_id += 1

		# If no sequence_item_id provided, create new sequence_item
		else:
			seq["sequence_item_id"] = latest_sequence_item_id
			latest_sequence_item_id += 1

			# Set id for each new message
			for message in seq["messages"]:
				message["message_id"] = latest_message_id
				latest_message_id += 1

			saved_sequences.append(seq)

	sequence.label = label
	sequence.date_updated = timestamp()
	sequence.payload = saved_sequences
	flag_modified(sequence, "payload")
	sequence.save_to_db()
	return sequence_schema.dump(sequence).data, 200

def delete_sequence(account_id, sequence_id, data):
	"""Soft delete sequence."""
	find_account = AccountModel.find_by_id(account_id)
	if not find_account or find_account.deleted:
		abort(404, "account not found")

	sequence = SequenceModel.query.filter_by(id=sequence_id).filter_by(deleted=False).first()
	if sequence is None:
		abort(400, "Sequence not found")

	if data is None:
		# If no data is provided, assume it to delete whole sequence
		sequence.deleted = True
		sequence.save_to_db()
		resp = '', 204
	else:
		saved_sequences = sequence.payload
		new_sequences = []

		# If data is provided, check for each ID and delete only given ID
		for item in data:
			# Check if sequence_item_id is provided
			sequence_item_id = item.get("sequence_item_id")
			if sequence_item_id is not None:
				# If sequence_item_id is provided and message_id also provided, it means delete messages in selected sequence item
				messages = item.get("message_id")
				sequence_item = list(filter(lambda x: x.get("sequence_item_id") == sequence_item_id, saved_sequences))[0]
				if messages is not None:
					# Filter process: return all sequence item message if it's not in deleted messages' list
					filtered_messages = [i for i in sequence_item.get("messages") if i.get("message_id") not in messages]
					# Return sequence item messages after filtered
					sequence_item["messages"] = filtered_messages
				else:
					# If message_id is not provided means that delete this sequence item
					saved_sequences = list(filter(lambda x: x.get("sequence_item_id") != sequence_item_id, saved_sequences))

		sequence.payload = saved_sequences
		sequence.date_updated = timestamp()
		flag_modified(sequence, "payload")
		sequence.save_to_db()
		resp = sequence_schema.jsonify(sequence)

	return resp

def check_format(data):
	try:
		label = data["label"]
		sequence = data["sequence"]
	except:
		abort(400, "Invalid format")

	for seq in sequence:
		try:
			interval = seq["interval"]
			messages = seq["messages"]
		except:
			abort(400, "Invalid sequence item format")

		for item in messages:
			try:
			    message_type = item["message_type"].upper()
			except (KeyError, TypeError):
			    return abort(400, "message_type is required.")

			if message_type in ("FB_TEXT"):
			    try:
			        text = item["text"]
			    except (KeyError, TypeError):
			        return abort(400, "text is missing.")

			    if len(text) > 640:
			        return abort(400, "text limit is 640 chars, your text exceed the limit.")

			elif message_type in ("FB_ATTACHMENT"):
			    try:
			        attachment_url = item["attachment_url"]
			    except (KeyError, TypeError):
			        return abort(400, "attachment_url is missing.")

			elif message_type == "FB_QUICK_REPLY":
			    try:
			        text = item["text"]
			        quick_replies = item["quick_replies"]  # list
			    except (KeyError, TypeError):
			        return abort(400, "Invalid request.")

			    if len(text) > 640:
			        return abort(400, "Text limit is 640 chars, your text exceeds the limit.")

			    if len(quick_replies) > 3:
			        return abort(400, "Quick Reply button limit is 3 buttons, your buttons exceed the limit.")

			    for quick_reply in quick_replies:
			        try:
			            title = quick_reply["title"]
			            payload = quick_reply["payload"]
			        except (KeyError, TypeError):
			            return abort(400, "Invalid request.")

			elif message_type == "FB_BUTTON_TEMPLATE":
			    try:
			        text = item["text"]
			        buttons = item["buttons"]  # list
			    except (KeyError, TypeError):
			        return abort(400, "Invalid templates requests.")

			    if len(text) > 640:
			        return abort(400, "Text limit is 640 chars, your text exceed the limit.")

			    if len(buttons) > 3:
			        return abort(400, "Button limit is 3 buttons, your buttons exceed the limit.")

			    for button in buttons:
			        try:
			            button_type = button["type"]
			        except (KeyError, TypeError):
			            return abort(400, "Invalid button type requests.")
			        if button_type == "web_url":
			            try:
			                button_url = button["url"]
			                button_title = button["title"]
			            except (KeyError, TypeError):
			                return abort(400, "Invalid button requests.")

			            if len(button_title) > 20:
			                return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

			            # Validate webhook url, it must be https
			            url = urlparse(button_url)
			            if url.scheme != 'https':
			                return abort(400, "Invalid webhook url, it must be https.")

			        elif button_type == "postback" or button_type == "phone_number":
			            try:
			                button_title = button["title"]
			                payload = button["payload"]
			            except (KeyError, TypeError):
			                return abort(400, "Invalid button requests.")

			            if len(button_title) > 20:
			                return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

			        elif button_type == "element_share":
			            pass

			elif message_type == "FB_GENERIC_TEMPLATE":
			    try:
			        templates = item["message_templates"]  # list
			    except (KeyError, TypeError):
			        return abort(400, "Invalid templates request.")

			    if len(templates) > 10:
			        return abort(400, "Horizontal templates limit is 10 element, your templates exceed the limit.")

			    for template in templates:
			        try:
			            template_title = template["template_title"]
			            image_url = template["image_url"]
			            subtitle = template["subtitle"]
			            template_url = template["template_url"]
			            buttons = template["buttons"]
			        except (KeyError, TypeError):
			            return abort(400, "Invalid templates request.")

			        if len(template_title) > 80:
			            return abort(400, "Title limit is 80 chars, your title exceeds the limit.")

			        if len(subtitle) > 80:
			            return abort(400, "Subtitle limit is 80 chars, your title exceeds the limit.")

			        if len(buttons) > 3:
			            return abort(400, "Button limit is 3 buttons, your buttons exceed the limit.")

			        for button in buttons:
			            try:
			                button_type = button["type"]
			            except (KeyError, TypeError):
			                return abort(400, "Invalid button type requests.")
			            if button_type == "web_url":
			                try:
			                    button_url = button["url"]
			                    button_title = button["title"]
			                except (KeyError, TypeError):
			                    return abort(400, "Invalid button requests.")

			                if len(button_title) > 20:
			                    return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

			            elif button_type == "postback" or button_type == "phone_number":
			                try:
			                    button_title = button["title"]
			                    payload = button["payload"]
			                except (KeyError, TypeError):
			                    return abort(400, "Invalid button requests.")

			                if len(button_title) > 20:
			                    return abort(400, "Button title limit is 20 chars, your button title exceeds the limit.")

			            elif button_type == "element_share":
			                pass

def create_sequence(data, account_id):

	check_format(data)

	label = data["label"]
	channel = data["channel"]
	sequences = data["sequence"]

	message_id = 1
	sequence_item_id = 1
	for sequence in sequences:
		sequence["sequence_item_id"] = sequence_item_id
		if ("interval" or "messages") not in sequence:
			abort(400, "Invalid sequence format.")

		for message in sequence["messages"]:
			message["message_id"] = message_id
			message_id += 1

		sequence_item_id += 1

	seq_model = SequenceModel(
		account_id=account_id,
		payload=sequences,
		channel=channel,
		label=label
	)
	seq_model.save_to_db()

	resp = sequence_schema.dump(seq_model).data
	return resp, 201
