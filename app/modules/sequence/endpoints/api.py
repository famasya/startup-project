from flask import request
from flask_restplus import Namespace, Resource, fields
from flask_login import login_required
from ....utilities.api_limiter import api_limiter
from ..ops import trigger_sequence, get_sequence, update_sequence, delete_sequence, create_sequence

api = Namespace('sequences', description='Sequence related operations')

@api.route('/<account_id>')
class Sequence(Resource):
	@login_required
	def get(self, account_id):
		api_limiter(account_id, request.referrer)
		return get_sequence(account_id)	

	@login_required
	def post(self, account_id):
		api_limiter(account_id, request.referrer)
		data = request.json
		return create_sequence(data, account_id)


@api.route('/<account_id>/triggers')
class SequenceTriggers(Resource):
	@login_required
	def post(self, account_id):
		api_limiter(account_id, request.referrer)
		data = request.json
		return trigger_sequence(account_id, data)

@api.route('/<account_id>/<sequence_id>')
class SequenceItem(Resource):
	@login_required
	def get(self, account_id, sequence_id):
		api_limiter(account_id, request.referrer)
		return get_sequence(account_id, sequence_id)		

	@login_required
	def put(self, account_id, sequence_id):
	    api_limiter(account_id, request.referrer)
	    data = request.json
	    resp = update_sequence(data, account_id, sequence_id)
	    return resp

	@login_required
	def delete(self, account_id, sequence_id):
		api_limiter(account_id, request.referrer)
		data = request.json
		resp = delete_sequence(account_id, sequence_id, data)
		return resp
