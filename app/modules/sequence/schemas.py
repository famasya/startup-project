from ...extensions.schema import ma
from .models import SequenceModel

class SequenceSchema(ma.Schema):
    class Meta:
        model = SequenceModel
        fields = ["id", "account_id", "payload", "date_created", "date_updated", "deleted", "label", "channel"]

sequence_schema = SequenceSchema()
sequences_schema = SequenceSchema(many=True)
