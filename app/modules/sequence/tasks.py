from .ops import get_information
from flask import current_app
from app.extensions.flask_celery import make_celery

celery = make_celery(current_app)

