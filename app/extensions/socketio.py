# pylint: disable=C0103
"""Flask SocketIO."""
from flask_socketio import SocketIO


socketio = SocketIO()
