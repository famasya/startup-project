# pylint: disable=C0103

"""Extension for flask jwt extended."""
import base64
from flask_login import LoginManager

login_manager = LoginManager()


def generate_api_key(username, password):
    """
    This function will generate username and
    password into base64 with output string
    """
    user = '{0}:{1}'.format(username, password)
    byte = user.encode('utf-8')  # Convert to byte type
    encoded = base64.encodebytes(byte)  # encode the byte-type user
    string_decoded = encoded.decode('utf-8').split('\n')[0]  # Convert it to string
    return string_decoded


def verify_api_key(api_key):
    """
    This funtion will verify api_key and return
    username and password string type
    """
    try:
        byte = api_key.encode('utf-8')  # Convert to byte-type
        decoded = base64.decodebytes(byte)  # Decode the byte-type
        string_decoded = decoded.decode('utf-8')  # Convert it to string

        username = string_decoded.split(':')[0]
    except:
        return None

    return username
