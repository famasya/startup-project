# pylint: disable=C0103, E1101

"""Extension for SQLAlchemy."""
from flask import abort
from sqlalchemy import exc
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
