# pylint: disable=C0301
"""
Redis config, this config works if we set redis in the same server with the app.
"""
import redis

conn = redis.Redis('localhost')
