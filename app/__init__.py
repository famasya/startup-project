# pylint: disable=C0103, C0413

"""
Adding Flask Extensions:
If you want to add more flask extensions, you can import the extensions from app/extensions
and init the app in this file
"""
import os
import logging
from logging.handlers import RotatingFileHandler
from logging import FileHandler, WARNING

from flask import Flask, request, Blueprint, current_app, g
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_login import LoginManager, current_user, AnonymousUserMixin, UserMixin
from raven.contrib.flask import Sentry
import redis

from app.extensions.db import db
from app.extensions.auth import login_manager
from app.extensions.schema import ma

from app.extensions.mail import mail
from app.extensions.flask_celery import make_celery
from app.extensions.socketio import socketio


def register_socket():
    """Define socket on each modules and import here."""
    from app.modules.user.sockets import (
        connect_handler, client_disconnected,
        on_join, on_leave
    )
    from app.modules.message.sockets import (
        handle_message, connect_handler, client_disconnected,
        on_join, on_leave
    )


def register_blueprint(flask_app):
    """Define custom routes that not included into RESTPlus."""
    from .modules.webhook_connector.messenger.messenger import mod as mod1
    from .modules.webhook_connector.line.line import mod as mod2
    from .modules.websocket.routes import mod as mod3
    from .modules.webhook_connector.charging.payment import mod as mod4
    from .modules.webhook_connector.smooch.livechat import mod as mod5
    from .modules.webhook_connector.telegram.telegram import mod as mod6
    from .modules.webhook_connector.woocommerce.woocommerce import mod as mod7

    flask_app.register_blueprint(mod1, url_prefix='/messenger')
    flask_app.register_blueprint(mod2, url_prefix='/line')
    flask_app.register_blueprint(mod3, url_prefix='/socket')
    flask_app.register_blueprint(mod4, url_prefix='/charging')
    flask_app.register_blueprint(mod5, url_prefix='/smooch')
    flask_app.register_blueprint(mod6, url_prefix='/telegram')
    flask_app.register_blueprint(mod7, url_prefix='/woocommerce')

# Prepare list of api keys
def prepare_api():
    r = redis.StrictRedis()

    try:
        accounts = db.engine.execute("SELECT accounts.id, account_packages.api_call_limit FROM accounts, account_packages WHERE accounts.package_id = account_packages.id")
        for account in accounts:
            r.set('limit_{}'.format(account['id']), account['api_call_limit'])
    except:
        pass

# INITIALIZATION
def create_app(config_name):
    """Define Create App."""
    # define path for email templates
    # it shoud be at /path/to/project/app/templates
    template_dir = os.path.abspath(os.path.dirname(__file__))
    template_dir = os.path.join(template_dir, 'templates')
    app = Flask(__name__, template_folder=template_dir)

    with app.app_context():
        # FLASK CONFIG
        app.config.from_object('app.config.default')
        app.config.from_envvar('CONFIG_FILE')

        db.init_app(app)

        prepare_api()

        # FLASK EXTENSIONS
        ma.init_app(app)
        mail.init_app(app)
        login_manager.init_app(app)
        socketio.init_app(app,
            message_queue=app.config["CELERY_BROKER_URL"],
            engineio_logger=True,
            ping_timeout=80
        )

        # REGISTER ROUTES FROM RESTPLUS
        from .modules import api
        api.init_app(app)

        # REGISTER BLUEPRINT
        register_blueprint(app)

        # REGISTER SOCKET
        register_socket()

        return app

from celery.schedules import crontab

# USED FOR ANOTHER FILE
app = create_app(os.getenv('CONFIG_FILE')) # define app
migrate = Migrate(app, db)

# error tracking from sentry.io, only for production
if os.environ["CONFIG_FILE"] == "config/production.py" or os.environ["CONFIG_FILE"] == "config/development.py":
    sentry = Sentry(dsn='https://f351414deb54489a8562a210914e9e4b:82cebbc11c7d47cfae0bf9af9c846153@sentry.io/158730')
    sentry.init_app(app)

# Check the current_user
@app.before_request
def load_users():
    g.user = current_user

@app.after_request
def after_request(response):
    """ Add CORS
    Set header to current_user if the user is logged using
    api_key or login route
    """
    try:
        response.headers.add('Current-User', g.user.username)
        response.headers.add('Current-User-Role', g.user.role)
        response.headers.add('Current-User-Account', g.user.account_id)
    except AttributeError:
        response.headers.add('Current-User', 'anonymous')

    # This cors setting is for testing on local machine
    # It need to run the frontend at port 3030
    if os.environ["CONFIG_FILE"] == "config/default.py":
        response.headers.add('Access-Control-Allow-Origin', 'http://localhost:3030')
        response.headers.add('Access-Control-Allow-Credentials', 'true')
        response.headers.add('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        response.headers.add('Content-Type', 'text/html; charset=utf-8')
        response.headers.add('Access-Control-Allow-Headers', 'Authorization, DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range')
    return response

# REGISTER COMMAND:
# export FLASK_APP=run.py
# flask db dropdb : delete all data from database (WARNING: DON'T DO IT WHILE ON THE PRODUCTION)
# flask db init : initiate alembic version and create folder app/migrations
# flask db migrate : create new version of database table and column
# flask db upgrade : implement new version of database to database
manager = Manager(app)
manager.add_command('db', MigrateCommand)
