function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

function getCookieValue(a) {
    var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}

var user_ref = getCookieValue("user_ref");
document.getElementById("user_ref").value = user_ref;